<?php

return [

    'analytics' => [
        'google' => [
            'enabled' => env('HS_ANALYTICS_GOOGLE_ENABLED', false),
            'tracking_id' => env('HS_ANALYTICS_GOOGLE_TRACKING_ID', null),
        ],
    ],

    'app' => [

        // The name of the application that is displayed in the Web UI
        'name' => env('APP_NAME', 'HackyStack'),

        // The logo or icon filename that is displayed in the Web UI
        // This file should reside in /public/assets/
        'logo' => env('APP_LOGO', 'hackystack-icon.svg'),

    ],

    'auth' => [

        'tenant' => env('HS_AUTH_TENANT', ''),

        // Authentication Form
        // --------------------------------------------------------------------
        // When users visit the `/login` page, you can choose how the login form
        // appears. The Controllers/Auth/LoginController::show() method contains
        // conditional logic will return the view or handle redirect behavior.
        //
        // 'local' => shows login form and a list of provider options
        // 'providers' => only shows list of provider options
        // 'github' => redirect to provider SSO form
        // 'gitlab' => redirect to provider SSO form
        // 'google' => redirect to provider SSO form
        // 'okta' => redirect to provider SSO form
        // 'microsoft' => redirect to provider SSO form
        // 'salesforce' => redirect to provider SSO form
        // 'slack' => redirect to provider SSO form

        'form' => env('HS_AUTH_FORM', 'local'),

        // Authentication Providers and Single-Sign On (SSO) Providers
        // --------------------------------------------------------------------
        // Single sign on (SSO) uses Laravel Socialite and the community of
        // Socialite Providers for authenticating with the provider.
        // https://laravel.com/docs/7.x/socialite
        // https://socialiteproviders.netlify.app/about.html
        //
        // This is a centralized list of available SSO providers that is used
        // throughout the code base for conditionals and loops. The providers
        // below have already been implemented and only need the security keys
        // or tokens set in the auth_providers database table for each provider.
        'available_sso_providers' => [
            'github',
            'gitlab',
            'google',
            'okta',
            'microsoft',
            'salesforce',
            'slack'
        ],

        // Authentication provider environment variables
        'local' => [
            'enabled' => env('HS_AUTH_LOCAL_ENABLED', true),
            '2fa_enabled' => env('HS_AUTH_LOCAL_2FA_ENABLED', false),
            '2fa_enforced' => env('HS_AUTH_LOCAL_2FA_ENFORCED', false),
            'register_enabled' => env('HS_AUTH_LOCAL_REGISTER_ENABLED', true),
            'reset_enabled' => env('HS_AUTH_LOCAL_RESET_ENABLED', true),
            'verify_enabled' => env('HS_AUTH_LOCAL_VERIFY_ENABLED', true),

            'human_name' => 'Local',
            'icon' => 'ri-shield-user-fill',
            'password' => [
                'count_days_until_must_change' => 120,
            ],
            'failed_login' => [
                'attempt_limit_max' => 8,
                'force_reset_password' => false,
                'force_account_lock' => true,
            ]
        ],
        'github' => [
            'human_name' => 'GitHub',
            'icon' => 'ri-github-fill',
        ],
        'gitlab' => [
            'human_name' => 'GitLab',
            'icon' => 'ri-gitlab-fill',
        ],
        'google' => [
            'human_name' => 'Google',
            'icon' => 'ri-google-fill',
        ],
        'okta' => [
            'human_name' => 'OKTA',
            'icon' => 'ri-account-circle-fill',
        ],
        'microsoft' => [
            'human_name' => 'Microsoft',
            'icon' => 'ri-microsoft-fill',
        ],
        'salesforce' => [
            'human_name' => 'Salesforce',
            'icon' => 'ri-customer-service-2-fill',
        ],
        'slack' => [
            'human_name' => 'Slack',
            'icon' => 'ri-slack-fill',
        ],

    ],

    'cloud' => [

        // Cloud Providers
        // --------------------------------------------------------------------
        // This is a centralized list of available cloud providers that is used
        // throughout the code base for conditionals and loops. The providers
        // below have already been implemented and only need the security keys
        // or tokens set in the cloud_providers database table for each provider.
        'available_providers' => [
            'aws',
            'gcp'
        ],

        'accounts' => [
            'aws' => [

                // Root Email Schema
                // ------------------------------------------------------------
                // When creating a globally unique root email address for AWS,
                // a HackyStack administrator needs access to the email account
                // to be able to perform password resets and delete the account.
                //
                // Plus Method - Creates an alias on an email account or group
                // that HackyStack administrators can access.
                // {plus_prefix}+{cloud_account_short_id}@{plus_domain}
                // Ex. `gitlab-aws-master-accounts+a1b2c3d4@gitlab.com`
                'root_email' => [
                    'method' => env('HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_METHOD', 'plus'),
                    'plus_prefix' => env('HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_PREFIX'),
                    'plus_domain' => env('HS_CLOUD_ACCOUNTS_AWS_ROOT_EMAIL_PLUS_DOMAIN'),
                ],

                // AWS Tags for Cloud Accounts
                // ------------------------------------------------------------
                // You can set a dynamic list of tags that will be added to each
                // cloud account. You can specify a different set of tags for
                // each realm.
                //
                // Supported Types:
                // 'string'
                // 'auth-group-namespace-slug'
                // 'auth-user-string'
                // 'auth-user-provider-meta-data'
                // 'cloud-account-string'
                // 'cloud-organization-unit-string'
                // 'cloud-provider-string'
                // 'cloud-realm-string'
                //
                'tags' => [
                    'sandbox-realm' => [
                        [
                            'key' => 'gl_env_type',
                            'type' => 'string',
                            'value' => 'experiment'
                        ],
                        [
                            'key' => 'gl_env_name',
                            'type' => 'string',
                            'value' => 'User Sandbox'
                        ],
                        [
                            'key' => 'gl_owner_email_handle',
                            'type' => 'auth-user-string',
                            'value' => 'user_handle'
                        ],
                        [
                            'key' => 'gl_entity',
                            'type' => 'string',
                            'value' => 'allocate'
                        ],
                        [
                            'key' => 'gl_realm',
                            'type' => 'string',
                            'value' => 'sandbox'
                        ],
                        [
                            'key' => 'gl_dept',
                            'type' => 'auth-group-namespace-slug',
                            'value' => 'department'
                        ],
                    ],
                    // 'realm-slug' => [
                    //      [
                    //          'key' => 'tag_name',
                    //          'type' => '<see docs>',
                    //          'value' => 'database_column'
                    //      ],
                    // ]
                ],

                // AWS IAM User Password Complexity Requirements
                // ------------------------------------------------------------
                // When creating an AWS IAM user account, a password is randomly
                // generated in app\Services\V1\Cloud\CloudAccountUserService
                // using the hackyzilla/password-generator package. The values
                // below allow you to customize how passwords are generated.
                // https://github.com/hackzilla/password-generator
                'iam' => [
                    'password_requirements' => [
                        'must_change_password' => false,
                        'length' => 24,
                        'upper_case' => [
                            'enabled' => true,
                            'min_count' => 2,
                            'max_count' => null,
                        ],
                        'lower_case' => [
                            'enabled' => true,
                            'min_count' => 2,
                            'max_count' => null,
                        ],
                        'numbers' => [
                            'enabled' => true,
                            'min_count' => 2,
                            'max_count' => null,
                        ],
                        'symbols' => [
                            'enabled' => true,
                            'min_count' => 2,
                            'max_count' => 4,
                        ],
                    ],
                ],

            ],
            'gcp' => [

                // GCP Labels for Cloud Accounts (GCP Projects)
                // ------------------------------------------------------------
                // You can set a dynamic list of labels that will be added to each
                // cloud account. You can specify a different set of labels for
                // each realm.
                //
                // Supported Types:
                // 'string'
                // 'auth-group-namespace-slug'
                // 'auth-user-string'
                // 'auth-user-provider-meta-data'
                // 'cloud-account-string'
                // 'cloud-organization-unit-string'
                // 'cloud-provider-string'
                // 'cloud-realm-string'
                //
                'labels' => [
                    // 'realm-slug' => [
                    //      [
                    //          'key' => 'tag_name',
                    //          'type' => '<see docs>',
                    //          'value' => 'database_column'
                    //      ],
                    // ]
                ],
            ],
        ],

    ],

    'developer' => [

        // GitLab API Connection for Developer Tools
        // --------------------------------------------------------------------
        // When performing development and creating releases, several tools
        // use the GitLab API for accessing the Git repository and related
        // resources in your GitLab project. You should update your `.env`
        // file with your GitLab username and API Personal Access Token.
        // If you are developing for a fork of HackyStack on a private GitLab
        // instance, you can update the URL and project ID below.
        'gitlab' => [
            'api_token' => env('HS_DEVELOPER_GITLAB_API_TOKEN'),
            'api_username' => env('HS_DEVELOPER_GITLAB_API_USERNAME'),
            'changelog_label' => env('HS_DEVELOPER_GITLAB_CHANGELOG_LABEL', 'changelog-type'),
            'base_url' => env('HS_DEVELOPER_GITLAB_BASE_URL', 'https://gitlab.com'),
            'project_id' => env('HS_DEVELOPER_GITLAB_PROJECT_ID', '19349073'),
        ],

    ],

    'footer' => [
        'copyright' => env('HS_FOOTER_COPYRIGHT', 'Powered by HackyStack'),
        'documentation_link' => env('HS_FOOTER_DOCUMENTATION_LINK', 'https://docs.hackystack.io'),
        'support_link' => env('HS_FOOTER_SUPPORT_LINK', 'https://gitlab.com/hackystack/hackystack-portal/-/issues'),
        'support_instructions' => env('HS_FOOTER_SUPPORT_INSTRUCTIONS', ''),
    ],

    // Static variable whether initial installation script has been executed
    'installed' => false,

    'log' => [

        // IP Address Logging
        // --------------------------------------------------------------------
        // It is recommended to track the IP address and geo-location info for
        // each user for monitoring and security logs to identify malicious
        // activity. If you do not wish to track IP addresses (due to company
        // security policies or another reason), you can disable IP logging by
        // setting `HS_LOG_IP_ADDRESS_ENABLED=false` in your .env file.
        //
        // To provide more context for an IP address, we look up the IP location
        // information using ip-api.com API in the stevebauman/location package
        // when displaying an IP address in the UI or CLI.
        //
        // If you do not want to lookup the IP location information in UI or CLI,
        // set `HS_LOG_IP_ADDRESS_LOCATION=false` in your .env file.
        //
        // We only store IP address and not location information in logs.
        //

        'ip_address' => [
            'enabled' => env('HS_LOG_IP_ADDRESS_ENABLED', true),
            'location' => env('HS_LOG_IP_ADDRESS_LOCATION', true),
        ],

        'user_agent' => [
            'enabled' => env('HS_LOG_USER_AGENT_ENABLED', true),
        ]

    ]

];
