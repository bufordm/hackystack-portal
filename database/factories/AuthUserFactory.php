<?php

use Illuminate\Support\Str;
use Faker\Generator as Faker;

$factory->define(App\Models\Auth\User::class, function (Faker $faker) {
    return [
        'auth_tenant' => factory(\App\Models\Auth\Tenant::class)->create();
        'auth_provider' => 'local',
        'full_name' => $faker->firstName.' '.$faker->full_name,
        'organization_name' => $faker->optional()->company,
        'email' => $faker->safeEmail,
        'email_recovery' => $faker->optional()->freeEmail,
        'password' => $faker->optional()->passthrough('Abcd3fgH!J|<'),
        'phone_number' => $faker->passthrough('2065557890'),
        'phone_country_code' => '1',
        'expires_at' => $faker->optional()->passthrough('2029-12-31 01:23:45'),
    ];
});
