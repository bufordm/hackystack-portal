@extends('errors._layouts.core')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="clearfix">
                    <h1 class="float-left display-3 mr-4" style="margin-bottom: 40px;">401</h1>
                    <h4 class="pt-3">Oops! You don't have permission to access that page.</h4>
                    <p class="text-muted">If this is an error, please contact your system administrator to add your user account to a group that has a role with the <code class="text-primary">`{{ session('401-permission') }}`</code> permission.</p>
                    <div class="text-left">
                        <a class="btn btn-outline-primary" href="{{ route('user.dashboard.index') }}">Return to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
