@extends('errors._layouts.core')

@section('content')

    <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-6">
                <div class="clearfix">
                    <h1 class="float-left display-3 mr-4">500</h1>
                    <h4 class="pt-3">Sorry. It's Not You. It's Us.</h4>
                    <p class="text-muted">The page you are looking for is temporarily unavailable. Please try again later.</p>
                    <div class="text-left">
                        <a class="btn btn-outline-primary" href="{{ route('user.dashboard.index') }}">Return to Dashboard</a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
