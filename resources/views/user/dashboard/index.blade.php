@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">


            <div class="row">

                <div class="col-sm-6 col-lg-3">
                    <div class="card text-white bg-gradient-primary">
                        <div class="card-body card-body d-flex justify-content-between align-items-start">
                            <div>
                                <div class="text-value-lg">{{ $cloud_accounts_count }}</div>
                                <div>Cloud Accounts</div>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-transparent p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ri ri-xl ri-list-settings-line mt-0"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mt-3">
                                    <a class="dropdown-item" href="{{ route('user.cloud.index') }}">View Accounts</a>
                                    <a class="dropdown-item" href="{{ route('user.cloud.accounts.create') }}">Create Account</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>

                <div class="col-sm-12 col-lg-9">
                    <div class="card text-white bg-gradient-success">
                        <div class="card-body card-body d-flex justify-content-between align-items-start">
                            <div>
                                <div class="text-value-lg">N/A <small>(usage analytics not enabled)</small></div>
                                <div>Last 30 Days</div>
                            </div>
                            <div class="btn-group">
                                <a class="btn btn-transparent p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                    <i class="ri ri-xl ri-list-settings-line mt-0"></i>
                                </a>
                                <div class="dropdown-menu dropdown-menu-right mt-3">
                                    <a class="dropdown-item" href="{{ route('user.analytics.index') }}">View Usage</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>

            <div class="row">

                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex border-bottom-0">
                            <div class="card-title">
                                My Terraform Environments
                            </div>
                        </div>
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">ID</th>
                                    <th style="width: 30%;">Name</th>
                                    <th style="width: 15%;">Yesterday</th>
                                    <th style="width: 15%;">Last Week</th>
                                    <th style="width: 15%;">Last Month</th>
                                    <th style="width: 15%;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">
                                        <div class="alert alert-primary">
                                            You have not created any environments yet.
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>

            </div><!-- /row -->

            <div class="row">

                <div class="col-5">
                    <div class="card">
                        <div class="card-header border-bottom-0">
                            <div class="card-title">
                                Service Status
                            </div>
                        </div>
                        <table class="table mb-0">
                            <tr>
                                <td style="width: auto; vertical-align: middle;"><i class="ri ri-xl ri-tools-fill text-green"></i></td>
                                <td>
                                    <div class="text-green">Upcoming Maintenance</div>
                                    <div class="text-muted small">
                                        No maintenance is currently scheduled.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: auto; vertical-align: middle;"><i class="ri ri-xl ri-checkbox-circle-fill text-success"></i></td>
                                <td>
                                    <div class="text-success">Cloud Accounts (Beta)</div>
                                    <div class="text-muted small">
                                        All services are operating normally with beta stability.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: auto; vertical-align: middle;"><i class="ri ri-xl ri-calendar-event-fill text-primary"></i></td>
                                <td>
                                    <div class="text-primary">Terraform Environments</div>
                                    <div class="text-muted small">
                                        Services will be available in an upcoming release.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="width: auto; vertical-align: middle;"><i class="ri ri-xl ri-calendar-event-fill text-primary"></i></td>
                                <td>
                                    <div class="text-primary">Usage Analytics</div>
                                    <div class="text-muted small">
                                        Services will be available in an upcoming release.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;"><i class="ri ri-xl ri-alert-fill text-warning"></i></td>
                                <td>
                                    <span class="text-warning">AWS API</span>
                                    <div class="text-muted small">
                                        Services are operating however isolated problems have been reported with resource creation failing with 500 errors.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;"><i class="ri ri-xl ri-spam-2-fill text-danger"></i></td>
                                <td>
                                    <span class="text-danger">GCP API</span>
                                    <div class="text-muted small">
                                        Services are not available due to breaking changes with an API version upgrade.
                                    </div>
                                </td>
                            </tr>
                            <tr>
                                <td style="vertical-align: middle;"><i class="ri ri-xl ri-calendar-event-fill text-primary"></i></td>
                                <td>
                                    <span class="text-primary">GitLab API</span>
                                    <div class="text-muted small">
                                        Services will be available in an upcoming release.
                                    </div>
                                </td>
                            </tr>

                        </table>
                    </div>
                </div>

                <div class="col-7">
                    <div class="card">
                        <div class="card-header">
                            <div class="card-title">
                                Documentation and Help
                            </div>
                        </div>
                        <div class="card-body">

                        </div>
                    </div>
                </div>

            </div>

        </div>


    </div>
</div>

@endsection

@section('javascript')

@endsection
