@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <!-- Page Header -->
            <div class="d-flex mb-4">
                <div class="flex-grow-1">
                    <h3>Cloud Accounts</h3>
                </div>
                <div class="">
                    @if(in_array('user.cloud.accounts.create', $request->user()->permissions))
                        <a class="btn btn-primary" href="{{ route('user.cloud.accounts.create') }}">Create Individual Account</a>
                    @else
                        <button class="btn btn-secondary" disabled data-toggle="tooltip" data-placement="top" title="You do not have permission to create an account.">Create Individual Account</button>
                    @endif
                    <button class="btn btn-secondary" disabled data-toggle="tooltip" data-placement="top" title="Contact the administrator to create a group account using the CLI.">Create Group Account</button>
                </div>
            </div>
            <!-- END Page Header -->

            <div class="alert alert-warning">
                You can create an individual account using the <a href="{{ route('user.cloud.accounts.create') }}">web form</a>. Please contact the administrator to create a group account using the CLI.
            </div>

            @foreach($cloud_providers as $provider)

                <div class="card">
                    <div class="card-header border-bottom-0 d-flex">
                        <div class="flex-grow-1">
                            <h5 class="mb-0 font-weight-bold">{{ $provider->name }}</h5>
                        </div>
                        <div class="">
                            <code>Cloud Provider ID {{ $provider->short_id }}</code>
                        </div>
                    </div>
                    @if($provider->cloudAccounts->count() == 0)
                        <div class="card-body">
                            <div class="alert alert-info">
                                You do not currently have any accounts for this cloud provider.
                            </div>
                        </div>
                    @else
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th style="width: 15%;">Cloud Account ID</th>
                                    <th style="width: 35%;">Name</th>
                                    <th style="width: 35%;">Analytics</th>
                                    <th style="width: 15%; text-align: right;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($provider->cloudAccounts as $cloud_account)
                                    @if($provider->type == 'aws')
                                        <tr>
                                            <td><code>{{ $cloud_account->short_id }}</code></td>
                                            <td>
                                                @if($cloud_account->state == 'provisioning')
                                                    <i class="ri-time-line ri-lg text-warning" data-toggle="tooltip" data-placement="top" title="View Details" data-original-title="This account is currently being provisioned."></i>
                                                @elseif($cloud_account->state == 'provisioning-error')
                                                    <i class="ri-alert-fill ri-lg text-danger" data-toggle="tooltip" data-placement="top" title="View Details" data-original-title="This account failed to provision. {{ $cloud_account->api_meta_data['FailureReason'] }}"></i>
                                                @endif
                                                <a href="{{ route('user.cloud.accounts.show', ['id' => $cloud_account->id]) }}">{{ $cloud_account->slug }}</a>
                                                @if($cloud_account->cloudAccountUsers()->count() > 1)
                                                    <span class="badge badge-primary ml-1">Group</span>
                                                @endif

                                            </td>
                                            <td class="text-muted">Usage analytics coming soon</td>
                                            @if($cloud_account->state == 'provisioning')
                                                <td style="text-align: right; vertical-align: top;">
                                                    <span class="badge badge-warning text-white">Provisioning In Progress</span>
                                                </td>
                                            @elseif($cloud_account->state == 'provisioning-error')
                                                <td style="text-align: right; vertical-align: middle;">
                                                    <span class="badge badge-danger text-white">{{ $cloud_account->api_meta_data['FailureReason'] }}</span>
                                                </td>
                                            @else
                                                <td class="d-flex flex-row-reverse buttons" style="text-align: right; vertical-align: middle;">
                                                    <div class="btn-toolbar">
                                                        <div class="btn-group">
                                                            <a class="btn btn-outline-primary" style="display: flex;" data-toggle="tooltip" data-placement="top" title="View Details" data-original-title="View Details" href="{{ route('user.cloud.accounts.show', ['id' => $cloud_account->id]) }}">
                                                                <i class="ri-settings-3-line ri-lg"></i>
                                                            </a>
                                                            <a class="btn btn-outline-primary" style="display: flex;" target="_blank" data-toggle="tooltip" data-placement="top" title="Open Web Console" data-original-title="Open Web Console" href="https://{{ $cloud_account->api_meta_data['Id'] }}.signin.aws.amazon.com/console">
                                                                <i class="ri-external-link-line ri-lg"></i>
                                                            </a>
                                                            {{--
                                                            <a class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Connect using CLI" data-original-title="Connect using CLI" href="#">
                                                                <i class="ri-terminal-box-fill ri-lg"></i>
                                                            </a>
                                                            --}}
                                                        </div>
                                                    </div>
                                                </td>
                                            @endif
                                        </tr>
                                    @elseif($provider->type == 'gcp')
                                        <tr>
                                            <td>{{ $cloud_account->short_id }}</td>
                                            <td><a href="{{ route('user.cloud.accounts.show', ['id' => $cloud_account->id]) }}">{{ $cloud_account->slug }}</a></td>
                                            <td class="text-muted">Usage analytics coming soon</td>
                                            <td class="d-flex flex-row-reverse">
                                                <div class="btn-toolbar">
                                                    <div class="btn-group">
                                                        <a class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="View Details" data-original-title="View Details" href="#">
                                                            <i class="ri-settings-3-line ri-lg"></i>
                                                        </a>
                                                        <a class="btn btn-outline-primary" target="_blank" data-toggle="tooltip" data-placement="top" title="Open Web Console" data-original-title="Open Web Console" href="#">
                                                            <i class="ri-external-link-line ri-lg"></i>
                                                        </a>
                                                        {{--
                                                        <a class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Connect using CLI" data-original-title="Connect using CLI" href="#">
                                                            <i class="ri-terminal-box-fill ri-lg"></i>
                                                        </a>
                                                        --}}
                                                    </div>
                                                </div>
                                            </td>
                                        </tr>
                                    @endif
                                @endforeach
                            </tbody>
                        </table>

                    @endif

                </div>

            @endforeach

        </div>
    </div>

@endsection

@section('javascript')

@endsection
