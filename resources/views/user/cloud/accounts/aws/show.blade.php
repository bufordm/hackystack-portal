@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <!-- Page Header -->
            <div class="d-flex mb-4">
                <div class="flex-grow-1">
                    <h3 class="font-weight-bold"><i class="ri ri-xs ri-amazon-fill mr-2"></i>Cloud Account</h3>
                    @if($cloud_account->name != $cloud_account->slug)
                        <h5 class="mb-0">{{ $cloud_account->name }}</h5>
                        <code>{{ $cloud_account->slug }}-{{ $cloud_account->short_id }}</code>
                    @else
                        <h5 class="mb-0">{{ $cloud_account->name }}-{{ $cloud_account->short_id }}</h5>
                    @endif
                </div>
                <div class="">
                    @if($cloud_account->state == 'active')
                        <div class="btn-toolbar">
                            <div class="btn-group">
                                <a class="btn btn-outline-primary" target="_blank" href="https://{{ $cloud_account->api_meta_data['Id'] }}.signin.aws.amazon.com/console">
                                    <i class="ri-external-link-line ri-lg"></i> Open AWS Web Console
                                </a>
                                {{--
                                <a class="btn btn-outline-primary" data-toggle="tooltip" data-placement="top" title="Connect using CLI" data-original-title="Connect using CLI" href="#">
                                    <i class="ri-terminal-box-fill ri-lg"></i>
                                </a>
                                --}}
                                @if($cloud_account_user != null)
                                    <a class="btn btn-outline-primary" data-toggle="modal" data-target="#iam-credentials-{{ $cloud_account_user->short_id }}" href="#">
                                        <i class="ri-shield-user-fill ri-lg"></i> View IAM Credentials
                                    </a>
                                @endif
                            </div>
                        </div>
                    @endif
                </div>
            </div>
            <!-- END Page Header -->

            @if($cloud_account->state == 'active')
                <div class="row">
                    <div class="col-12">
                        <div class="card text-white bg-gradient-success">
                            <div class="card-body card-body d-flex justify-content-between align-items-start">
                                <div>
                                    <div class="text-value-lg">N/A <small>(usage analytics not enabled)</small></div>
                                    <div>Last 30 Days</div>
                                </div>
                                {{--
                                <div class="btn-group">
                                    <a class="btn btn-transparent p-0" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                        <i class="ri ri-xl ri-list-settings-line mt-0"></i>
                                    </a>
                                    <div class="dropdown-menu dropdown-menu-right mt-3">
                                        <a class="dropdown-item" href="{{ route('user.analytics.index') }}">View Usage</a>
                                    </div>
                                </div>
                                --}}
                            </div>
                        </div>
                    </div>
                </div>
            @elseif($cloud_account->state == 'provisioning')
                <div class="alert alert-warning page-alert alert-dismissible fade show" role="alert">
                    The Cloud Account is in the process of being provisioned. Please allow a few minutes for the cloud provider to complete the provisioning process.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>

            @elseif($cloud_account->state == 'provisioning-error')
                <div class="alert alert-danger page-alert alert-dismissible fade show" role="alert">
                    The cloud provider experienced an error when creating the Cloud Account. {{ array_key_exists('FailureReason', $cloud_account->api_meta_data) ? $cloud_account->api_meta_data['FailureReason'] : 'UNKNOWN_REASON' }}
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @elseif($cloud_account->state == 'provisioning-pending')
                <div class="alert alert-danger page-alert alert-dismissible fade show" role="alert">
                    This Cloud Account is in a pending state since the cloud provider credentials have not been properly configured. Please contact your system administrator.
                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                        <span aria-hidden="true">&times;</span>
                    </button>
                </div>
            @endif

            <!-- Terraform Environments -->
            <div class="row">
                <div class="col-12">
                    <div class="card">
                        <div class="card-header d-flex border-bottom-0">
                            <div class="card-title">
                                My Terraform Environments
                            </div>
                        </div>
                        <table class="table mb-0">
                            <thead>
                                <tr>
                                    <th style="width: 10%;">ID</th>
                                    <th style="width: 30%;">Name</th>
                                    <th style="width: 15%;">Yesterday</th>
                                    <th style="width: 15%;">Last Week</th>
                                    <th style="width: 15%;">Last Month</th>
                                    <th style="width: 15%;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <td colspan="6">
                                        <div class="alert alert-primary">
                                            You have not created any environments yet.
                                        </div>
                                    </td>
                                </tr>
                            </tbody>
                        </table>

                    </div>
                </div>
            </div>
            <!-- END Terraform Environments -->

            <div class="row">
                <div class="col-6">
                    <!-- Cloud Account Meta Data -->
                    <div class="card">
                        <div class="card-header d-flex">
                            <div class="flex-fill flex-grow-1 card-title">
                                Meta Data
                            </div>
                            <div class="flex-fill text-right">
                                <code>{{ $cloud_account->short_id }}</code>
                            </div>
                        </div>
                        <div class="card-body">

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Cloud Provider</label>
                                <div class="col-md-8 col-form-label">
                                    {{ $cloud_account->cloudProvider->slug }}-{{ $cloud_account->cloudProvider->short_id }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Cloud Realm</label>
                                <div class="col-md-8 col-form-label">
                                    {{ $cloud_account->cloudRealm->slug }}-{{ $cloud_account->cloudRealm->short_id }}
                                </div>
                            </div>

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Organization Unit</label>
                                <div class="col-md-8 col-form-label">
                                    {{ $cloud_account->cloudOrganizationUnit->slug }}-{{ $cloud_account->cloudOrganizationUnit->short_id }}
                                </div>
                            </div>

                            @if($cloud_account->status == 'active')
                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">AWS Account ID</label>
                                    <div class="col-md-8 col-form-label">
                                        {{ array_key_exists('Id', $cloud_account->api_meta_data) ? $cloud_account->api_meta_data['Id'] : 'Unknown' }}
                                    </div>
                                </div>

                                <div class="form-group row">
                                    <label class="col-md-4 col-form-label">Root Email</label>
                                    <div class="col-md-8 col-form-label">
                                        {{ array_key_exists('Email', $cloud_account->api_meta_data) ? $cloud_account->api_meta_data['Email'] : 'Unknown' }}
                                    </div>
                                </div>
                            @endif

                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">Created</label>
                                <div class="col-md-8 col-form-label">
                                    {{ $cloud_account->created_at->format('Y-m-d H:i:s T') }}
                                </div>
                            </div>
                            <div class="form-group row">
                                <label class="col-md-4 col-form-label">State</label>
                                <div class="col-md-8 col-form-label">
                                    {{ Str::title($cloud_account->state) }}
                                </div>
                            </div>
                        </div>
                    </div>
                    <!-- END Cloud Account Meta Data -->
                </div>
                <div class="col-6">
                    <!-- Cloud Account Users -->
                    <div class="card">
                        <div class="card-header d-flex border-bottom-0">
                            <div class="flex-fill flex-grow-1 card-title">
                                Cloud Account Users
                            </div>
                            <div class="flex-fill text-right">
                                &nbsp;
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width: 40%;">Username</th>
                                    <th style="width: 40%;">Roles</th>
                                    <th style="width: 20%;">&nbsp;</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cloud_account->cloudAccountUsers as $cloud_account_user_row)
                                    <tr>
                                        <td>{{ $cloud_account_user_row->username }}</td>
                                        <td class="small">
                                            @foreach($cloud_account_user_row->cloudAccountUserRoles as $cloud_account_user_role)
                                                {{ $cloud_account_user_role->cloudAccountRole->api_name }}
                                            @endforeach
                                        </td>
                                        <td class="text-right">
                                            @if($cloud_account_user_row->state == 'active')
                                                <span class="badge badge-success text-white">Active</span>
                                            @elseif($cloud_account_user_row->state == 'provisioning-pending')
                                                <span class="badge badge-warning text-white">Provisioning Pending</span>
                                            @elseif($cloud_account_user_row->state == 'provisioning-error')
                                                <span class="badge badge-danger text-white">Provisioning Error</span>
                                            @elseif($cloud_account_user_row->state == 'deprovisioned')
                                                <span class="badge badge-secondary">Deprovisioned</span>
                                            @else
                                                <span class="badge badge-muted">{{ $cloud_account_user_row->state }}</span>
                                            @endif
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END Cloud Account Users -->
                    <!-- Cloud Account Roles -->
                    <div class="card">
                        <div class="card-header d-flex border-bottom-0">
                            <div class="flex-fill flex-grow-1 card-title">
                                Cloud Account Roles
                            </div>
                            <div class="flex-fill text-right">
                                &nbsp;
                            </div>
                        </div>
                        <table class="table">
                            <thead>
                                <tr>
                                    <th style="width: 40%;">Role Name</th>
                                    <th style="width: 60%;">Users</th>
                                </tr>
                            </thead>
                            <tbody>
                                @foreach($cloud_account->cloudAccountRoles as $cloud_account_role)
                                    <tr>
                                        <td>{{ $cloud_account_role->api_name }}</td>
                                        <td class="small">
                                            @foreach($cloud_account_role->cloudAccountUserRoles as $cloud_account_user_role)
                                                {{ $cloud_account_user_role->cloudAccountUser->username }}<br />
                                            @endforeach
                                        </td>
                                    </tr>
                                @endforeach
                            </tbody>
                        </table>
                    </div>
                    <!-- END Cloud Account Roles -->
                </div>
            </div><!-- END row -->

        </div>
    </div>

    {{-- If authenticated user has a cloud account user for this account --}}
    @if($cloud_account_user != null)

        {{-- Include the modal and pass variable through with user credentials --}}
        @include('user.cloud.accounts.aws._modals.iam-credentials', [
            'cloud_account_user' => $cloud_account_user
        ])

    @endif

@endsection

@section('javascript')

@endsection
