@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <!-- Page Header -->
            <div class="d-flex mb-4">
                <div class="flex-grow-1">
                    <h3>Cloud Account</h3>
                    <h5>{{ $cloud_account->name }}</h5>
                </div>
                <div class="">
                    {{--
                    @if(in_array('user.cloud.accounts.create', $request->user()->permissions))
                        <a class="btn btn-primary" href="{{ route('user.cloud.accounts.create') }}">Create Account</a>
                    @else
                        <button class="btn btn-secondary" disabled data-toggle="tooltip" data-placement="top" title="You do not have permission to create an account.">Create Account</button>
                    @endif

                    --}}
                </div>
            </div>
            <!-- END Page Header -->

            <!-- Cloud Account Meta Data -->
            <div class="card">
                <div class="card-header d-flex">
                    <div class="flex-fill flex-grow-1 card-title">
                        Meta Data
                    </div>
                    <div class="flex-fill text-right">
                        <code>{{-- Str::title($auth_user->authProvider->slug) --}}</code>
                    </div>
                </div>
                <div class="card-body">
                    
                </div>
            </div>
            <!-- END Cloud Account Meta Data -->

        </div>
    </div>

@endsection

@section('javascript')

@endsection
