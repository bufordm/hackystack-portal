@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">

            <!-- Page Header -->
            <div class="d-flex">
                <div class="flex-grow-1">
                    <h3>Create a Cloud Account</h3>
                </div>
                <div class="">
                    <a class="btn btn-outline-dark mr-2" href="{{ route('user.cloud.index') }}">Cancel and Go Back</a>
                </div>
            </div>
            <!-- END Page Header -->

            <div class="row mt-4">
                <div class="col-8">

                    <!-- Cloud Provider -->
                    <div class="card">
                        <div class="card-body">
                            <form method="GET" action="{{ route('user.cloud.accounts.create') }}">
                                <div class="form-group">
                                    <label class="font-weight-bold" for="cloud_provider_id">Cloud Provider</label>
                                    @error('cloud_provider_id')
                                        <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                            {{ $message }}
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @enderror
                                    @if($cloud_providers->count() > 0)
                                        <select class="form-control" id="cloud_provider_id" name="cloud_provider_id" onchange="this.form.submit()">
                                            <option {!! $request->has('cloud_provider_id') ? '' : 'selected="selected"' !!} disabled="disabled">Select...</option>
                                            @foreach($cloud_providers as $provider)
                                                <option {!! $request->has('cloud_provider_id') && $request->cloud_provider_id == $provider->id ? 'selected="selected"' : '' !!} value="{{ $provider->id }}">{{ $provider->slug.'-'.$provider->short_id }} {{ $provider->api_meta_data != null ? '(Master Account '.$provider->api_meta_data['MasterAccountId'].')' : '' }}</option>
                                            @endforeach
                                        </select>
                                    @else
                                        <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                            No cloud providers have been configured. Please contact your system administrator.
                                            <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                    @endif
                                </div>
                            </form>
                        </div>
                    </div>
                    <!-- END Cloud Provider -->

                    <!-- Cloud Organization Unit -->
                    @if($request->has('cloud_provider_id'))
                        <div class="card">
                            <div class="card-body">
                                <form method="GET" action="{{ route('user.cloud.accounts.create') }}">
                                    <input type="hidden" name="cloud_provider_id" value="{{ $request->cloud_provider_id }}">
                                    <div class="form-group">
                                        <label class="font-weight-bold" for="cloud_provider_id">Organization Unit</label>
                                        @error('cloud_organization_unit_id')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {{ $message }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @enderror
                                        @if($sandbox_cloud_organization_units->count() > 0)
                                            <select class="form-control" id="cloud_organization_unit_id" name="cloud_organization_unit_id" onchange="this.form.submit()">
                                                <option {!! $request->has('cloud_organization_unit_id') ? '' : 'selected="selected"' !!} disabled="disabled">Select...</option>
                                                @foreach($sandbox_cloud_organization_units as $organization_unit)
                                                    @if(in_array(str_replace('-sandbox', '', 'department-'.$organization_unit->slug), $request->user()->groups))
                                                        <option value="{{ $organization_unit->id }}">{{ $organization_unit->slug.'-'.$organization_unit->short_id }}</option>
                                                    @endif
                                                @endforeach
                                            </select>
                                        @else
                                            <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                                No cloud organization units have been configured. Please contact your system administrator.
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @endif
                                    </div>
                                </form>
                            </div>
                        </div>
                    @endif
                    <!-- END Cloud Organization Unit -->

                    @if($request->has('cloud_provider_id') && $request->has('cloud_organization_unit_id') && $cloud_account == null)

                        <form method="POST" action="{{ route('user.cloud.accounts.store') }}">
                            @csrf

                            <!-- Account Name -->
                            <div class="card">
                                <div class="card-body">

                                    <input type="hidden" name="cloud_provider_id" value="{{ $request->cloud_provider_id }}">
                                    <input type="hidden" name="cloud_organization_unit_id" value="{{ $request->cloud_organization_unit_id }}">
                                    <input type="hidden" name="account_name_prefix" value="{{ $cloud_organization_unit->slug }}">
                                    <div class="form-group">
                                        <label class="font-weight-bold" for="account_name">Account Name</label>
                                        @error('account_name_suffix')
                                            <div class="alert alert-danger alert-dismissible fade show" role="alert">
                                                {{ $message }}
                                                <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                                    <span aria-hidden="true">&times;</span>
                                                </button>
                                            </div>
                                        @enderror
                                        {{--
                                        <div class="input-group">
                                            <div class="input-group-prepend">
                                                <span class="input-group-text" id="account_name_prepend">{{ $cloud_organization_unit->slug }}-</span>
                                            </div>
                                            <input type="text" class="form-control" readonly="readonly" id="account_name_suffix" aria-describedby="account_name_prepend" name="account_name_suffix" maxlength="{{ 50 - strlen($cloud_organization_unit->slug) }}" value="{{ $request->user()->user_handle }}">
                                        </div>
                                        --}}
                                        <input type="hidden" name="account_name_suffix" value="{{ $request->user()->user_handle }}">
                                        <input type="text" readonly class="form-control-plaintext" id="account_name" value="{{ $cloud_organization_unit->slug }}-{{ $request->user()->user_handle }}">
                                    </div>

                                </div>
                            </div>
                            <!-- END Account Name -->

                            <!-- Form Action Buttons -->
                            <div class="card">
                                <div class="card-body text-right">
                                    <a class="btn btn-outline-dark mr-2" href="{{ route('user.cloud.index') }}">Cancel and Go Back</a>
                                    <button type="submit" class="btn btn-success">Create Account</button>
                                </div>
                            </div>
                            <!-- Form Action Buttons -->

                        </form>

                    @elseif($request->has('cloud_provider_id') && $request->has('cloud_organization_unit_id') && $cloud_account != null)

                        <div class="card">
                            <div class="card-body">
                                <div class="alert alert-primary alert-dismissible fade show" role="alert">
                                    <div class="mb-2">A Cloud Account already exists with that name ({{ $cloud_account->slug }}).</div>
                                    <a class="btn btn-primary" href="{{ route('user.cloud.accounts.show', ['id' => $cloud_account->id]) }}">View account</a>
                                    <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                                        <span aria-hidden="true">&times;</span>
                                    </button>
                                </div>
                            </div>
                        </div>

                    @endif

                </div>
                <div class="col-4">

                    <!-- Documentation -->
                    <div class="card">
                        <div class="card-header">
                            <h5 class="mb-0">Documentation</h5>
                        </div>
                        <div class="card-body">
                            <p>
                                When creating a Cloud Account, you need to select which cloud provider the account will be provisioned in.<br />
                                <br />
                                You will also choose an organization unit based on the groups that your user account is associated with.<br />
                                <br />
                                When creating an individual account, the account name is based on your email address handle. When creating a group account, you can specify an appropriate alpha-dash name to append to the organization unit (group) name.<br />
                                <br />
                                After clicking the <strong>Create Account</strong> button, please allow a few minutes for your new account to be provisioned in the cloud provider.
                            </p>
                        </div>
                    </div>
                    <!-- END Documentation -->

                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
