@extends('user._layouts.core')

@section('content')

    <div class="container-fluid">
        <div class="fade-in">
            <div class="card">
                <div class="card-header">
                    <div class="card-title">My Groups</div>
                </div>
                <div class="card-body">
                    {{-- TODO --}}
                </div>
            </div>

            <div class="card">
                <div class="card-header">
                    <div class="card-title">Available Groups</div>
                </div>
                <div class="card-body">
                    <div class="list-group">
                        @foreach($auth_groups as $group)

                            <div class="list-group-item">
                                <span class="badge badge-secondary">{{ $group->namespace }}</span>
                                {{ $group->name }}
                            </div>

                        @endforeach
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('javascript')

@endsection
