# HackyStack Portal Changelog

## Disclaimer

* This Changelog is programatically generated using the [developer:generate-changelog](https://gitlab.com/hackystack/hackystack-portal/-/blob/main/app/Console/Commands/Developer/GenerateChangelog.php) command.
* The nomencalture standards, syntax and spelling of merge request and commit descriptions will vary and are considered best effort by the project contributors and maintainers.
* Any erroneous work-in-progress (WIP) commit messages are removed prior to adding release notes to the Changelog, however the count of commits is not modified. Modifications are the discretion of the project maintainers.
* The `changelog-type::___` scoped label is applied to merge requests to automatically categorize merge requests and related commits. Some merge requests may apply to multiple categories, however we apply the category based on the predominant category of commits in the merge request.
* The `New Features` and `Limitations` sections for each release are written in the milestone description by the project maintainers and are designed to provide a high-level overview of the release notes. For technical details, you are encouraged to review the descriptions of the merge requests and commits.
* The `Merge Requests` and `Commits` sections for each release are sorted alphabetically by category and then by description. Note that capitalized letters and lowercase letters are sorted as separate ranges due to the Laravel collections sort functionality.

<!-- New versions should be added here. -->

## 0.2.0
* **URL:** https://gitlab.com/hackystack/hackystack-portal/-/milestones/8
* **Release Date:** 2021-01-28

### Overview
This is the second release of HackyStack that fixes bugs with AWS API usage, provides stability for creating individual AWS accounts, adds features for creating AWS group accounts, and additional backend and CLI features.

### New Features
* AWS Group Accounts - You can create an AWS account using the CLI and create multiple Cloud Account Users for the AWS account that allow multiple users to share the same infrastructure resources.
* Additional Cloud CLI Commands - We have added additional CLI commands for Cloud Account Users, Cloud Account Roles
* Early iteration of Dockerfile for easy installation of HackyStack Portal. This is for development and testing purposes and is not considered stable yet.

### Limitations
* `High` [#38](https://gitlab.com/hackystack/hackystack-portal/-/issues/38) - AWS Group Accounts - The creation is only available using the CLI since we do not have an account naming and user invitation/management UI created yet.
* `High` [#4](https://gitlab.com/hackystack/hackystack-portal/-/issues/4) - Local authentication form has not been implemented
* `Medium` [#5](https://gitlab.com/hackystack/hackystack-portal/-/issues/5) - GCP API has not been fully implemented due to GCP Resource Manager API limitations
* `Medium` [#6](https://gitlab.com/hackystack/hackystack-portal/-/issues/6) - Logging has only been partially implemented on `Auth` services
* `Medium` [#7](https://gitlab.com/hackystack/hackystack-portal/-/issues/7) - Logging has not been implemented on `Cloud` services
* `Low` [#8](https://gitlab.com/hackystack/hackystack-portal/-/issues/8) - Queued jobs for expiration have not be implementation

### Merge Requests (21)
* `backend` Add AWS root email dynamic generation capability - !85 - @jeffersonmartin
* `backend` Add Bugsnag API Key to .env.example - !88 - @jeffersonmartin
* `backend` Add Vendor/Aws/IamUserService createLoginProfile() method to provisioning of Cloud Accounts - !101 - @jeffersonmartin
* `backend` Add support for Bugsnag log provider - !87 - @jeffersonmartin
* `backend` Add support for dynamic tagging of cloud accounts - !84 - @jeffersonmartin
* `backend` Fix AWS API race condition and rate limits when importing AWS Organization Units - !89 - @jeffersonmartin
* `backend` Fix AWS IAM User password generation to enforce character type requirements - !105 - @jeffersonmartin
* `backend` Fix Cloud Realm root organization unit ID missing in database rows - !96 - @jeffersonmartin
* `backend` Fix Vendor/AWS/IAMUserService createLoginProfile() to remove typo in parameters - !102 - @jeffersonmartin
* `backend` Fix cloud account provisioning recursive background job to fail after number of attempts - !93 - @jeffersonmartin
* `backend` Fix config/hackystack-*.php.example files to be empty and move GitLab examples to `.example-gitlab` - !95 - @jeffersonmartin
* `backend` Update Cloud Account User background job to fix race condition and add envoy.blade.php example - !90 - @jeffersonmartin
* `backend` Update env examples with AWS root email variables - !86 - @jeffersonmartin
* `cli` Add Changelog generator CLI command - !106 - @jeffersonmartin
* `cli` Add cloud-account:* console commands - !97 - @jeffersonmartin
* `cli` Fix cloud-account-user-role:create to remove deprecated output after creation - !99 - @jeffersonmartin
* `cli` Fix cloud-account-user:create and AuthUser model to use correct namespace for CloudAccountUser model - !98 - @jeffersonmartin
* `cli` Update background job logs for cloud accounts - !91 - @jeffersonmartin
* `cli` Update cloud-account-* CLI command with improved outputs - !103 - @jeffersonmartin
* `other` Add DOCKERFILE for easy installation of HackyStack Portal - !94 - @jsandlin
* `user-ui` Update UI for creating and viewing group AWS accounts - !100 - @jeffersonmartin

### Commits (116)
* `backend` Add envoy.blade.php.example - 78de92cf - !90
* `backend` Add sleep functions to Console/Commands/Cloud/CloudRealmImportConfigFile - 740c6e2b - !89
* `backend` Fix Vendor/Aws/IamUserService deprecated comments in create() method - b68cd1a7 - !101
* `backend` Initial commit of empty config/hackystack-auth-groups.php.example - f745a086 - !95
* `backend` Initial commit of empty config/hackystack-auth-provider-fields.php.example - 15625f89 - !95
* `backend` Initial commit of empty config/hackystack-cloud-realms.php.example - 3bdf8e22 - !95
* `backend` Merge branch '12-add-aws-root-email-dynamic-generation-capability' of gitlab.com:hackystack/hackystack-portal into 12-add-aws-root-email-dynamic-generation-capability - e1a409e5 - !86
* `backend` Refactor Console/Commands/Cloud/CloudAccountCreateStatusRefresh to add conditional checks for state when provisioning Cloud Account Users - 9a36c5f2 - !93
* `backend` Rename config/hackystack-auth-groups.php.example to .example-gitlab - 0c976497 - !95
* `backend` Rename config/hackystack-auth-provider-fields.php.example to .example-gitlab - 9bc3d730 - !95
* `backend` Rename config/hackystack-cloud-realms.php.example to .example-gitlab - ec9d663f - !95
* `backend` Update .env.example to add AWS root email variables - 18cd3907 - !86
* `backend` Update .env.example to add BUGSNAG_API_KEY - 6b8ba76f - !88
* `backend` Update .gitignore to add /config/hackystack.php - d34916d0 - !90
* `backend` Update .gitignore to add envoy.blade.php - c0874267 - !90
* `backend` Update CloudAccountService to add AWS root email method with plus concatenation support - 7d5bfc72 - !85
* `backend` Update CloudAccountService to add AWS root email method with plus concatenation support - 7d5bfc72 - !86
* `backend` Update CloudAccountService to add conditional State when provisioning an account to catch instance vs delayed variations - bd3d3f0e - !90
* `backend` Update CloudAccountService to add generateTags method - 22473095 - !84
* `backend` Update CloudAccountService to refactor provision method with tags method - 0532f05a - !84
* `backend` Update CloudAccountUserService provision() method to invoke createLoginProfile() method after IAM user is created - 2e662a20 - !101
* `backend` Update CloudAccountUserService to refactor password generation using complexity requirements - c1186732 - !105
* `backend` Update CloudRealmService store method to add cloud_organization_unit_id_root after organization unit is created - b0360baf - !96
* `backend` Update Console/Commands/Cloud/CloudAccountCreateStatusRefresh to refactor cloud account user provisioning status conditional - 4efcbcfd - !90
* `backend` Update Console/Commands/HackystackInstall to add prompts for AWS root email variables - d1fde859 - !86
* `backend` Update Vendor/Aws/IamUserService to remove typo $id parameter from method. - d0103ab6 - !102
* `backend` Update composer dependencies to add hackzilla/password-generator package - fa6865fb - !105
* `backend` Update composer to add bugsnag package, update guzzle version dependency, and rename deprecated faker package - c28bd7b8 - !87
* `backend` Update config/app to add Bugsnag Service Provider - 8b321b0e - !87
* `backend` Update config/hackystack to add accounts.aws.tags and accounts.gcp.labels arrays - 416c5bfd - !84
* `backend` Update config/hackystack to add root_email nested array with method (future expansion), plus_prefix and plus_domain - 46c8c541 - !85
* `backend` Update config/hackystack to add root_email nested array with method (future expansion), plus_prefix and plus_domain - 46c8c541 - !86
* `backend` Update config/hackystack.php to add AWS IAM User Password Complexity Requirements - e916c501 - !105
* `backend` Update config/hackystack.php.example to add AWS IAM User Password Complexity Requirements - 37c5701b - !105
* `backend` Update config/hackystack.php.example to add accounts.aws.tags and accounts.gcp.labels arrays - 9c647d48 - !84
* `backend` Update config/logging to add bugsnag driver and add to stack channel - 3ef2e390 - !87
* `cli` Add cloud-account-role:create CLI command - bff22b3d - !97
* `cli` Add cloud-account-user-role:create CLI command - d7993a53 - !97
* `cli` Add cloud-account-user-role:delete CLI command - a0fcec87 - !97
* `cli` Add cloud-account-user:create CLI command - 52d282dc - !97
* `cli` Add cloud-account-user:delete CLI command - 541a6e7c - !97
* `cli` Add cloud-account-user:get CLI command - f1f48219 - !97
* `cli` Add cloud-account-user:list CLI command - ccddcf2b - !97
* `cli` Add coud-account-role:get CLI command - 5190f13c - !97
* `cli` Add developer:generate-changelog CLI command - be6bfc02 - !106
* `cli` Fix CloudAccountUserService to rename comment typos Organization Unit to IAM User - e77ef8e8 - !97
* `cli` Fix Models/Auth/AuthUser to add namespace to cloudAccountUsers() relationship - ea675e01 - !98
* `cli` Fix Models/Auth/AuthUser to add namespace to cloudAccounts() relationship - 3b129c31 - !98
* `cli` Fix Models/Cloud/CloudAccountUser typo with cloud_account_group_id in fillable array - db8c7ffc - !97
* `cli` Fix cloud-account-user-role:create CLI command to rename table header typo Group Slug to Cloud Accounts - e32a8385 - !103
* `cli` Fix cloud-account-user-role:create to remove the deprecated cloud-account-user-role:get output - 5f263a98 - !99
* `cli` Fix cloud-account-user:create CLI command to rename table header typo Group Slug to Cloud Accounts - 5dee522f - !103
* `cli` Update .env.example to add HackyStack Developer variables for GitLab API - cc9e24d2 - !106
* `cli` Update CloudAccountUserService `deprovision()` method to implement AWS IAM User Service deleteUser SDK - e533ab64 - !97
* `cli` Update Console/Commands/Cloud/CLoudAccountCreateStatusRefresh to refactor comment outputs to be log friendly - dfd5e63f - !91
* `cli` Update Console/Commands/Cloud/CloudAccountGet to add cloud account groups, users and user roles relationships - df4695c1 - !97
* `cli` Update Console/Commands/Cloud/CloudAccountGet to fix comments and condense relationship outputs - 527c0f5d - !97
* `cli` Update Models/Auth/AuthUser to add cloudAccountUsers relationship - d692c0dd - !97
* `cli` Update Models/Cloud/CloudAccount to add cloudAccountGroupsRoles relationship - 335b8d87 - !97
* `cli` Update Models/Cloud/CloudAccount to add cloudAccountUsersRoles relationship - 673df042 - !97
* `cli` Update Models/Cloud/CloudAccountRole to add cloudAccountGroups relationship - 29874800 - !97
* `cli` Update Models/Cloud/CloudAccountRole to add cloudAccountUsers relationship - 3db8c692 - !97
* `cli` Update Vendor/AWS/IAMUserService create() method to remove comment block references to recently refactored createLoginProfile method - 6f0677c4 - !97
* `cli` Update Vendor/AWS/IAMUserService create() method to return an array of API returned values - 91c716c0 - !97
* `cli` Update Vendor/AWS/IAMUserService createLoginProfile method to refactor returned array (breaking change) - 1b48c411 - !97
* `cli` Update Vendor/AWS/IAMUserService to implement createAccessKey() method - 85ff9e69 - !97
* `cli` Update Vendor/AWS/IAMUserService to implement delete() method - 4ea7a337 - !97
* `cli` Update Vendor/AWS/IAMUserService to implement get() method - c0b4a8d1 - !97
* `cli` Update `cloud-account-user:create` CLI command to move instructions for cloud-account-user-role:create to end of console output. - 3360c9f2 - !103
* `cli` Update app/Console/Kernel to add Commands/Developer namespace - 3fd18734 - !106
* `cli` Update app/Console/Kernel to add log output to scheduled commands - 21beb0a2 - !91
* `cli` Update cloud-account-role:create CLI command to move instructions for cloud-account-user-role:create to end of console output - 6dc16cd8 - !103
* `cli` Update cloud-account-user-role:create CLI command to filter list of CloudAccountUsers values to only include users that aren't associated with the CloudAccountRole yet. - d8903641 - !103
* `cli` Update cloud-account-user:create CLI command to add --with-password argument after user completion to show IAM password in console output. - 9727c085 - !103
* `cli` Update cloud-account-user:get CLI command to add `--with-password` argument - 35b49e56 - !97
* `cli` Update cloud-account-user:get CLI command to add all remaining database fields and relationships to console output - 2fab5b9d - !97
* `cli` Update cloud-account:create CLI command to add instructions above the slug about the naming conventions, include a list of sample account names that have been previously created (both individual and group). - 8365f767 - !103
* `cli` Update config/hackystack.php and hackystack.php.example with the GitLab API variables for developers - 9ba8bd7a - !106
* `cli` Update dev_env/mysql/Dockerfile - 43b177fd - !97
* `user-ui` Fix views/user/cloud/accounts/aws/show list of cloud account roles to add line breaks - ac100fba - !100
* `user-ui` Fix views/user/cloud/accounts/aws/show looped cloud account users variable that broke the IAM credential modal window - 01febe5f - !100
* `user-ui` Update views/user/cloud/index to add `group` badge on shared accounts in table - 32a88987 - !100
* `user-ui` Update views/user/cloud/index to adjust table column width for longer account names - 5c233b0c - !100
* `user-ui` Update views/user/cloud/index to rename Create Account button to Create Individual Account - 60797e27 - !100
* `user-ui` Update views/user/cloud/index to rename Create Group button (disabled) and alert message with CLI instructions - 3835e8a9 - !100

## 0.1.0-beta
* **URL:** https://gitlab.com/hackystack/hackystack-portal/-/milestones/1
* **Release Date:** 2020-11-05

### Overview
This is the first release of HackyStack that is used for testing in a live environment.

### New Features
* Admin CLI console with `php hacky` (renamed `php artisan`)
  * Provides full functionality for administering `auth` and `cloud` models
  * Easy installation script with `php hacky hackystack:install`
* New user interface using `coreui.io` free Bootstrap theme
  * User profile
  * User dashboard
  * Create AWS cloud accounts for individual users
  * Create AWS cloud account users (IAM users)
  * Create AWS cloud account roles (IAM permission policies)
* Okta authentication using Laravel Socialite
  * Auto-attach with authentication groups based on Okta metadata
  * Auto-filled authentication user database fields based on Okta metadata
* AWS API implemented for creating AWS organization units, accounts, IAM users and permission policies
* GitLab API scaffolding implementation for infrastructure-as-code group creation

### Limitations
* `High` [#4](https://gitlab.com/hackystack/hackystack-portal/-/issues/4) - Local authentication form has not been implemented
* `Medium` [#5](https://gitlab.com/hackystack/hackystack-portal/-/issues/5) - GCP API has not been fully implemented due to GCP Resource Manager API limitations
* `Medium` [#6](https://gitlab.com/hackystack/hackystack-portal/-/issues/6) - Logging has only been partially implemented on `Auth` services
* `Medium` [#7](https://gitlab.com/hackystack/hackystack-portal/-/issues/7) - Logging has not been implemented on `Cloud` services
* `Low` [#8](https://gitlab.com/hackystack/hackystack-portal/-/issues/8) - Queued jobs for expiration have not be implementation
* `Low` [#9](https://gitlab.com/hackystack/hackystack-portal/-/issues/9) - Group accounts have not been implemented. Some UI pages have static implementation for individual account.

### Merge Requests (82)
* `backend` Add BaseModel - !10 - @jeffersonmartin
* `backend` Add auth providers to auth base db seed - !22 - @jeffersonmartin
* `backend` Add base db seed for auth package - !9 - @jeffersonmartin
* `backend` Add cloud account role and group to auth base seed - !75 - @jeffersonmartin
* `backend` Add custom attributes to auth user model - !77 - @jeffersonmartin
* `backend` Add database migration for cloud keypairs - !17 - @jeffersonmartin
* `backend` Add database migrations for cloud package - !15 - @jeffersonmartin
* `backend` Add git meta data and provision flag to cloud models and migrations - !65 - @jeffersonmartin
* `backend` Add gitlab api service scaffolding - !67 - @jeffersonmartin
* `backend` Add hackystack config file - !4 - @jeffersonmartin
* `backend` Add migrations for auth package - !6 - @jeffersonmartin
* `backend` Add models for auth package - !7 - @jeffersonmartin
* `backend` Add models for cloud package - !16 - @jeffersonmartin
* `backend` Add parent org unit string to cloud providers - !74 - @jeffersonmartin
* `backend` Add services for auth package - !8 - @jeffersonmartin
* `backend` Add services for cloud package - !29 - @jeffersonmartin
* `backend` Fix Auth Provider encrypted fields - !57 - @jeffersonmartin
* `backend` Fix bugs in cloud models - !31 - @jeffersonmartin
* `backend` Fix cloud organization unit service bugs - !78 - @jeffersonmartin
* `backend` Fix migrations/create_cloud_realms_table to remove unique constraint from slug - !40 - @jeffersonmartin
* `backend` Fix model namespace refactor typos - !18 - @jeffersonmartin
* `backend` Fix namespace for cloud account child models - !38 - @jeffersonmartin
* `backend` Fix typo in auth group user service - !20 - @jeffersonmartin
* `backend` Fix typos and update comment blocks - !44 - @jeffersonmartin
* `backend` Fix typos in auth models and services - !23 - @jeffersonmartin
* `backend` Fix typos in cloud models - !28 - @jeffersonmartin
* `backend` Implement Laravel fresh scaffolding - !2 - @jeffersonmartin
* `backend` Implement aws account provisioning - !76 - @jeffersonmartin
* `backend` Implement aws api for organizations - !70 - @jeffersonmartin
* `backend` Implement cloud accounts aws iam provisioning - !79 - @jeffersonmartin
* `backend` Implement gcp folder provisioning for cloud realms - !69 - @jeffersonmartin
* `backend` Refactor auth sso provider config vars into database fields - !24 - @jeffersonmartin
* `backend` Refactor default tenant implementation - !55 - @jeffersonmartin
* `backend` Update auth group model to add expires at to pivot relations - !19 - @jeffersonmartin
* `backend` Update auth tenant with git meta data - !66 - @jeffersonmartin
* `backend` Update auth users to add sso provider meta data - !27 - @jeffersonmartin
* `backend` Update cloud realm to add root organization unit relationship - !39 - @jeffersonmartin
* `backend` Update composer to include aws google yaml - !43 - @jeffersonmartin
* `backend` Update config/hackystack-auth-groups-example to add providers child arrays - !50 - @jeffersonmartin
* `backend` Update database/migrations/auth_* to add comment('encrypted') on fields - !81 - @jeffersonmartin
* `backend` Update factories and seeders - !51 - @jeffersonmartin
* `cli` Add Commands/AuthProviderEdit - !47 - @jeffersonmartin
* `cli` Add Commands/HackystackInstall - !48 - @jeffersonmartin
* `cli` Add auth provider groups and fields - !52 - @jeffersonmartin
* `cli` Add auth role permission commands - !61 - @jeffersonmartin
* `cli` Add cli commands for auth group roles and users - !21 - @jeffersonmartin
* `cli` Add cli commands for auth groups - !11 - @jeffersonmartin
* `cli` Add cli commands for auth providers - !25 - @jeffersonmartin
* `cli` Add cli commands for auth roles - !14 - @jeffersonmartin
* `cli` Add cli commands for auth tenants - !13 - @jeffersonmartin
* `cli` Add cli commands for auth users - !26 - @jeffersonmartin
* `cli` Add cli commands for cloud accounts - !36 - @jeffersonmartin
* `cli` Add cli commands for cloud billing accounts - !34 - @jeffersonmartin
* `cli` Add cli commands for cloud organization units - !37 - @jeffersonmartin
* `cli` Add cli commands for cloud providers - !32 - @jeffersonmartin
* `cli` Add cli commands for cloud realms - !33 - @jeffersonmartin
* `cli` Add hackystack install commands - !58 - @jeffersonmartin
* `cli` Create namespaces for console commands - !53 - @jeffersonmartin
* `cli` Fix bugs in auth commands - !30 - @jeffersonmartin
* `cli` Fix cli command for cloud realm delete - !35 - @jeffersonmartin
* `cli` Fix cli commands for auth groups - !12 - @jeffersonmartin
* `cli` Fix cloud realm import config file command - !64 - @jeffersonmartin
* `cli` Implement cloud provider api credentials - !63 - @jeffersonmartin
* `cli` Refactor Auth Group import from config file - !41 - @jeffersonmartin
* `cli` Refactor cloud realm import - !42 - @jeffersonmartin
* `cli` Update Commands/AuthGroupImportConfigFile with refactored providers array - !49 - @jeffersonmartin
* `cli` Update auth provider commands - !62 - @jeffersonmartin
* `cli` Update cloud services with git group provisioning - !68 - @jeffersonmartin
* `cli` Update console command outputs to abbreviate parent relationships - !56 - @jeffersonmartin
* `cli` Update console commands with reduced verbosity with no interaction - !73 - @jeffersonmartin
* `docs` Docs initial scaffolding - !71 - @jeffersonmartin
* `other` Update .gitlab-ci.yml to enable or configure SAST - !83 - @jeffersonmartin
* `other` Upgrade to laravel v8.0 - !45 - @jeffersonmartin
* `user-ui` Add routes/auth file - !5 - @jeffersonmartin
* `user-ui` Adding logging to okta auth - !54 - @jeffersonmartin
* `user-ui` Fix potential xss issue with unescaped variables - !82 - @jeffersonmartin
* `user-ui` Fix typos with auth providers and login - !60 - @jeffersonmartin
* `user-ui` Implement cloud accounts ui - !80 - @jeffersonmartin
* `user-ui` Implement coreui theme and stylesheets - !3 - @jeffersonmartin
* `user-ui` Implement user UI and refactor authentication - !46 - @jeffersonmartin
* `user-ui` Implement user route based permissions - !59 - @jeffersonmartin
* `user-ui` Update ui theme with tweaks - !72 - @jeffersonmartin

### Commits (503)
* `backend` Add AuthBaseSeed - c30d75d3 - !9
* `backend` Add AuthGroupRoleService - 423c2a46 - !8
* `backend` Add AuthGroupService - b6f97b84 - !8
* `backend` Add AuthGroupUserService - cbaef1ed - !8
* `backend` Add AuthProviderGroupService - d66294c0 - !8
* `backend` Add AuthProviderService - ee22d870 - !8
* `backend` Add AuthRoleFactory - 7e78f147 - !51
* `backend` Add AuthRoleService - 9962f142 - !8
* `backend` Add AuthServiceAccountRoleService - 9d8bfb2e - !8
* `backend` Add AuthServiceAccountService - 87f4bd17 - !8
* `backend` Add AuthTenantService - d2b6bd5e - !8
* `backend` Add AuthUserFactory - 474c0c9b - !51
* `backend` Add AuthUserService - b7500cc4 - !8
* `backend` Add BaseModel - 43fa836a - !10
* `backend` Add BaseService - dd1e2e1e - !8
* `backend` Add CloudAccountGroupRoleService - 5e26655d - !29
* `backend` Add CloudAccountGroupService - ba2237cf - !29
* `backend` Add CloudAccountService - 201af58d - !29
* `backend` Add CloudAccountUserRoleService - 2625701c - !29
* `backend` Add CloudAccountUserService - ead909f0 - !29
* `backend` Add CloudBillingAccountService - e10db1ae - !29
* `backend` Add CloudOrganizationUnitService - 7bdee533 - !29
* `backend` Add CloudProviderService - b5ebe472 - !29
* `backend` Add CloudRealmService - 359efc8c - !29
* `backend` Add Command/Cloud/CloudRealmDeprovision - bd8bb09a - !69
* `backend` Add Commands/Auth/AuthTenantEdit - 71a5c472 - !66
* `backend` Add Commands/Cloud/CloudAccountCreateStatusRefresh - df156044 - !76
* `backend` Add Commands/Cloud/CloudRealmProvision - 96b24ac5 - !69
* `backend` Add ExampleService.php.stub - 6a11bf65 - !8
* `backend` Add Models/Auth/AuthGroup - b5b6f572 - !7
* `backend` Add Models/Auth/AuthGroupRole - 30bbd853 - !7
* `backend` Add Models/Auth/AuthGroupUser - bde813ca - !7
* `backend` Add Models/Auth/AuthProvider - 36b7a6c0 - !7
* `backend` Add Models/Auth/AuthProviderGroup - c06ddc9e - !7
* `backend` Add Models/Auth/AuthRole - 8463d70c - !7
* `backend` Add Models/Auth/AuthServiceAccount - 605702f3 - !7
* `backend` Add Models/Auth/AuthServiceAccountRole - ea590805 - !7
* `backend` Add Models/Auth/AuthTenant - 537079b7 - !7
* `backend` Add Models/Auth/AuthUser - e546b1e8 - !7
* `backend` Add Models/Cloud/CloudAccount - 69840fed - !16
* `backend` Add Models/Cloud/CloudAccountGroup - 976101e0 - !16
* `backend` Add Models/Cloud/CloudAccountGroupRole - 9a7c5a14 - !16
* `backend` Add Models/Cloud/CloudAccountUser - 1c209a56 - !16
* `backend` Add Models/Cloud/CloudAccountUserRole - 2f43371c - !16
* `backend` Add Models/Cloud/CloudBillingAccount - 5bed9706 - !16
* `backend` Add Models/Cloud/CloudKeypair - 3fa5a0fa - !16
* `backend` Add Models/Cloud/CloudOrganizationUnit - 3b86e9a4 - !16
* `backend` Add Models/Cloud/CloudProvider - 89508e78 - !16
* `backend` Add Models/Cloud/CloudRealm - 7d10ec5e - !16
* `backend` Add Services/V1/Vendor/Aws/BaseService - 36e2ddc7 - !70
* `backend` Add Services/V1/Vendor/Aws/OrganizationAccountService - fbc75a3f - !70
* `backend` Add Services/V1/Vendor/Aws/OrganizationService - 6d83d64f - !70
* `backend` Add Services/V1/Vendor/Aws/OrganizationalUnitService - ea2af6d3 - !70
* `backend` Add Services/V1/Vendor/Gcp/BaseService - 310e7a58 - !69
* `backend` Add Services/V1/Vendor/Gcp/CloudResourceManagerFolderService - 5a6083f5 - !69
* `backend` Add Services/V1/Vendor/Gitlab/BaseService - fb8edb59 - !67
* `backend` Add Services/V1/Vendor/Gitlab/GroupService - 33f91738 - !67
* `backend` Add auth SSO providers to seed file - e87d5d9b - !22
* `backend` Add config/hackystack with scaffolding for analytics and auth providers - dd695251 - !4
* `backend` Add migrations/create_auth_groups_roles_table - fe4e511e - !6
* `backend` Add migrations/create_auth_groups_table - 9f9e28d8 - !6
* `backend` Add migrations/create_auth_groups_users_table - a353bfb6 - !6
* `backend` Add migrations/create_auth_password_resets_table - 5f5cc745 - !6
* `backend` Add migrations/create_auth_providers_groups_table - 84923518 - !6
* `backend` Add migrations/create_auth_providers_table - 0036737b - !6
* `backend` Add migrations/create_auth_roles_table - adef7aff - !6
* `backend` Add migrations/create_auth_service_accounts_roles_table - cbb364ab - !6
* `backend` Add migrations/create_auth_service_accounts_table - e970f82e - !6
* `backend` Add migrations/create_auth_tenants_table - 40821199 - !6
* `backend` Add migrations/create_auth_users_table - 92ba6903 - !6
* `backend` Add migrations/create_cloud_accounts_groups_roles_table - 641885fe - !15
* `backend` Add migrations/create_cloud_accounts_groups_table - 5571e966 - !15
* `backend` Add migrations/create_cloud_accounts_table - eeb3c8c3 - !15
* `backend` Add migrations/create_cloud_accounts_users_roles_table - f6bf20b5 - !15
* `backend` Add migrations/create_cloud_accounts_users_table - 1b20123c - !15
* `backend` Add migrations/create_cloud_billing_accounts_table - 5260259d - !15
* `backend` Add migrations/create_cloud_keypairs_table - c3329395 - !17
* `backend` Add migrations/create_cloud_organization_units_table - 77254ad9 - !15
* `backend` Add migrations/create_cloud_providers table - 34926128 - !15
* `backend` Add migrations/create_cloud_realms_table - 1a7a3851 - !15
* `backend` Fix Auth model prefix names - d6ae407f - !18
* `backend` Fix AuthGroupUserService to change variable name from role to group - 91a342e6 - !20
* `backend` Fix AuthRoleService to fix default value for permissions array - a672b80c - !77
* `backend` Fix CloudAccount BaseModel path and fillable array comma - 5b261833 - !31
* `backend` Fix CloudAccountService flag_provisioned and state values - 69e5873c - !79
* `backend` Fix CloudAccountUserRoleService flag_provisioned and state values - 4f539d72 - !79
* `backend` Fix CloudBillingAccount BaseModel path - cc2db067 - !31
* `backend` Fix CloudKeypair BaseModel path - 04303a62 - !31
* `backend` Fix CloudOrganizationUnit BaseModel path - fba0bc02 - !31
* `backend` Fix CloudOrganizationUnitService flag_provisioned and state values - 7f2972a7 - !79
* `backend` Fix CloudProvider BaseModel path - 481e1365 - !31
* `backend` Fix CloudProviderService flag_provisioned value - 1b2b4423 - !79
* `backend` Fix CloudProviderService namespace conventions - 81a2ea69 - !79
* `backend` Fix CloudRealm BaseModel path - cbeb6f17 - !31
* `backend` Fix CloudRealmService to remove legacy namespace - 42313fcf - !79
* `backend` Fix Models/Cloud/CloudAccountUserRole to add setFlagProvisionedAttribute - cc311da4 - !79
* `backend` Fix Services/V1/Cloud/* to add use App\Services namespace - e2ed7827 - !79
* `backend` Fix User/CloudAccountController::store to add auth_tenant_id to cloudAccountUserService - 172c1838 - !79
* `backend` Fix Vendor/Aws/IamUserRoleService to fix aws_api_client variable typo - 23482011 - !79
* `backend` Fix Vendor/Aws/OrganizationalUnitService json_decode bug with API credentials - 5ae02ef0 - !76
* `backend` Initial commit of CloudAccountRoleService - ab671e5f - !79
* `backend` Initial commit of Laravel with tweaked composer and README - e185e399 - !2
* `backend` Initial commit of Models/Cloud/CloudAccountRole - 4d5b011c - !79
* `backend` Initial commit of Vendor/Aws/IamUserRoleService - 60f2e18e - !79
* `backend` Initial commit of Vendor\Aws\IamUserService - 39de3940 - !79
* `backend` Initial commit of database/migrations/create_cloud_accounts_roles_table - cb904ded - !79
* `backend` Merge branch 'master' into 'add-hackystack-config-file' - 6ee41c35 - !4
* `backend` Refactor CloudAccountUserRoleService - 69d83b36 - !79
* `backend` Refactor CloudAccountUserService - 576ebaf2 - !79
* `backend` Refactor CloudAccountUserService - a49c0192 - !79
* `backend` Refactor CloudOrganizationUnitService based on bug testing - b76f6061 - !78
* `backend` Refactor Commands/Cloud/CloudRealmGet to implement without-child-relationship conditional - e56e6100 - !69
* `backend` Refactor Models/Cloud/CloudAccountGroupRole to add cloud_account_role and remove auth_role - ada61f62 - !79
* `backend` Refactor Models/Cloud/CloudAccountUserRole to add cloud_account_role and remove auth_role - 7f6e4d6e - !79
* `backend` Refactor Vendor/Aws/OrganizationAccountService to remove waiting count loop (now background job command) and add getCreateStatus method - 9b2afc03 - !76
* `backend` Update Auth/AuthUserService to fix naming of invoked service methods and config paths - 01c70b97 - !23
* `backend` Update Auth/AuthUserService to refactor store and update docblock comment parameters - 6d8a85fa - !23
* `backend` Update Auth/AuthUserService to rename $request to $request_data - 546af4ce - !23
* `backend` Update Auth/LoginController to add defaultAuthTenant id to replace $auth_tenant variable - e1b3503b - !55
* `backend` Update AuthBaseSeed to add cloud-account-group - 168979e0 - !75
* `backend` Update AuthBaseSeed to add cloud-account-role - adaa83c7 - !75
* `backend` Update AuthBaseSeed with simpler description for auth privileged roles - 74aa364f - !75
* `backend` Update AuthBaseSeed with user baseline roles and group self-service roles - ba5da7c4 - !51
* `backend` Update AuthGroup model to add expires_at to pivot relationships - 57f2adcc - !19
* `backend` Update AuthProviderService to add encryption to client_id and client_secret fields on store and update - 1e960a1e - !57
* `backend` Update AuthProviderService to rename request data variables typos - 2da2d363 - !23
* `backend` Update AuthTenantService to add auth provider creation to store method - 9f404301 - !24
* `backend` Update AuthTenantService to add git fields - 5311017f - !66
* `backend` Update AuthTenantService to implement updateGitMetaData method - 4fb71387 - !66
* `backend` Update AuthUser model to add provider_meta_data and provider_token fields - 0447f1ac - !27
* `backend` Update AuthUser model to rename authy_id to 2fa_token - f25ab62f - !23
* `backend` Update AuthUser model to rename pivot relationship from authUserGroup to authGroupUser - 43e030d6 - !23
* `backend` Update AuthUserService to add provider_meta_data and provider_token fields - e847922a - !27
* `backend` Update BaseController to add defaultAuthTenant method - 0d308bad - !55
* `backend` Update CloudAccountGroup model namespace from Auth to Cloud - 642e7701 - !38
* `backend` Update CloudAccountGroupRole model namespace from Auth to Cloud - 2b4b5ae2 - !38
* `backend` Update CloudAccountGroupRole to add expires_at field - e00c8aa1 - !28
* `backend` Update CloudAccountGroupService to add cloud_provider_id field - d16b1bc3 - !79
* `backend` Update CloudAccountService to add billing_access true value - e7ae25e1 - !79
* `backend` Update CloudAccountService to add creation of default Cloud Account Roles to store method - c01ca29f - !79
* `backend` Update CloudAccountService to add provisionCloudAccount and deprovisionCloudAccount methods - a72fbdb9 - !76
* `backend` Update CloudAccountService to change provision method provisioned conditional check to use flag instead of api_meta_data value - fadba6f8 - !79
* `backend` Update CloudAccountService to rename deprovisionCloudAccount method to deprovision - d4f2094e - !79
* `backend` Update CloudAccountService to rename provisionCloudAccount method to provision - 4317c0d2 - !79
* `backend` Update CloudAccountUser model namespace from Auth to Cloud - 5228e8a9 - !38
* `backend` Update CloudAccountUserRole model namespace from Auth to Cloud - 9a08adbd - !38
* `backend` Update CloudAccountUserRole to add expires_at field - 057ba3de - !28
* `backend` Update CloudAccountUserRoleService to add cloudAccountUser relationship - 71d7c44f - !31
* `backend` Update CloudAccountUserService to add provisionCloudAccountUser method - db4e68ac - !79
* `backend` Update CloudOrganizationUnit model to add parent and child organizational unit relationships - 187ef4c9 - !28
* `backend` Update CloudOrganizationUnit model to make name and slug nullable in updateRules - 08ad7492 - !28
* `backend` Update CloudOrganizationUnitService to add deprovisionCloudProviderOrganizationUnit method - 979fe666 - !70
* `backend` Update CloudOrganizationUnitService to add provisionCloudProviderOrganizationUnit method - a5e2a753 - !70
* `backend` Update CloudOrganizationUnitService to implement provisionCloudProviderOrganizationUnit methods - 48c2af39 - !70
* `backend` Update CloudProvider model to add billing account and realm relationships - de02ccee - !31
* `backend` Update CloudProviderService to add default_roles array - af5f0328 - !79
* `backend` Update CloudProviderService to add parent_org_unit field - 910d443b - !74
* `backend` Update CloudProviderService to add syncCloudProviderOrganization method - 53c2cba6 - !70
* `backend` Update CloudProviderService to fix syncCloudProviderOrganization method in store method - 28b920ef - !74
* `backend` Update CloudProviderService to implement syncCloudProviderOrganization in methods - 6afb26b1 - !70
* `backend` Update CloudRealm model to add rootCloudOrganizationUnit relationship - 4a9a0983 - !39
* `backend` Update CloudRealmService to add root organization unit creation - 6015b19d - !39
* `backend` Update CloudRealmService to implement deprovisionCloudProviderOrganizationUnit method - f702e16a - !69
* `backend` Update CloudRealmService to implement provisionCloudProviderOrganizationUnit method - 218f0603 - !69
* `backend` Update CloudRealmService to remove deprecated deprovisionCloudProviderOrganizationUnit method - ac4818a1 - !70
* `backend` Update CloudRealmService to remove deprecated provisionCloudProviderOrganizationUnit method - b4e71f36 - !70
* `backend` Update CloudRealmService with typo fixes - b528f248 - !69
* `backend` Update Commands/Auth/AuthProviderEdit to decrypt client_id and client_secret - c7bfb370 - !57
* `backend` Update Commands/Auth/AuthTenantGet to add git fields - c87ba685 - !66
* `backend` Update Commands/AuthGroupImportConfigFile to remove deprecated Yaml package namespace - 7020b3fe - !44
* `backend` Update Commands/AuthTenantDelete to fix error handling outputted instructions - 541380d8 - !23
* `backend` Update Commands/Cloud/CloudAccountCreateStatusRefresh to add loop through users and roles to provision - a89e2f16 - !79
* `backend` Update Commands/Cloud/CloudProviderCredential to add parent_org_unit - 3105ca81 - !74
* `backend` Update Commands/CloudBillingAccountList to fix typo in console output - d08ea342 - !44
* `backend` Update Commands/CloudRealmGet to add child organization unit relationships - 173f44a6 - !39
* `backend` Update Commands/CloudRealmImportConfigFile to remove deprecated Yaml package namespace - c8d669d3 - !44
* `backend` Update Console/Kernel to add cloud-account:create-status-refresh to schedule every minute - e1d01d81 - !79
* `backend` Update Models/* with improved comment block docs for relationships - 6390cb32 - !44
* `backend` Update Models/Auth/AuthTenant to add cloud child relationships - 0c1aaa8e - !66
* `backend` Update Models/Auth/AuthTenant with git_provider and git_meta_data fields - 1c5e97fb - !66
* `backend` Update Models/Auth/AuthUser to add groups array attribute - 147a2c42 - !77
* `backend` Update Models/Auth/AuthUser to add roles array attribute - a54e6cc7 - !77
* `backend` Update Models/Auth/AuthUser to add user_handle attribute - 5bb554a6 - !77
* `backend` Update Models/Cloud/CloudAccount to add child relationships for users, groups and roles - 8b4060d4 - !79
* `backend` Update Models/Cloud/CloudAccount to add git_meta_data and provisioned fields - e8c5a973 - !65
* `backend` Update Models/Cloud/CloudAccountGroup to add api_meta_data and flag_provisioned - a1775f4e - !79
* `backend` Update Models/Cloud/CloudAccountGroup to add cloud account users relationship - baf3dd65 - !79
* `backend` Update Models/Cloud/CloudAccountGroup to add cloud_provider_id field - c0384615 - !79
* `backend` Update Models/Cloud/CloudAccountGroup to remove deprecated api_meta_data and provisioned fields - 71c75510 - !79
* `backend` Update Models/Cloud/CloudAccountGroupRole to remove deprecated expires_at field - da5d5090 - !79
* `backend` Update Models/Cloud/CloudAccountRole to add api_name field - dd60445c - !79
* `backend` Update Models/Cloud/CloudAccountRole to refactor child relationships for group and user roles - 1218d8b5 - !79
* `backend` Update Models/Cloud/CloudAccountRole to remove deprecated slug and provisioned fields - 9e75ee87 - !79
* `backend` Update Models/Cloud/CloudAccountUser to add cloud_account_group_id and cloud_provider_id fields - d280a699 - !79
* `backend` Update Models/Cloud/CloudAccountUser to add credentials, meta_data, and provisioned fields - a2729ae1 - !79
* `backend` Update Models/Cloud/CloudAccountUser to remove flag_provisioned and provisioned from fillable and validation rules (backend logic only) - 4dee9490 - !79
* `backend` Update Models/Cloud/CloudAccountUserRole to add provisioned fields to casts - 737e224c - !79
* `backend` Update Models/Cloud/CloudKeypair to add provisioned flag - 75077ffe - !65
* `backend` Update Models/Cloud/CloudOrganizationUnit to add git_meta_data and provisioned flags - 24ce6b49 - !65
* `backend` Update Models/Cloud/CloudProvider to add default_roles array field - bbcdf8d2 - !79
* `backend` Update Models/Cloud/CloudProvider to add git_meta_data and provisioned flags - eecdb31d - !65
* `backend` Update Models/Cloud/CloudProvider to add parent_org_unit field - 684ddea7 - !74
* `backend` Update Models/Cloud/CloudRealm to add git_meta_data and provisioned flags - 1a7a6e58 - !65
* `backend` Update Okta/LoginController to add defaultAuthTenant id to okta provider lookup - 8fcc4634 - !55
* `backend` Update Services/BaseService to add defaultAuthTenant method - 750afb49 - !55
* `backend` Update Services/V1/Vendor/Aws/OrganizationAccountService to add tags array - d1af69f5 - !70
* `backend` Update Services/V1/Vendor/Aws/OrganizationAccountService to update docblock comments - d081d4c1 - !70
* `backend` Update Services/V1/Vendor/Aws/OrganizationService to fix credentials json_decode typo - e1687b1a - !70
* `backend` Update Services/V1/Vendor/Aws/OrganizationService to use single organization root in array - ec19f118 - !70
* `backend` Update User/CloudAccountController to add AccountUser and AccountUserRole creation workflow to store method - 3d16551f - !79
* `backend` Update User/InfrastructureController to add auth_tenant_id filter to page data - 73d9a991 - !55
* `backend` Update Vendor/Aws/IamUserService to fix typo with UserId and remove password from returned array - 86cb6088 - !79
* `backend` Update Vendor/Aws/IamUserService to remove attachUserPolicy (refactored into separate service) - 02453f3c - !79
* `backend` Update Vendor/Gcp/CloudResourceManagerFolderService to add organizations example to docblock - 157c36e6 - !78
* `backend` Update composer to add m4tthumphrey/php-gitlab-api package - 6ef27a31 - !67
* `backend` Update composer.json/lock to include aws-sdk-php, google, and symfony/yaml - 1e36716b - !43
* `backend` Update config/hackystack to add missing local auth variables used in AuthUserService - 879bc3b7 - !23
* `backend` Update config/hackystack to remove SSO provider variables (now in database) - 5d1ddf70 - !24
* `backend` Update config/hackystack-auth-groups-example to add providers child arrays - 3a195b52 - !50
* `backend` Update config/logging to add hackystack-cloud and hackystack-git log channels - 9aec1f27 - !67
* `backend` Update database/migrations/auth_* to add comment('encrypted') on fields - ba68c15a - !81
* `backend` Update database/migrations/create_cloud_accounts_groups_roles_table to refactor relationship fields - e8e32b2c - !79
* `backend` Update database/migrations/create_cloud_accounts_groups_table to add cloud_provider_id field - c9e998f5 - !79
* `backend` Update database/migrations/create_cloud_accounts_users_roles_table to refactor relationship fields - 47c8232c - !79
* `backend` Update database/migrations/create_cloud_accounts_users_table to add cloud_account_group_id field - 053f8ab2 - !79
* `backend` Update database/migrations/create_cloud_accounts_users_table to make cloud_account_group_id nullable - 5a30fe02 - !79
* `backend` Update database/migrations/create_cloud_accounts_users_table to refactor fields - 56f0496e - !79
* `backend` Update database/migrations/create_cloud_providers_table to add MySQL comment for encrypted api_credentials field - 88474fa7 - !79
* `backend` Update database/migrations/create_cloud_providers_table to add default_roles field - cd773fc1 - !79
* `backend` Update database/seeds/AuthBaseSeed to remove static list of providers (now in tenant store method) - 490e4471 - !24
* `backend` Update migrations/create_auth_groups_users to add expires_at field - 4b8b83ee - !23
* `backend` Update migrations/create_auth_providers_table to change client_id from string to text - fe8ba944 - !57
* `backend` Update migrations/create_auth_tenants_table to add git fields - 3d790459 - !66
* `backend` Update migrations/create_auth_users_table to add created_by UUID fields - 8f9da164 - !23
* `backend` Update migrations/create_auth_users_table to add provider_meta_data json field - e567c8e2 - !27
* `backend` Update migrations/create_auth_users_table to make timezone field nullable - 45cf29b3 - !23
* `backend` Update migrations/create_cloud_accounts to add git_meta_data and provisioned flags - 0dfc3266 - !65
* `backend` Update migrations/create_cloud_keypairs to add provisioned flags - dedcc120 - !65
* `backend` Update migrations/create_cloud_organization_units to add git_meta_data and provisioned flags - 78f42671 - !65
* `backend` Update migrations/create_cloud_providers to add git_meta_data and provisioned flags - f7bb3f42 - !65
* `backend` Update migrations/create_cloud_providers_table to add parent_org_unit field - c468b0a9 - !74
* `backend` Update migrations/create_cloud_realms to add git_meta_data and provisioned flags - ca311c32 - !65
* `backend` Update migrations/create_cloud_realms_table to add cloud_organization_unit_id_root - c925a2fc - !39
* `backend` Update migrations/create_cloud_realms_table to remove unique constraint from slug - 9440fe22 - !40
* `backend` Update user-base-role to change infrastructure route to cloud route name - 8cafb891 - !75
* `backend` Update views/user/cloud/accounts/create and controller to add existing account alert warning - 7f64496e - !79
* `cli` Add Commands/Auth/AuthGroupRoleList - ba66367a - !56
* `cli` Add Commands/Auth/AuthGroupUserList - 287766ba - !56
* `cli` Add Commands/Auth/AuthProviderGet - 50bdeed3 - !62
* `cli` Add Commands/Auth/AuthRolePermissionAttach - b661dfba - !61
* `cli` Add Commands/Auth/AuthRolePermissionDetach - 892d750b - !61
* `cli` Add Commands/AuthGroupCreate - a4175e9d - !11
* `cli` Add Commands/AuthGroupDelete - b9ecb330 - !11
* `cli` Add Commands/AuthGroupGet - f899e628 - !11
* `cli` Add Commands/AuthGroupImportConfigFile - 9f7e9405 - !41
* `cli` Add Commands/AuthGroupList - 8489b769 - !11
* `cli` Add Commands/AuthGroupRoleCreate - 1f7b5d1e - !21
* `cli` Add Commands/AuthGroupRoleDelete - bc884f8c - !21
* `cli` Add Commands/AuthGroupRoleGet - 03291dea - !21
* `cli` Add Commands/AuthGroupUserCreate - 1d65629c - !21
* `cli` Add Commands/AuthGroupUserDelete - 48ed606b - !21
* `cli` Add Commands/AuthGroupUserGet - b2773317 - !21
* `cli` Add Commands/AuthProviderEdit - 9e3fc880 - !47
* `cli` Add Commands/AuthProviderFieldCreate - 7b2ed27d - !52
* `cli` Add Commands/AuthProviderFieldDelete - e47fe2cf - !52
* `cli` Add Commands/AuthProviderFieldGet - 7144f70b - !52
* `cli` Add Commands/AuthProviderFieldImportConfigFile - c1242c2e - !52
* `cli` Add Commands/AuthProviderFieldList - 47f580b2 - !52
* `cli` Add Commands/AuthProviderGroupCreate - c1695784 - !52
* `cli` Add Commands/AuthProviderGroupDelete - a1c17f4c - !52
* `cli` Add Commands/AuthProviderGroupGet - 88637423 - !52
* `cli` Add Commands/AuthProviderGroupList - b9e7a280 - !52
* `cli` Add Commands/AuthProviderList - a84009ca - !25
* `cli` Add Commands/AuthRoleCreate - 108f1b67 - !14
* `cli` Add Commands/AuthRoleDelete - 8652feaa - !14
* `cli` Add Commands/AuthRoleGet - 3c15473c - !14
* `cli` Add Commands/AuthRoleList - fd05b90f - !14
* `cli` Add Commands/AuthTenantCreate - e6a88e5c - !13
* `cli` Add Commands/AuthTenantDelete - 915ca6ea - !13
* `cli` Add Commands/AuthTenantGet - 70f59fdc - !13
* `cli` Add Commands/AuthTenantList - 26fc2338 - !13
* `cli` Add Commands/AuthUserCreate - becca4d5 - !26
* `cli` Add Commands/AuthUserDelete - 55724c5d - !26
* `cli` Add Commands/AuthUserGet - 5d5b3a24 - !26
* `cli` Add Commands/AuthUserList - bf9cca68 - !21
* `cli` Add Commands/AuthUserPasswordReset - 07eae25c - !26
* `cli` Add Commands/Cloud/CloudOrganizationUnitGitProvision - 04a8b8df - !68
* `cli` Add Commands/Cloud/CloudProviderCredential - a0d2c771 - !63
* `cli` Add Commands/Cloud/CloudProviderGet - 3932f485 - !68
* `cli` Add Commands/Cloud/CloudProviderGitProvision - f80020b2 - !68
* `cli` Add Commands/CloudAccountCreate - 1a9ee56f - !36
* `cli` Add Commands/CloudAccountDelete - 02f7b61e - !36
* `cli` Add Commands/CloudAccountGet - 78db3ae2 - !36
* `cli` Add Commands/CloudAccountList - 4e4f9c34 - !36
* `cli` Add Commands/CloudBillingAccountCreate - 1b7bde2b - !34
* `cli` Add Commands/CloudBillingAccountDelete - f58644dc - !34
* `cli` Add Commands/CloudBillingAccountGet - 92769f75 - !34
* `cli` Add Commands/CloudBillingAccountList - d63e9be3 - !34
* `cli` Add Commands/CloudOrganizationUnitCreate - f9d14768 - !37
* `cli` Add Commands/CloudOrganizationUnitDelete - d55a84da - !37
* `cli` Add Commands/CloudOrganizationUnitGet - 66f29404 - !37
* `cli` Add Commands/CloudOrganizationUnitList - a5d07dae - !37
* `cli` Add Commands/CloudProviderList - 69deff6d - !32
* `cli` Add Commands/CloudRealmCreate - 9f4c86bd - !33
* `cli` Add Commands/CloudRealmDelete - 44421c00 - !33
* `cli` Add Commands/CloudRealmGet - 234db299 - !33
* `cli` Add Commands/CloudRealmImportConfigFile - 68c02499 - !42
* `cli` Add Commands/CloudRealmList - dce3f731 - !33
* `cli` Add Commands/HackystackBackup - a3e8748a - !58
* `cli` Add Commands/HackystackInstall - 59eac2d9 - !48
* `cli` Add Commands/HackystackInstallDatabase - 8ef9347d - !58
* `cli` Add Commands/HackystackReset - 1792638c - !58
* `cli` Add config/hackystack-auth-groups.php.example - b67c0818 - !41
* `cli` Add config/hackystack-auth-provider-fields example - 0fc6692c - !52
* `cli` Add config/hackystack-cloud-realms.php.example - d6e7ebe9 - !42
* `cli` Add config/hackystack.php.example - b2fc4b64 - !58
* `cli` Add hacky executable (duplicate of artisan) - 09b46012 - !58
* `cli` Fix database/seeders/AuthBaseSeed to permissions json_encode bug - 667447e7 - !58
* `cli` Move Auth commands into Commands/Auth namespace - 051fab88 - !53
* `cli` Move Cloud commands into Commands/Cloud namespace - f90f9ad8 - !53
* `cli` Refactor .env.example - f96a542d - !58
* `cli` Refactor Commands/HackystackInstall - 9b424874 - !58
* `cli` Update .gitignore to add /storage/backup and .env.dev - a7483f1d - !58
* `cli` Update .gitignore to add config/hackystack-* files that are not examples - b614b273 - !42
* `cli` Update AuthRole model to fix manyToMany relationship pivot table names - 0d722e59 - !30
* `cli` Update AuthTenantService store method to change cloud provider slug to type - ff0ff3d8 - !63
* `cli` Update AuthTenantService to add cloud providers relationship - ca63e7f8 - !30
* `cli` Update AuthUserCreate to fix comment typo - 7822f183 - !30
* `cli` Update AuthUserList to fix created_at date format - 015fe5d9 - !30
* `cli` Update CloudOrganizationUnitService to fix cloud_organization_unit_id_parent field - 8cbcfd87 - !37
* `cli` Update CloudOrganizationUnitService to implement provisionGitGroup and destroyGitGroup method - ce66a51a - !68
* `cli` Update CloudProviderService store method to add creation of default billing account - 34990c5d - !63
* `cli` Update CloudProviderService to implement provisionGitGroup and destroyGitGroup method - 47954460 - !68
* `cli` Update CloudRealmDelete command to rename output typo - 30732d71 - !35
* `cli` Update CloudRealmService to change name and slug for root organization unit - 14a10b89 - !42
* `cli` Update Commands/Auth/*Delete to remove values table and replace with call to :get commands - 35d3c2e0 - !56
* `cli` Update Commands/Auth/*Get to remove parent relationship tables and show inline short ID and slug - f5e802d1 - !56
* `cli` Update Commands/Auth/*Get to replace child relationship tables with command get list call and add --without-child-relationships option - d8535d31 - !56
* `cli` Update Commands/Auth/AuthProviderEdit to add slug lookup option - 3d2bf070 - !62
* `cli` Update Commands/Auth/AuthRoleGet to add table of permissions in child relationships output - 0edce6e7 - !61
* `cli` Update Commands/AuthGroupCreate to fix signature to be singular - c13413d9 - !12
* `cli` Update Commands/AuthGroupGet to add iso8601 date format - 1c9fb31c - !12
* `cli` Update Commands/AuthGroupGet to add roles relationship table to output - 5abd05ae - !21
* `cli` Update Commands/AuthGroupGet to add users relationship table to output - c9ffe9b5 - !21
* `cli` Update Commands/AuthGroupGet to improve error handling instructions - c5b90d79 - !21
* `cli` Update Commands/AuthGroupImportConfigFile with refactored providers array - 99131f26 - !49
* `cli` Update Commands/AuthGroupList to add auth_tenant relationship - 8a8322ed - !30
* `cli` Update Commands/AuthGroupList to remove deprecated dd() from debugging - d41cdc58 - !12
* `cli` Update Commands/AuthProviderList to fix comment and add missing dependency injection - fa8514e4 - !30
* `cli` Update Commands/AuthRoleList to add auth tenant relationship - 7fc8b3b0 - !30
* `cli` Update Commands/Cloud/CloudProviderList to add type field - ea3101ec - !63
* `cli` Update Commands/CloudRealmImportConfigFile to add provider slug to table outputs - f7beae29 - !64
* `cli` Update Commands/CloudRealmImportConfigFile to fix billing account typo - 77ded2fd - !64
* `cli` Update Console/Commands/* to add conditional for no-interaction option when showing preview table outputs - 36032b89 - !73
* `cli` Update Console/Kernel to load commands from subdirs - eb2a1ca6 - !53
* `cli` Update Models/Cloud/CloudProvider to add api_credentials and type field - 11c99e66 - !63
* `cli` Update Services/v1/Cloud/CloudProviderService to add type and api_credentials field - b87107fd - !63
* `cli` Update config/database to set null values for mysql defaults - d78e40c7 - !58
* `cli` Update config/hackystack to add cloud providers array - 3d8f39d8 - !30
* `cli` Update config/hackystack to add installed variable - e5d0f882 - !58
* `cli` Update database/seeders/AuthBaseSeed to fix default permissions for user-base-role - eefe5b0c - !61
* `cli` Update migrations/create_cloud_accounts_table to fix slug unique composite index - 26c128a6 - !63
* `cli` Update migrations/create_cloud_billing_accounts_table to fix slug unique composite index - 31ee093a - !63
* `cli` Update migrations/create_cloud_keypairs_table to fix slug unique composite index - 683fb534 - !63
* `cli` Update migrations/create_cloud_organization_units_table to add cloud_organization_unit_id_parent and fix unique slug - c25b4d6c - !37
* `cli` Update migrations/create_cloud_organization_units_table to fix slug unique composite index - 80bc9ed5 - !63
* `cli` Update migrations/create_cloud_providers_table to add api_credentials and type field - 9ee87099 - !63
* `cli` Update migrations/create_cloud_providers_table to fix slug unique composite index - ce06f80c - !63
* `cli` Update routes/auth to add conditional database look to fix initial install error - 23d283e5 - !58
* `docs` Initial commit of docs/install/README - 7b440f92 - !71
* `other` Move database/seeds to database/seeders and add namespaces (per Laravel upgrade guide) - 29229b78 - !45
* `other` Set .gitlab-ci.yml to enable or configure SAST - 6b8cb439 - !83
* `other` Update composer.json/lock with Laravel 8 and package dependency upgrades - 0c820049 - !45
* `user-ui` Add .gitignore to storage/clockwork - cb92f188 - !3
* `user-ui` Add .gitkeep to resources/assets - 9331c9a2 - !3
* `user-ui` Add Auth/LoginController - bf2d74a1 - !46
* `user-ui` Add Auth/LogoutController - 30391276 - !46
* `user-ui` Add Auth/Okta/LoginController - c554efa0 - !46
* `user-ui` Add AuthProviderField model - a7a186a9 - !46
* `user-ui` Add AuthRealmService - e01ced4f - !46
* `user-ui` Add BaseController - b1b6f5c4 - !46
* `user-ui` Add CloudAccountController - 37ecc190 - !72
* `user-ui` Add Inter font - 28e5849b - !46
* `user-ui` Add Middleware/CheckUserPermission - 44562dc5 - !59
* `user-ui` Add Services/V1/Auth/AuthProviderFieldService - 62435a99 - !46
* `user-ui` Add User/AccountProfileController - 32c4aefc - !46
* `user-ui` Add User/AnalyticController - 28ed8768 - !46
* `user-ui` Add User/DashboardController - c3497ab5 - !46
* `user-ui` Add User/EnvironmentController - 65013690 - !46
* `user-ui` Add User/InfrastructureController - d29d2e41 - !46
* `user-ui` Add alerts partial to user views - f33a09e2 - !59
* `user-ui` Add coreui to package.json and implement scss and webpack configuration - 4a755f19 - !3
* `user-ui` Add migrations/create_auth_providers_fields_table - 35226f4d - !46
* `user-ui` Add placeholder Admin/DashboardController - 93f31670 - !46
* `user-ui` Add placeholder for Auth/Github/LoginController - ec066d9f - !46
* `user-ui` Add placeholder for Auth/Gitlab/LoginController - 0d3bed58 - !46
* `user-ui` Add placeholder for Auth/Google/LoginController - ec8e25e4 - !46
* `user-ui` Add placeholder for Auth/Microsoft/LoginController - 547ee159 - !46
* `user-ui` Add placeholder for Auth/Salesforce/LoginController - ab595751 - !46
* `user-ui` Add placeholder for Auth/Slack/LoginController - 45449b59 - !46
* `user-ui` Add placeholder for Controllers/Auth/Local - a33be0eb - !46
* `user-ui` Add placeholder for views/user/analytics/index - 38a542a0 - !46
* `user-ui` Add placeholder for views/user/dashboard/index - 4213fc6d - !46
* `user-ui` Add placeholder for views/user/environments/index - 2fb97528 - !46
* `user-ui` Add public/assets/gitlab-logo - 468a6c5d - !46
* `user-ui` Add public/assets/hackystack-icon - a90fffc8 - !46
* `user-ui` Add resources/views/user/cloud/accounts/create - 63d2c987 - !72
* `user-ui` Add routes/auth file - 4dbd238f - !5
* `user-ui` Add scaffolding for resources/views/user/cloud/accounts/show-aws - 01a2bbc8 - !72
* `user-ui` Add scaffolding for resources/views/user/cloud/accounts/show-gcp - 451d6919 - !72
* `user-ui` Add storage/debugbar/.gitignore - b1becc15 - !46
* `user-ui` Add views/errors/401 for unauthorized routes - 31d85111 - !59
* `user-ui` Add views/user/account/profile/index - 9e1de044 - !46
* `user-ui` Add views/user/cloud/accounts/aws/_modals/iam-credentials - e32151ae - !80
* `user-ui` Fix Auth/LoginController to fix local provider variable - b1601be4 - !60
* `user-ui` Fix Commands/Auth/AuthProviderEdit to add only decrypt client_id and client_secret if not null - 2b1c518a - !60
* `user-ui` Fix Okta/LoginController to decrypt client_id and client_secret - 11eb6610 - !60
* `user-ui` Fix Okta/LoginController with throwable exception handler for 500 error - 754f311b - !60
* `user-ui` Fix saas/_buttons.scss to remove display:flex due to full width regression - ca88276f - !80
* `user-ui` Fix views/auth/local/login to fix unescaped variable - 7809bbbb - !82
* `user-ui` Fix views/user/_partials/alerts to change variables to escaped values for session vars - 677b1760 - !82
* `user-ui` Fix views/user/_partials/header to change variables to escaped values for request vars - c509e3a3 - !82
* `user-ui` Fix views/user/cloud/accounts/create to fix unescaped output variable - 8646fe81 - !82
* `user-ui` Implement config/hackystack - fb74dfe7 - !3
* `user-ui` Implement routes/_ scaffolding - ada93a72 - !3
* `user-ui` Implement views/auth/_ scaffolding - 230b83ac - !3
* `user-ui` Implement views/errors/_ scaffolding - c4a54d71 - !3
* `user-ui` Implement views/user/_ scaffolding - b88420a9 - !3
* `user-ui` Implement views/user/cloud/accounts/show-aws - a39bd9f4 - !80
* `user-ui` Implement views/user/dashboard/index - 3285596d - !80
* `user-ui` Merge branch 'master' into 'add-hackystack-auth-routes' - b18a7cb4 - !5
* `user-ui` Move views/user/cloud/accounts/show-aws and show-gcp into aws/ and gcp/ folders - 6a6126da - !80
* `user-ui` Refactor AuthProviderGroupService - 3e58e854 - !46
* `user-ui` Refactor resources/views/user/* to move infrastructure/ routes to cloud/ and refactor list of accounts table - a34764e9 - !72
* `user-ui` Refactor routes/auth - f606f3f5 - !46
* `user-ui` Refactor routes/user - c78e2921 - !46
* `user-ui` Refactor views/user/_layouts/core - 436f8276 - !46
* `user-ui` Refactor views/user/_partials/header - 2dd37650 - !46
* `user-ui` Refactor views/user/cloud/index - 362bd922 - !80
* `user-ui` Remove deprecated views/user/_partials/sidenav - 678bd17b - !46
* `user-ui` Remove deprecated views/user/dashboard/show - efd0191a - !46
* `user-ui` Rename User/InfrastructureController to CloudController - 9770ffdc - !72
* `user-ui` Update .env.example to add APP_NAME and APP_LOGO - a8f44afb - !72
* `user-ui` Update .gitignore to add .DS_STORE - aa7a2176 - !46
* `user-ui` Update Auth/LoginController with logging - 015acbff - !54
* `user-ui` Update AuthProviderService to fix typos with base_url and client_id - 18f72a8b - !46
* `user-ui` Update AuthUser model with BaseModel properties and boot method and extend Authenticatable - efe23516 - !46
* `user-ui` Update AuthUserService to fix json_encode for provider_meta_data - e9c3c0e5 - !46
* `user-ui` Update CSS to change color palette and tweak alignment and UI styles - 74958917 - !46
* `user-ui` Update CloudAccountController to add cloud_account_user IAM credentials - 46787dfc - !80
* `user-ui` Update Middleware/Authenticate to update login route - f86e3476 - !46
* `user-ui` Update Models/Auth/AuthUser to add getPermissions attribute with aggregate role permissions - efc5ad70 - !59
* `user-ui` Update Okta/LoginController to add log entries throughout - d4cbc2ab - !54
* `user-ui` Update Providers/EventServiceProvider to add Socialite providers - e463cc71 - !46
* `user-ui` Update User/AnalyticController to use coming-soon view - 099e6a9d - !72
* `user-ui` Update User/CloudController to limit list of accounts to authenticated user - 0933e7c8 - !80
* `user-ui` Update User/DashboardController to add cloud_accounts_count for user - 208750f1 - !80
* `user-ui` Update User/EnvironmentController to use coming-soon view - c5504376 - !72
* `user-ui` Update composer to add authentication packages - 6882ab62 - !46
* `user-ui` Update composer to add stevebauman/location package - 9ba2a1cf - !54
* `user-ui` Update composer to remove laravel/fortify package - e96023e6 - !59
* `user-ui` Update composer with clockwork package - 9ad92f1c - !3
* `user-ui` Update composer with meta data - 7f46f30b - !3
* `user-ui` Update composer.lock - 33980431 - !3
* `user-ui` Update config/app to add Socialite Service Provider - f604f805 - !46
* `user-ui` Update config/auth to update default model and table - 9cbf4d60 - !46
* `user-ui` Update config/hackystack to add auth.tenant variable - 4a3df05b - !46
* `user-ui` Update config/hackystack to add env variables for IP address and user agent tracking opt-out - 49fdb690 - !54
* `user-ui` Update config/hackystack.php with hackystack.app.name and logo variables - e0dfb366 - !72
* `user-ui` Update config/logging to add hackystack-auth channel with local storage file - bb086377 - !54
* `user-ui` Update coreui.css from auto-generated updates - 938ffaa8 - !80
* `user-ui` Update fonts/remixicon.css to fix ri-lg and ri-xl alignment - b03cece4 - !46
* `user-ui` Update migrations/create_auth_users_table to fix two_factor fields - a55e6a09 - !46
* `user-ui` Update package.json to add typeface-inter - 013cd4dd - !46
* `user-ui` Update package.json/lock with coreui dependencies - 89556889 - !3
* `user-ui` Update public/assets/hackystack-avatar-default - ccc7e82b - !46
* `user-ui` Update public/assets/hackystack-ico - 8cfc2eb4 - !46
* `user-ui` Update public/css/ and /js/ with regenerated SCSS - e64f6dce - !72
* `user-ui` Update resources/saas/_buttons to add icon sizing tweaks inside buttons - 18f9fddc - !72
* `user-ui` Update resources/saas/_card to remove margin-bottom from card-titles - 7dc6c4e1 - !72
* `user-ui` Update resources/saas/_header to make header navigation links lighter font weight - 86e2c34f - !72
* `user-ui` Update resources/saas/_tables to make cells vertical align in middle and fix padding for button group cells - 69fca3bc - !72
* `user-ui` Update resources/saas/_type to add letter-spacing with narrower heading fonts - bbb61f9e - !72
* `user-ui` Update resources/saas/variables/components/_buttons-forms to make input border color one shade darker - 5dd8bdc1 - !72
* `user-ui` Update resources/saas/variables/components/_shared to make border color one shade darker - aec392bb - !72
* `user-ui` Update resources/views/* to add config(hackystack.app.name) to html header title - bfc10e8b - !72
* `user-ui` Update resources/views/auth/* to refactor login pages with logo and application title - d79bd63a - !72
* `user-ui` Update resources/views/errors to add coming-soon - 062d3987 - !72
* `user-ui` Update resources/views/errors/ to make return to dashboard button left aligned - 92d20901 - !72
* `user-ui` Update resources/views/user/_partials/header to add variable-based logo and application title - 8f16d0d8 - !72
* `user-ui` Update resources/views/user/account/profile/index to fix card title styles - 85c79e20 - !72
* `user-ui` Update resources/views/user/account/profile/index to sort groups by name - cf002dbe - !72
* `user-ui` Update routes/user to add permission middleware - b7f2c776 - !59
* `user-ui` Update routes/user to move base path / out of permission middleware - ed33a60e - !72
* `user-ui` Update routes/user to rename infrastructure routes to cloud routes - 92770ad9 - !72
* `user-ui` Update views/auth/local/login to fix provider slug - c1ce7e90 - !46
* `user-ui` Update views/auth/providers/login to refactor SSO providers from config array to database query - e947f065 - !46
* `user-ui` Update views/user/_partirals/header to add conditional links based on user permissions - f8ddeb6b - !59
* `user-ui` Update views/user/cloud/accounts/gcp to remove dd(cloud_account) - 2f59a501 - !82
* `user-ui` Update webpack.mix.js to add typeface-inter - 1732c9f4 - !46
* `user-ui` Update webpack.mix.js with coreui mix files - ccf9da18 - !3
* `user-ui` WIP views/user/groups/index - 4744ad1e - !46
* `user-ui` WIP views/user/infrastructure/index - 78818751 - !46
