<?php

use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Route;

// Redirect any root paths to /dashboard. If the user has not authenticated
// yet, the 'auth' middleware will redirect to the /login route.
Route::get('/', function () {
    return redirect()->route('user.dashboard.index');
})->middleware(['auth']);

Route::middleware(['auth','permission'])->namespace('User')->group(function () {

    // Dashboard
    Route::get('/dashboard', 'DashboardController@index')->name('user.dashboard.index');

    // Infrastructure
    Route::get('/cloud', 'CloudController@index')->name('user.cloud.index');

    Route::get('/cloud/accounts/create', 'CloudAccountController@create')->name('user.cloud.accounts.create');
    Route::post('/cloud/accounts', 'CloudAccountController@store')->name('user.cloud.accounts.store');
    Route::get('/cloud/accounts/{id}', 'CloudAccountController@show')->name('user.cloud.accounts.show');

    // Environments
    Route::get('/environments', 'EnvironmentController@index')->name('user.environments.index');

    // Analytics
    Route::get('/analytics', 'AnalyticController@index')->name('user.analytics.index');

    // Profile
    Route::get('/account/profile', 'AccountProfileController@index')->name('user.account.profile.index');

});
