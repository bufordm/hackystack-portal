# HackyStack Portal

> HackyStack is in early development and is not ready for production environments. We expect to be stable for GitLab's internal use by v0.4 and will tag v1.0 when we consider HackyStack stable for community use.

HackyStack is an open source project created by [Jeff Martin](https://gitlab.com/jeffersonmartin) with contributions from [GitLab](https://about.gitlab.com) team members to provide a streamlined UX for integrating multiple cloud provider APIs and infrastructure-as-code technologies while automating the access approval and access provisioning process for IT teams. 

This project focuses on the provisioning of infrastructure accounts for sandbox use cases (ephemeral demos, testing, training, etc.) and is not designed for provisioning production, staging, or business systems infrastructure.

![Cloud Account Screenshot](https://gitlab.com/hackystack/hackystack-portal/-/raw/main/docs/screenshots/0.1.0-beta/08_CloudAccountReady.png)

You can see screenshots of each release in [docs/screenshots](https://gitlab.com/hackystack/hackystack-portal/-/tree/main/docs/screenshots).

You can learn more about how we've implemented HackyStack at GitLab in our [public company handbook](https://about.gitlab.com/handbook/).
* [Sandbox Cloud (powered by HackyStack)](https://about.gitlab.com/handbook/infrastructure-standards/realms/sandbox/)
* [Infrastructure Standards](https://about.gitlab.com/handbook/infrastructure-standards/)
* [Infrastructure Standards - Labels and Tags](https://about.gitlab.com/handbook/infrastructure-standards/labels-tags/)

### Business Problems We're Solving

The oversimplified user story is "I need to spin up VM(s) or cluster(s) in GCP or AWS to try something, what's the company infrastructure standards for doing that?" 

The goal is to create a frictionless approach for technical team members that includes the tagging needed for cost allocation, best practice security configurations, and streamline the provisioning of infrastructure without needing to wait several days or weeks for an access request to be approved and provisioned. 

This also reduces the burden on the accounting team that processes expense reports for team members each month. Each team member’s account is now part of consolidated billing.

We are developing HackyStack as an [open source project](https://about.gitlab.com/handbook/engineering/open-source/) to allow your infrastructure or IT team to simplify your processes for provisioning sandbox accounts. You can also use HackyStack for individual use by cloning the repository, enter your AWS or GCP API key, click the provision button and the system provisions everything for you. 

This project can also be advocated to your partners and customers for deploying demo, testing, or training infrastructure without long manual provisioning documentation or burdening your internal infrastructure team members.

### Technical Problems We're Solving

1. **Self-Service Provisioning:** Creating an "easy button" for technical users at a company to get access to an AWS account or GCP project with zero manual provisioning by the IT team.
2. **Cloud Agnostic:** Providing a universal interface that is cloud provider agnostic so you don't need to create different architecture and provisioning processes for AWS, GCP, etc.
3. **Hierarchy:** Defining a standard reference architecture for organizational unit hierarchy.
4. **Auto Labeling/Tagging:** Apply labels and tags to resource for cost management, infrastructure-as-code, and security policy compliance without users needing to remember to adding tags.
5. **Billing Costs per User:** Unified billing metrics across all cloud providers on a per-user, per-account/project, and per-group/team level.
6. **Automated Access Requests:** Supplementing single sign-on (SSO) providers with pre-auth automated group membership provisioning with seamless manager approval(s)
7. **Automated Access Approval Provisioning:** Supplementing single sign-on (SSO) providers with post-auth provisioning of infrastructure resources in one or more provider APIs.
8. **GitOps Infrastructure-as-Code Provisioning:** Automatically creating Git projects with Terraform infrastructure-as-code scaffolding with security best practices that uses CI/CD automation.
9. **Standardized Infrastructure-as-Code Library:** Linking to curated library of Terraform modules for easily deploying common infrastructure elements that follow company security best practices.
10. **Daily Workflow Cost Controls:** Slack bots and notifications for users to easily provision or destroy infrastructure and threshold cost/usage report notifications.

### Tech Stack

* [Laravel](https://laravel.com/docs/8.x) - web portal, CLI application, API provisioning handler
* MySQL - database
* [Terraform](https://www.terraform.io/) v0.13 - Infrastructure as Code configuration
* [AWS API](https://github.com/aws/aws-sdk-php) 
* [Google Cloud API](https://github.com/googleapis/google-api-php-client) 
* [GitLab API](https://github.com/GitLabPHP/Client) - For Git SCM of Terraform configurations
* [GitLab CI](https://docs.gitlab.com/ee/ci/) - For automated Terraform deployments

This project was built using Laravel instead of other viable languages due to Jeff's prior experience and proficiency with Laravel to achieve the most efficient time to business value. For those who are not familiar with Laravel, it is the PHP equivalent of [Ruby on Rails](https://trends.google.com/trends/explore?date=all&q=ruby%20on%20rails,laravel) and [Django](https://trends.google.com/trends/explore?date=all&q=Django,laravel) and has seen [tremendous community popularity](https://packagist.org/packages/laravel/framework/stats) in recent years since PHP has made revolutionary improvements in recent years with PHP 5.x and PHP 7.x. This project also allows us to dogfood GitLab CI/CD capabilities for PHP projects.

### Database Models and Relationship Diagrams

#### Authentication Service

![Auth Models](https://gitlab.com/hackystack/hackystack-portal/-/raw/main/docs/images/HackyStack_Model_Relationships_-_Auth.jpg?inline=false)

#### Cloud Service

![Cloud Models](https://gitlab.com/hackystack/hackystack-portal/-/raw/main/docs/images/HackyStack_Model_Relationships_-_Cloud.jpg?inline=false)

### Installation

Please see [install documentation](https://gitlab.com/hackystack/hackystack-portal/-/tree/main/docs/install) for instructions on configuring your local development environment or deploying to production.

### Contributing and Support

Please open a merge request to contribute code or create an issue for bug reports and feature requests.

---

HackyStack was created by [Jeff Martin](https://gitlab.com/jeffersonmartin) with an [Apache 2.0 license](LICENSE).
