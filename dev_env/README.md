# This setup is for local testing only.

### Instructions:
1. Build and start up your mysql & php containers. The additional arguments provided here force a clean rebuild.
   ```
   docker-compose up -d --no-deps --build
   ```
1. Now you can follow the [Configuring HackyStack](../docs/install/README.md#configuring-hackystack) instructions.
   - Keep in mind that must execute the commands inside the php docker container.
      ```
      docker exec -it php php hacky hackystack:install-database
      ```
   
---

#### How to Rebuild containers from scratch
```
docker-compose up -d --no-deps --build
```
