<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class DashboardController extends BaseController
{
    public function __construct()
    {
        // TODO Additional validation of roles and permissions
    }

    public function index(Request $request)
    {

        $auth_user_id = $request->user()->id;

        $cloud_accounts_count = Models\Cloud\CloudAccount::query()
            ->whereHas('cloudAccountUsers', function($query) use($auth_user_id) {
                $query->where('auth_user_id', $auth_user_id);
            })
            ->count();

        return view('user.dashboard.index', compact([
            'request',
            'cloud_accounts_count'
        ]));

    }

}
