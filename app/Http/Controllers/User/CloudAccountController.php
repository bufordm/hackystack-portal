<?php

namespace App\Http\Controllers\User;

use App\Http\Controllers\BaseController;
use App\Models;
use App\Services;
use App\Models\Cloud\CloudOrganizationUnit;
use Illuminate\Http\Request;
use Illuminate\Support\Arr;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class CloudAccountController extends BaseController
{
    public function __construct()
    {
        //
    }

    /**
     *   Create a Cloud Account form
     *
     *   @param  Request $request
     */
    public function create(Request $request)
    {
        // Get list of providers that have been provisioned
        $cloud_providers = Models\Cloud\CloudProvider::query()
            ->where('api_meta_data', '!=', null)
            ->where('auth_tenant_id', $this->defaultAuthTenant()->id)
            ->get();

        // If cloud provider has been specified on the page
        if($request->has('cloud_provider_id')) {

            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('id', $request->cloud_provider_id)
                ->firstOrFail();

            // TODO Refactor the code here that is GitLab specific and hard
            // coded for the initial version

            // Get sandbox organization units for the cloud provider
            $sandbox_cloud_organization_units = Models\Cloud\CloudOrganizationUnit::query()
                ->where('cloud_provider_id', $request->cloud_provider_id)
                ->whereHas('parentCloudOrganizationUnit', function($query) {
                    $query->where('slug', 'sandbox-realm');
                })
                ->get();

        } else {
            $sandbox_cloud_organization_units = null;
        }

        // If cloud provider has been specified on the page
        if($request->has('cloud_organization_unit_id')) {

            // Get sandbox organization units for the cloud provider
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('cloud_provider_id', $request->cloud_provider_id)
                ->where('id', $request->cloud_organization_unit_id)
                ->firstOrFail();

            // Check if cloud account exists with same name
            // TODO Refactor this for dynamic naming in next iteration. This
            // is a temporary workaround to avoid duplicate accounts.
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('cloud_organization_unit_id', $cloud_organization_unit->id)
                ->where('slug', $cloud_organization_unit->slug.'-'.$request->user()->user_handle)
                ->first();

        } else {
            $cloud_organization_unit = null;
            $cloud_account = null;
        }

        return view('user.cloud.accounts.create', compact([
            'request',
            'cloud_providers',
            'sandbox_cloud_organization_units',
            'cloud_organization_unit',
            'cloud_account',
        ]));

    }

    /**
     *   Store a Cloud Account
     *
     *   @param  Request $request
     *      cloud_provider_id|uuid
     *      cloud_organization_unit_id|uuid
     *      account_name_prefix|string
     *      account_name_suffix|string
     */
    public function store(Request $request)
    {

        // Validate form request input matches rules in model
        $validator = Validator::make(
            $request->all(),
            [
                // Validation rules
                'cloud_provider_id' => 'required|uuid|exists:cloud_providers,id',
                'cloud_organization_unit_id' => 'required|uuid|exists:cloud_organization_units,id',
                'account_name_prefix' => 'required|exists:cloud_organization_units,slug',
                'account_name_suffix' => 'required|alpha_num',
            ],
            [
                // Custom error messages
                // 'short_id.required' => 'You have not provided an invitation token.',
            ]
        )->validate();

        // Lookup cloud provider to get relationship IDs
        $cloud_provider = Models\Cloud\CloudProvider::query()
            ->where('id', $request->cloud_provider_id)
            ->firstOrFail();

        // Lookup cloud organization unit to get relationship IDs
        $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
            ->where('id', $request->cloud_organization_unit_id)
            ->firstOrFail();

        // Concatenate organization unit slug and account name suffix
        $account_name = $request->account_name_prefix.'-'.$request->account_name_suffix;

        // Use service to create new Cloud Account
        $cloudAccountService = new Services\V1\Cloud\CloudAccountService();
        $cloud_account = $cloudAccountService->store([
            'auth_tenant_id' => $cloud_organization_unit->auth_tenant_id,
            'cloud_billing_account_id' => $cloud_provider->cloud_billing_account_id,
            'cloud_organization_unit_id' => $cloud_organization_unit->id,
            'cloud_provider_id' => $cloud_provider->id,
            'cloud_realm_id' => $cloud_organization_unit->cloud_realm_id,
            'name' => $account_name,
            'slug' => $account_name,
        ]);

        // Use service to create Cloud Account User
        $cloudAccountUserService = new Services\V1\Cloud\CloudAccountUserService();
        $cloud_account_user = $cloudAccountUserService->store([
            'auth_tenant_id' => $cloud_organization_unit->auth_tenant_id,
            'auth_user_id' => $request->user()->id,
            'cloud_account_id' => $cloud_account->id,
        ]);

        // TODO Refactor with dynamic roles in future iteration. Since the creator
        // of the account is assumed to be an administrator, this is fine for now.

        // If cloud provider is AWS
        if($cloud_provider->type == 'aws') {

            // Use service to create Cloud Account User Role
            $cloudAccountUserRoleService = new Services\V1\Cloud\CloudAccountUserRoleService();
            $cloud_account_user_role = $cloudAccountUserRoleService->store([
                'cloud_account_user_id' => $cloud_account_user->id,
                'cloud_account_role_name' => 'AdministratorAccess'
            ]);

        }

        // If cloud provider is GCP
        elseif($cloud_provider->type == 'gcp') {

            // Use service to create Cloud Account User Role
            $cloudAccountUserRoleService = new Services\V1\Cloud\CloudAccountUserRoleService();
            $cloud_account_user_role = $cloudAccountUserRoleService->store([
                'cloud_account_user_id' => $cloud_account_user->id,
                'cloud_account_role_name' => 'Owner'
            ]);

        }

        return redirect()->route('user.cloud.accounts.show', ['id' => $cloud_account->id]);

    }

    /**
     *   Show the Cloud Account
     *
     *   @param  uuid  $id          CloudAccount UUID
     *   @param  Request $request
     */
    public function show($id, Request $request)
    {
        // Get cloud account with eager loaded relationships
        $cloud_account = Models\Cloud\CloudAccount::with([
                'cloudProvider',
                'cloudRealm',
                'cloudOrganizationUnit',
                'cloudAccountUsers',
                'cloudAccountRoles',
                'cloudAccountGroups',
            ])
            ->where('id', $id)
            ->firstOrFail();

        // Get cloud account user for IAM credentials for current user
        $cloud_account_user = Models\Cloud\CloudAccountUser::query()
            ->where('cloud_account_id', $cloud_account->id)
            ->where('auth_user_id', $request->user()->id)
            ->first();

        if($cloud_account->cloudProvider->type == 'aws') {
            return view('user.cloud.accounts.aws.show', compact([
                'request',
                'cloud_account',
                'cloud_account_user'
            ]));
        } elseif($cloud_account->cloudProvider->type == 'gcp') {
            return view('user.cloud.accounts.gcp.show', compact([
                'request',
                'cloud_account',
                'cloud_account_user'
            ]));
        }

    }

}
