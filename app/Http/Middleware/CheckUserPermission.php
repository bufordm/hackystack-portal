<?php

namespace App\Http\Middleware;

use App\Models;
use Closure;
use Illuminate\Http\Request;

class CheckUserPermission
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {

        // If the user has permission to access the route, allow the request.
        if (in_array($request->route()->getName(), $request->user()->permissions)) {
            return $next($request);
        }

        // If the user does not have permission, redirect to the dashboard.
        else {
            $request->session()->flash('401-permission', $request->route()->getName());
            abort(401);
        }

    }
}
