<?php

namespace App\Models\Cloud;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class CloudRealm extends Models\BaseModel
{
    use SoftDeletes;

    public $table = 'cloud_realms';

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    public $fillable = [
        'auth_tenant_id',
        'cloud_billing_account_id',
        'cloud_provider_id',
        'api_meta_data',
        'git_meta_data',
        'name',
        'slug',
        'flag_provisioned',
        'provisioned_at',
    ];

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenants,id',
        'cloud_billing_account_id' => 'required|uuid|exists:cloud_billing_accounts,id',
        'cloud_provider_id' => 'required|uuid|exists:cloud_providers,id',
        'api_meta_data' => 'nullable|array',
        'git_meta_data' => 'nullable|array',
        'name' => 'required|string|max:255',
        'slug' => 'required|string|max:255',
        'flag_provisioned' => 'nullable|integer|between:0,1',
        'provisioned_at' => 'nullable|datetime',
    ];

    public $updateRules = [
        'cloud_billing_account_id' => 'nullable|uuid|exists:cloud_billing_accounts,id',
        'api_meta_data' => 'nullable|array',
        'git_meta_data' => 'nullable|array',
        'name' => 'required|string|max:255',
        'slug' => 'required|string|max:255',
        'flag_provisioned' => 'nullable|integer|between:0,1',
        'provisioned_at' => 'nullable|datetime',
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'short_id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        'state' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'cloud_billing_account_id' => 'string',
        'cloud_provider_id' => 'string',
        'api_meta_data' => 'array',
        'git_meta_data' => 'array',
        'name' => 'string',
        'slug' => 'string',
        'flag_provisioned' => 'integer',
        'provisioned_at' => 'datetime',
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'provisioned_at' => 'datetime',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`package_widget`). For a child
    // or many-to-many relationship, use a plural name (`package_widgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(Models\Auth\AuthTenant::class, 'auth_tenant_id');
    }

    public function cloudBillingAccount() {
        return $this->belongsTo(CloudBillingAccount::class, 'cloud_billing_account_id');
    }

    public function rootCloudOrganizationUnit() {
        return $this->belongsTo(CloudOrganizationUnit::class, 'cloud_organization_unit_id_root');
    }

    public function cloudProvider() {
        return $this->belongsTo(CloudProvider::class, 'cloud_provider_id');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    public function cloudAccounts() {
        return $this->hasMany(CloudAccount::class, 'cloud_realm_id');
    }

    public function cloudAccountsGroups() {
        return $this->hasMany(CloudAccountGroup::class, 'cloud_realm_id');
    }

    public function cloudAccountsUsers() {
        return $this->hasMany(CloudAccountUser::class, 'cloud_realm_id');
    }

    public function cloudKeypairs() {
        return $this->hasMany(CloudKeypair::class, 'cloud_realm_id');
    }

    public function cloudOrganizationUnits() {
        return $this->hasMany(CloudOrganizationUnit::class, 'cloud_realm_id');
    }

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    public function setFlagProvisionedAttribute($value)
    {
        // If value is different than current value
        if($this->flag_provisioned != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_provisioned'] = 1;
                $this->attributes['provisioned_at'] = now();
            } elseif($value == 0) {
                $this->attributes['flag_provisioned'] = 0;
                $this->attributes['provisioned_at'] = null;
            }

        }
    }

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

}
