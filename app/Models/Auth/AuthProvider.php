<?php

namespace App\Models\Auth;

use App\Models;
use Illuminate\Database\Eloquent\SoftDeletes;

class AuthProvider extends Models\BaseModel
{
    use SoftDeletes;

    public $table = 'auth_providers';

    //
    // Fillable Fields (for Creating/Updating records)
    // ------------------------------------------------------------------------
    // The array below specifies the column name of the fields that are allowed
    // to be included in a POST (store) or PATCH (update) request. This array
    // is used in the Service class for this model in App\Services.
    //

    protected $fillable = [
        'auth_tenant_id',
        'slug',
        'base_url',
        'client_id',
        'client_secret',
        'flag_enabled',
    ];

    // TODO add hidden fields for client_secret

    //
    // Form Validation Rules
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/validation#available-validation-rules
    // See additional documentation in App\Models\BaseModel about how we use
    // form validation rules and implementation examples.
    //

    public $storeRules = [
        'auth_tenant_id' => 'required|uuid|exists:auth_tenants,id',
        'slug' => 'required|string|max:55', // TODO enum providers list
        'base_url' => 'required|url|max:255',
        'client_id' => 'required|string|max:255',
        'client_secret' => 'required|string|max:255',
        'flag_enabled' => 'nullable|integer|between:0,1',
    ];

    public $updateRules = [
        'auth_tenant_id' => 'nullable|uuid|exists:auth_tenants,id',
        'slug' => 'required|string|max:55',
        'base_url' => 'required|url|max:255',
        'client_id' => 'required|string|max:255',
        'client_secret' => 'required|string|max:255',
        'flag_enabled' => 'nullable|integer|between:0,1',
    ];

    //
    // If the default error messages are not friendly enough for users, you
    // can set a custom error message for each field and condition not met.
    // https://laravel.com/docs/6.x/validation#custom-error-messages
    //

    public $validationMessages = [
        // 'field.condition' => 'This is a custom error message.'
    ];

    //
    // Casts (Format for Returned Values)
    // ------------------------------------------------------------------------
    // https://laravel.com/docs/7.x/eloquent-mutators#attribute-casting
    // The array below specifies how the database values will be formatted when
    // returned in Eloquent model, Resource class or JSON response. This is not
    // required since the database column type is used by default, however we
    // want to be explicit for engineering clarity.
    //

    protected $casts = [
        // Common fields
        'id' => 'string',
        'created_at' => 'datetime',
        'created_by' => 'string',
        'updated_at' => 'datetime',
        'updated_by' => 'string',
        'deleted_at' => 'datetime',
        'deleted_by' => 'string',
        // Model specific fields
        'auth_tenant_id' => 'string',
        'slug' => 'string',
        'base_url' => 'string',
        'client_id' => 'string',
        'client_secret' => 'string',
        'flag_enabled' => 'integer',
        'enabled_at' => 'datetime',
    ];

    //
    // Date Fields
    // ------------------------------------------------------------------------
    // Each of the date fields below are automatically formatted as datetime
    // and have the added benefit of being Carbon date objects so you do not
    // need to Carbon::parse($value), and can use $record->created_at->addDay()
    //

    protected $dates = [
        'created_at' => 'datetime',
        'updated_at' => 'datetime',
        'deleted_at' => 'datetime',
        'enabled_at' => 'datetime',
    ];

    //
    // Relationships
    // ------------------------------------------------------------------------
    // To define a relationship, create a public function with the name of the
    // table that you are defining a foreign relationship for. For a parent
    // relationship, use a singular table name (`packageWidget`). For a child
    // or many-to-many relationship, use a plural name (`packageWidgets`). You
    // can then copy and paste the respective return statement in.
    //

    //
    // Parent Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsTo(ForeignModel::class, 'foreign_table_id');
    //

    public function authTenant() {
        return $this->belongsTo(AuthTenant::class, 'auth_tenant_id');
    }

    public function createdBy() {
        return $this->belongsTo(AuthUser::class, 'created_by');
    }

    public function updatedBy() {
        return $this->belongsTo(AuthUser::class, 'updated_by');
    }

    public function deletedBy() {
        return $this->belongsTo(AuthUser::class, 'deleted_by');
    }

    public function enabledBy() {
        return $this->belongsTo(AuthUser::class, 'enabled_by');
    }

    //
    // Child Relationships
    // ------------------------------------------------------------------------
    // return $this->hasMany(ForeignModel::class, 'this_table_id');
    //

    public function authUsers() {
        return $this->hasMany(AuthUser::class, 'auth_provider_id');
    }

    //
    // Distant Child Relationships (Has Many Through)
    // ------------------------------------------------------------------------
    // return $this->hasManyThrough(
    //     DistantModel::class,
    //     IntermediateModel::class,
    //     'intermediate_key_id',
    //     'distant_key_id',
    //     'id'
    // );
    //

    //
    // Many-to-Many Relationships
    // ------------------------------------------------------------------------
    // return $this->belongsToMany(
    //     ForeignModel::class,
    //     'package_this_foreign_table',
    //     'this_table_id',
    //     'foreign_table_id'
    // )->withPivot([
    //     'id',
    //     'field_name',
    //     'created_at',
    //     'updated_at',
    //     'deleted_at'
    // ])->as('pivotModelName');
    //

    public function authGroups() {
        return $this->belongsToMany(
                AuthGroup::class,
                'auth_providers_groups',
                'auth_provider_id',
                'auth_group_id'
            )->withPivot([
                'id',
                'short_id',
                'type',
                'meta_key',
                'meta_value',
                'created_at',
                'updated_at',
                'deleted_at'
            ])->as('authProviderGroup');
    }

    //
    // Custom Attribute Getters
    // ------------------------------------------------------------------------
    // When getting attributes in the database, these methods add additional
    // business logic to be invoked when a field is fetched.
    //

    //
    // Custom Attribute Setters
    // ------------------------------------------------------------------------
    // When storing attributes in the database, these methods add additional
    // business logic to be invoked when a field is stored or updated.
    //

    public function setFlagEnabledAttribute($value)
    {
        // If value is different than current value
        if($this->flag_enabled != $value) {

            // Update flag and date values
            if($value == 1) {
                $this->attributes['flag_enabled'] = 1;
                $this->attributes['enabled_at'] = now();

                // TODO Audit Log

            } elseif($value == 0) {
                $this->attributes['flag_enabled'] = 0;
                $this->attributes['enabled_at'] = null;

                // TODO Audit Log

            }

        }
    }

    //
    // Scopes
    // ------------------------------------------------------------------------
    // We can use scope with Eloquent queries to filter results more easily
    // than unnecessary if() statements.
    //

}
