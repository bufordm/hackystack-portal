<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class AuthGroupList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group:list
                            {--T|auth_tenant_slug= : The slug of the tenant that providers belong to}
                            {--namespace=* : The namespace slug of the groups to list.}
                            {--search=* : Wildcard search of name, slug, or description.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Groups';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get namespace(s)
        $namespaces_array = $this->option('namespace');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Auth\AuthGroup::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($namespaces_array) {
                // If namespaces were specified, the array will not be empty
                if(count($namespaces_array) > 0) {
                    foreach($namespaces_array as $namespace_option) {
                        if ($namespace_option === array_key_first($namespaces_array)) {
                            $query->where('namespace', $namespace_option);
                        } else {
                            $query->orWhere('namespace', $namespace_option);
                        }
                    }
                }
            })
            ->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                        $query->orWhere('description', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through groups in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $group_record) {
            $group_rows[] = [
                'short_id' => $group_record->short_id,
                'auth_tenant' => '['.$group_record->authTenant->short_id.'] '.$group_record->authTenant->slug,
                'roles' => $group_record->authRoles()->count(),
                'users' => $group_record->authUsers()->count(),
                'namespace' => $group_record->namespace,
                'name' => $group_record->name,
                'slug' => $group_record->slug,
                'created_at' => $group_record->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Auth Groups - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Roles', 'Users', 'Namespace', 'Name', 'Slug', 'Created at'];
            $this->table($headers, $group_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
