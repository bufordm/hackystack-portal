<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class AuthProviderFieldCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-provider-field:create
                            {--T|auth_tenant_slug= : The slug of the tenant this provider field belongs to}
                            {--P|auth_provider_slug= : The slug of the Authentication Provider}
                            {--provider_key= : The key in the provider meta data to get the value of.}
                            {--user_column= : The column in the auth_users table that the provider key value should be copied to.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create an Authentication Provider Field';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Provider Fields - Create Record');

        //
        // Authentication Tenant
        // --------------------------------------------------------------------
        // Lookup the authentication tenant or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the provider exist in?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Authentication Provider
        // --------------------------------------------------------------------
        // Lookup the authentication provider or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth provider0 from console option
        $auth_provider_slug = $this->option('auth_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($auth_provider_slug) {
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $auth_providers = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Provider Slug'],
                $auth_providers
            );

            $auth_provider_prompt = $this->anticipate('Which provider will users be authenticating with?', Arr::flatten($auth_providers));

            // Lookup provider based on slug provided in prompt
            $auth_provider = Models\Auth\AuthProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $auth_provider_prompt)
                ->first();

        }

        // Validate that provider exists or return error message
        if(!$auth_provider) {
            $this->error('Error: No provider was found with that slug.');
            $this->error('');
            die();
        }

        // Provider Key
        $provider_key = $this->option('provider_key');
        if($provider_key == null) {
            $provider_key = $this->ask('What is the key in the provider meta data to get the value of?');
        }

        // User Column
        $user_column = $this->option('user_column');
        if($user_column == null) {
            $user_column = $this->ask('What is the column in the auth_users table that the provider key value should be copied to?');
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Verify inputs table
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'auth_tenant_id',
                        '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                    ],
                    [
                        'auth_provider_id',
                        '['.$auth_provider->short_id.'] '.$auth_provider->slug
                    ],
                    [
                        'provider_key',
                        $provider_key
                    ],
                    [
                        'user_column',
                        $user_column
                    ],
                ]
            );

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the creation of the record?')) {
                $this->error('Error: You aborted. No record was created.');
                die();
            }

        }

        // Initialize service
        $authProviderFieldService = new Services\V1\Auth\AuthProviderFieldService();

        // Use service to create record
        $record = $authProviderFieldService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'auth_provider_id' => $auth_provider->id,
            'provider_key' => $provider_key,
            'user_column' => $user_column,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('auth-provider-field:get', [
            'short_id' => $record->short_id
        ]);

    }

}
