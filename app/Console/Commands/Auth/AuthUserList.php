<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class AuthUserList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-user:list
                            {--T|auth_tenant_slug= : The slug of the tenant that users belong to}
                            {--P|auth_provider_slug= : The slug of the provider that users belong to}
                            {--search=* : Wildcard search of name, email, job title, or organization.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Authentication Users';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get auth provider slug
        $auth_provider_slug = $this->option('auth_provider_slug');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Auth\AuthUser::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($auth_provider_slug) {
                if($auth_provider_slug) {
                    $query->whereHas('authProvider', function (Builder $query) use ($auth_provider_slug) {
                        $query->where('slug', $auth_provider_slug);
                    });
                }
            })->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                        $query->orWhere('description', 'LIKE', '%$search_option%');
                    }
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $user_record) {
            $user_rows[] = [
                'short_id' => $user_record->short_id,
                'auth_tenant' => '['.$user_record->authTenant->short_id.'] '.$user_record->authTenant->slug,
                'auth_provider' => '['.$user_record->authProvider->short_id.'] '.$user_record->authProvider->slug,
                'groups' => $user_record->authGroups()->count(),
                'full_name' => $user_record->full_name,
                'email' => $user_record->email,
                'created_at' => $user_record->created_at ? $user_record->created_at->toIso8601String() : 'n/a'
            ];
        }

        $this->comment('');
        $this->comment('Auth Users - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Groups', 'Name', 'Email', 'Created at'];
            $this->table($headers, $user_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
