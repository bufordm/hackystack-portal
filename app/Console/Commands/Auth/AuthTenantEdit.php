<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class AuthTenantEdit extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-tenant:edit
                            {short_id? : The short ID of the tenant.}
                            {--T|auth_tenant_slug= : The current slug of the tenant.}
                            {--git_provider_type= : The type of Git provider (Default `gitlab`).}
                            {--git_provider_base_url= : The base URL (FQDN) of the Git provider API (Default `https://gitlab.com`).}
                            {--git_provider_base_group_id= : The Git provider group ID that sub-groups and projects will be created under.}
                            {--git_provider_api_token= : The API token used for authenticating with the Git provider.}
                            {--name= : The new name of the tenant.}
                            {--slug= : The new slug of the tenant.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Edit an authentication tenant';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Auth Tenant - Edit Record');

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $auth_tenant_slug == null) {
            $this->error('You did not specify the short_id or slug to lookup the record.');
            $this->comment('You can use the `auth-tenant:edit a1b2c3d4` command to lookup a provider by ID.');
            $this->comment('You can use the `auth-tenant-edit --auth_tenant_slug={tenant}` command to lookup a tenant with a slug.');
            $this->comment('You can use the `auth-tenant:list` command to get a list of records.');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($auth_tenant_slug != null) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_tenant == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Call the get method to display the tables of values for the record.
            $this->call('auth-tenant:get', [
                'short_id' => $auth_tenant->short_id,
                '--without-child-relationships' => true
            ]);

        }
        
        // Initialize service
        $authTenantService = new Services\V1\Auth\AuthTenantService();

        $name = $this->option('name');
        $slug = $this->option('slug');

        // Tenant Name
        $name = $this->option('name');
        if($name == null) {
            if($this->confirm('Do you want to change the tenant display name?')) {
                $name = $this->ask('What is the new display name?');
            } else {
                $name = $auth_tenant->name;
            }
        }

        // Tenant Slug
        $slug = $this->option('slug');
        if($slug == null) {
            if($this->confirm('Do you want to change the tenant slug?')) {
                $slug = $this->ask('What is the new slug?');
            } else {
                $slug = $auth_tenant->slug;
            }
        }

        // Git Provider Type
        $git_provider_type = $this->option('git_provider_type');
        if($git_provider_type == null) {
            if($this->confirm('Do you want to change the Git Provider Type?')) {
                $git_provider_type = $this->choice('What is the new provider type?', ['gitlab'], 0);
            } else {
                $git_provider_type = $auth_tenant->git_provider_type;
            }
        }

        // Git Provider Base URL
        $git_provider_base_url = $this->option('git_provider_base_url');
        if($git_provider_base_url == null) {
            if($this->confirm('Do you want to change the Git Provider Base URL?')) {
                $git_provider_base_url = $this->ask('What is the new base URL?');
            } else {
                $git_provider_base_url = $auth_tenant->git_provider_base_url;
            }
        }

        // Git Provider Base Group ID
        $git_provider_base_group_id = $this->option('git_provider_base_group_id');
        if($git_provider_base_group_id == null) {
            if($this->confirm('Do you want to change the Git Provider Base Group ID?')) {
                $git_provider_base_group_id = $this->ask('What is the new group ID?');
            } else {
                $git_provider_base_group_id = $auth_tenant->git_provider_base_group_id;
            }
        }

        // Git Provider API Token
        $git_provider_api_token = $this->option('git_provider_api_token');
        if($git_provider_api_token == null) {
            if($this->confirm('Do you want to change the Git Provider API Token?')) {
                $git_provider_api_token = $this->ask('What is the new API Token?');
            } else {
                $git_provider_api_token = $auth_tenant->git_provider_api_token;
            }
        }

        // Update database record using service method
        $auth_tenant = $authTenantService->update($auth_tenant->id, [
            'name' => $name,
            'slug' => $slug,
            'git_provider_type' => $git_provider_type,
            'git_provider_base_url' => $git_provider_base_url,
            'git_provider_base_group_id' => $git_provider_base_group_id,
            'git_provider_api_token' => $git_provider_api_token,
        ]);

        $this->comment('');

        // Show result in console
        $this->comment('Record successfully updated.');
        $this->comment('');

        if($auth_tenant->git_meta_data != null) {

            $this->comment('Updating child relationship Git meta data.');

            // Initialize services
            $cloudProviderService = new Services\V1\Cloud\CloudProviderService();
            $cloudRealmService = new Services\V1\Cloud\CloudRealmService();
            $cloudOrganizationUnitService = new Services\V1\Cloud\CloudOrganizationUnitService();
            $cloudAccountService = new Services\V1\Cloud\CloudAccountService();

            if($auth_tenant->cloudProviders()->count() > 0) {
                foreach($auth_tenant->cloudProviders as $cloud_provider) {
                    $cloudProviderService->provisionGitGroup($cloud_provider->id);
                }
                $this->line('<fg=green>Cloud providers git_meta_data updated.</>');
            } else {
                $this->line('<fg=cyan>No cloud providers for this tenant.</>');
            }

            if($auth_tenant->cloudRealms()->count() > 0) {
                foreach($auth_tenant->cloudRealms as $cloud_realm) {
                    $cloudRealmService->provisionGitGroup($cloud_realm->id);
                }
                $this->line('<fg=green>Cloud realms git_meta_data updated.</>');
            } else {
                $this->line('<fg=cyan>No cloud realms for this tenant.</>');
            }

            if($auth_tenant->cloudOrganizationUnits()->count() > 0) {
                foreach($auth_tenant->cloudOrganizationUnits as $organization_unit) {
                    $cloudOrganizationUnitService->provisionGitGroup($organization_unit->id);
                }
                $this->line('<fg=green>Cloud organization units git_meta_data updated.</>');
            } else {
                $this->line('<fg=cyan>No cloud organization units for this tenant.</>');
            }

            if($auth_tenant->cloudAccounts()->count() > 0) {
                foreach($auth_tenant->cloudAccounts as $account) {
                    $cloudAccountService->provisionGitGroup($account->id);
                }
                $this->line('<fg=green>Cloud accounts git_meta_data updated.</>');
            } else {
                $this->line('<fg=cyan>No cloud accounts for this tenant.</>');
            }

        }

        // Call the get method to display the tables of values for the record.
        $this->call('auth-tenant:get', [
            'short_id' => $auth_tenant->short_id
        ]);

    }

}
