<?php

namespace App\Console\Commands\Auth;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class AuthGroupGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'auth-group:get
                            {short_id? : The short ID of the group.}
                            {--slug= : The slug of the group.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get an Authentication Group by ID or slug';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // Get slug
        $slug = $this->option('slug');

        // If short ID or slug is not set, return an error message
        if($short_id == null && $slug == null) {
            $this->error('You did not specify the short id or slug to lookup the record.');
            $this->line('You can get a list of group roles using `auth-group:get --slug=my-group`');
            $this->line('You can get a list of role groups using `auth-role:get --slug=my-role`');
            $this->line('You can lookup by short ID using `auth-group:get a1b2c3d4`');
            $this->line('You can lookup by slug using `auth-group:get --slug=example`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If slug is specified, lookup by slug
        elseif($slug != null) {
            $auth_group = Models\Auth\AuthGroup::query()
                ->where('slug', $slug)
                ->first();
        }

        // If record not found, return an error message
        if($auth_group == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Auth Group');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $auth_group->id
                ],
                [
                    'short_id',
                    $auth_group->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$auth_group->authTenant->short_id.'] '.$auth_group->authTenant->slug
                ],
                [
                    'name',
                    $auth_group->name
                ],
                [
                    'slug',
                    $auth_group->slug
                ],
                [
                    'created_at',
                    $auth_group->created_at->toIso8601String()
                ],
            ]
        );

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            //
            // Auth Group - Child Relationship - Roles
            // --------------------------------------------------------------------
            // Loop through roles in Eloquent model and add calculated values to
            // array. The dot notation for pivot relationships doesn't work with a
            // get([]) so this is the best way to handle this.
            //

            // Loop through roles and add values to array
            $role_rows = [];
            foreach($auth_group->authRoles as $role) {
                $role_rows[] = [
                    'group_role_short_id' => $role->authGroupRole->short_id,
                    'role_short_id' => $role->short_id,
                    'role_slug' => $role->slug,
                    'expires_at' => $role->authGroupRole->expires_at ? $role->authGroupRole->expires_at : 'never'
                ];
            }

            $this->comment('');
            $this->comment('Auth Group Roles');

            // If rows exists, render a table or return a no results found message
            if(count($role_rows) > 0) {
                $this->table(
                    ['Group Role Short ID', 'Role Short ID', 'Role Slug', 'Expires at'],
                    $role_rows
                );
            } else {
                $this->error('No roles have been attached.');
            }

            //
            // Auth Group - Child Relationship - Users
            // --------------------------------------------------------------------
            // Loop through roles in Eloquent model and add calculated values to
            // array. The dot notation for pivot relationships doesn't work with a
            // get([]) so this is the best way to handle this.
            //

            // Loop through users and add values to array
            $user_rows = [];
            foreach($auth_group->authUsers as $user) {
                $user_rows[] = [
                    'group_user_short_id' => $user->authGroupUser->short_id,
                    'user_short_id' => $user->short_id,
                    'name' => $user->full_name,
                    'email' => $user->email,
                    'expires_at' => $user->authGroupUser->expires_at ? $user->authGroupUser->expires_at : 'never'
                ];
            }

            $this->comment('');
            $this->comment('Auth Group Users');

            // If rows exists, render a table or return a no results found message
            if(count($user_rows) > 0) {
                $this->table(
                    ['Group User Short ID', 'User Short ID', 'Name', 'Email', 'Expires at'],
                    $user_rows
                );
            } else {
                $this->error('No users have been attached.');
            }

        }

        $this->comment('');

    }
}
