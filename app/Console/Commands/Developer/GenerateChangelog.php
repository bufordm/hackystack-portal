<?php

namespace App\Console\Commands\Developer;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class GenerateChangelog extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'developer:generate-changelog
                            {milestone} : The milestone for this release}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Generate a list of changelog entries based on merge request descriptions.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->line('');
        $this->info('Generating a Changelog for '.$this->argument('milestone'));

        // Check if API token has been set or return an error
        if(!config('hackystack.developer.gitlab.api_token')) {
            $this->error('You have not set your GitLab API token in the `.env` file.');
            $this->line('1. Visit https://gitlab.com or your private GitLab instance.');
            $this->line('2. Navigate to User Settings > Personal Access Tokens');
            $this->line('3. Create a new token with the `api` scope.');
            $this->line('4. Update the `HS_DEVELOPER_GITLAB_API_TOKEN` variable in the `.env` file');
            $this->line('5. Update the `HS_DEVELOPER_GITLAB_API_USERNAE` variable in the `.env` file.');
            die();
        }

        // Connect to GitLab API using credentials defined in config/hackystack.php
        // and the .env file.
        $gitlab_client = new \Gitlab\Client();
        $gitlab_client->authenticate(config('hackystack.developer.gitlab.api_token'), \Gitlab\Client::AUTH_HTTP_TOKEN);
        $gitlab_client->setUrl(config('hackystack.developer.gitlab.base_url'));
        $gitlab_pager = new \Gitlab\ResultPager($gitlab_client);

        // Get details about the GitLab project
        $project = $gitlab_client->projects()->show(config('hackystack.developer.gitlab.project_id'));

        $this->line('Project Name: '.$project['name_with_namespace']);
        $this->line('Project URL: '.$project['web_url']);
        $this->line('Merge Requests: '.$project['_links']['merge_requests']);
        $this->line('');
        $this->line('Copy the outputs below this line to the Changelog file.');
        $this->line('<bg=cyan>--------------------------------------------------------------------------------</>');
        $this->line('');

        // Lookup milestone to get exact match
        $milestones = $gitlab_client->milestones()->all(config('hackystack.developer.gitlab.project_id'), ['search' => $this->argument('milestone')]);

        // If milestone matched, output details about the milestone
        if(count($milestones) == 1) {
            $this->comment('## '.$milestones[0]['title']);
            $this->line('* **URL:** '.$milestones[0]['web_url']);
            if($milestones[0]['start_date'] != null) {
                $this->line('* **Release Date:** '.$milestones[0]['due_date']);
            }
            $this->line('');
            if($milestones[0]['description'] != null) {
                $this->line('### Overview');
                $this->line($milestones[0]['description']);
            } else {
                $this->error('This milestone does not have a description that provides a high-level overview of this release.');
                die();
            }

        } else {
            $this->line('');
            $this->error('The milestone passed as an argument did not explicitely match an existing milestone.');

            // Get list of available milestones for quick reference spellcheck.
            $available_milestones = $gitlab_client->milestones()->all(config('hackystack.developer.gitlab.project_id'));
            $this->line('');
            $this->line('This is a list of available milestones for this project:');
            foreach($available_milestones as $available_milestone) {
                $this->line($available_milestone['title']);
            }

            // Additional instructions on accessing milestones
            $this->line('');
            $this->line('You can also visit the GitLab project milestones to make changes.');
            $this->line($project['web_url'].'/-/milestones');

            die();
        }

        //
        // Merge Requests
        //

        // Get merge requests for GitLab project with specific milestone
        $merge_requests = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'all', [config('hackystack.developer.gitlab.project_id'), [
            'milestone' => $this->argument('milestone'),
            'sort' => 'asc',
            'state' => 'merged',
        ]]);

        // Loop through merge requests and add to array
        $mr_outputs = [];
        foreach($merge_requests as $merge_request) {

            $mr_changelog_label = $this->getMergeRequestCategoryLabel($merge_request['labels']);

            // Add merge request to outputs
            $mr_outputs[] = '`'.$mr_changelog_label.'` '.$merge_request['title'].' - '.$merge_request['references']['short'].' - @'.$merge_request['author']['username'];

        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $mr_collection = collect($mr_outputs);
        $mr_collection_outputs = $mr_collection->sort()->values()->all();

        // Show list of merge requests in console output
        $this->line('');
        $this->comment('### Merge Requests ('.$mr_collection->count().')');
        foreach($mr_collection_outputs as $mr_collection_row) {
            $this->line('* '.$mr_collection_row);
        }


        //
        // Commits
        //

        // Loop through merge requests
        $commit_outputs = [];
        foreach($merge_requests as $merge_request) {

            $mr_changelog_label = $this->getMergeRequestCategoryLabel($merge_request['labels']);

            // Get commits for specific merge request
            $mr_commits = $gitlab_pager->fetchAll($gitlab_client->mergeRequests(), 'commits', [
                config('hackystack.developer.gitlab.project_id'),
                $merge_request['iid']
            ]);

            // Loop through commits and add values to array
            foreach($mr_commits as $mr_commit) {
                $commit_outputs[] = '`'.$mr_changelog_label.'` '.$mr_commit['title'].' - '.$mr_commit['short_id'].' - !'.$merge_request['iid'];
            }

        }

        // Convert the compiled array to a collection and sort alphabetically
        // Note: The sort() functionality sorts capitalized and lowercase letters
        // separately.
        $commit_collection = collect($commit_outputs);
        $commit_collection_outputs = $commit_collection->sort()->values()->all();

        // Show list of merge requests in console output
        $this->line('');
        $this->comment('### Commits ('.$commit_collection->count().')');
        foreach($commit_collection_outputs as $commit_collection_row) {
            $this->line('* '.$commit_collection_row);
        }

        $this->line('');
        $this->line('<bg=cyan>--------------------------------------------------------------------------------</>');
        $this->line('Copy the outputs above this line to the Changelog file.');

    }

    public function getMergeRequestCategoryLabel($merge_request_labels = [])
    {
        // Get label for categorizing changelog entry
        $merge_request_labels = collect($merge_request_labels);
        if($merge_request_labels->count() > 0) {
            // Loop through merge request labels
            foreach($merge_request_labels as $merge_request_label) {
                // If label has scope separator (::)
                if(strpos($merge_request_label, '::')) {
                    // Split string into prefix [0] and suffix [1] using `::` separator
                    $label_parts = explode('::', $merge_request_label);
                    // If label prefix is equal to config variable
                    if($label_parts[0] == config('hackystack.developer.gitlab.changelog_label')) {
                        // Set variable to suffix of label
                        $mr_changelog_label = $label_parts[1];
                    }
                }
            }
        } else {
            $mr_changelog_label = null;
        }

        return $mr_changelog_label;
    }
}
