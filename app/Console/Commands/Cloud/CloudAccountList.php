<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudAccountList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account:list
                            {--T|auth_tenant_slug= : The slug of the tenant that the organization units belong to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that organization units belongs to}
                            {--R|cloud_realm_slug= : The slug of the cloud realm the organization units belong to}
                            {--U|cloud_organization_unit_short_id= : The short ID of the cloud organization unit the cloud accounts belong to}
                            {--search=* : Wildcard search of name or slug.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get cloud provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // Get cloud realm slug
        $cloud_realm_slug = $this->option('cloud_realm_slug');

        // Get cloud organization unit short ID
        $cloud_organization_unit_short_id = $this->option('cloud_organization_unit_short_id');

        // Get search keywords
        $search_array = $this->option('search');

        // Get list of records from database
        $records = Models\Cloud\CloudAccount::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($cloud_provider_slug) {
                if($cloud_provider_slug) {
                    $query->whereHas('cloudProvider', function (Builder $query) use ($cloud_provider_slug) {
                        $query->where('slug', $cloud_provider_slug);
                    });
                }
            })->where(function($query) use ($cloud_realm_slug) {
                if($cloud_realm_slug) {
                    $query->whereHas('cloudRealm', function (Builder $query) use ($cloud_realm_slug) {
                        $query->where('slug', $cloud_realm_slug);
                    });
                }
            })->where(function($query) use ($cloud_organization_unit_short_id) {
                if($cloud_organization_unit_short_id) {
                    $query->whereHas('cloudOrganizationUnit', function (Builder $query) use ($cloud_organization_unit_short_id) {
                        $query->where('short_id', $cloud_organization_unit_short_id);
                    });
                }
            })->where(function($query) use ($search_array) {
                // If search keywords were specified, the array will not be empty
                if(count($search_array) > 0) {
                    foreach($search_array as $search_option) {
                        $query->where('name', 'LIKE', '%$search_option%');
                        $query->orWhere('slug', 'LIKE', '%$search_option%');
                    }
                }
            })->orderBy('slug')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $cloud_account_record) {
            $account_rows[] = [
                'short_id' => $cloud_account_record->short_id,
                'auth_tenant' => '['.$cloud_account_record->authTenant->short_id.'] '.$cloud_account_record->authTenant->slug,
                'cloud_provider' => '['.$cloud_account_record->cloudProvider->short_id.'] '.$cloud_account_record->cloudProvider->slug,
                'cloud_organization_unit' => '['.$cloud_account_record->cloudOrganizationUnit->short_id.'] '.$cloud_account_record->cloudOrganizationUnit->slug,
                'groups' => $cloud_account_record->authGroups()->count(),
                'users' => $cloud_account_record->authUsers()->count(),
                'name' => $cloud_account_record->name,
                'slug' => $cloud_account_record->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Accounts - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Organization Unit', 'Groups', 'Users', 'Name', 'Slug'];
            $this->table($headers, $account_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
