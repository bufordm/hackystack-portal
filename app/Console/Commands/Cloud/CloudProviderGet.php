<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudProviderGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-provider:get
                            {short_id? : The short ID of the provider.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Provider by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of cloud providers using `cloud-provider:list`');
            $this->line('You can lookup by short ID using `cloud-provider:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_provider == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Provider Values
        $this->comment('');
        $this->comment('Cloud Provider');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_provider->id
                ],
                [
                    'short_id',
                    $cloud_provider->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$cloud_provider->authTenant->short_id.'] '.$cloud_provider->authTenant->slug
                ],
                [
                    'type',
                    $cloud_provider->type
                ],
                [
                    'name',
                    $cloud_provider->name
                ],
                [
                    'slug',
                    $cloud_provider->slug
                ],
                [
                    'flag_provisioned',
                    $cloud_provider->flag_provisioned
                ],
                [
                    'created_at',
                    $cloud_provider->created_at->toIso8601String()
                ],
            ]
        );

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            //
            // Cloud Provider - Child Relationship - Cloud Realms
            // --------------------------------------------------------------------
            // Loop through realms in Eloquent model and add calculated
            // values to array. The dot notation for pivot relationships doesn't
            // work with a get([]) so this is the best way to handle this.
            //

            // Loop through realms and add values to array
            $realm_rows = [];
            foreach($cloud_provider->cloudRealms as $realm) {
                $realm_rows[] = [
                    'short_id' => $realm->short_id,
                    'name' => $realm->name,
                    'slug' => $realm->slug,
                ];
            }

            $this->comment('');
            $this->comment('Cloud Realms');

            // If rows exists, render a table or return a no results found message
            if(count($realm_rows) > 0) {
                $this->table(
                    ['Short ID', 'Name', 'Slug'],
                    $realm_rows
                );
            } else {
                $this->error('No cloud realms have been attached.');
            }

            //
            // Cloud Provider - Child Relationship - Cloud Organization Units
            // --------------------------------------------------------------------
            // Loop through organization units in Eloquent model and add calculated
            // values to array. The dot notation for pivot relationships doesn't
            // work with a get([]) so this is the best way to handle this.
            //

            // Loop through organization units and add values to array
            $organization_unit_rows = [];
            foreach($cloud_provider->cloudOrganizationUnits as $unit) {
                $organization_unit_rows[] = [
                    'account_short_id' => $unit->short_id,
                    'parent_cloud_organization_unit' => $unit->cloud_organization_unit_id_parent ? '['.$unit->parentCloudOrganizationUnit->short_id.'] '.$unit->parentCloudOrganizationUnit->slug : '',
                    'name' => $unit->name,
                    'slug' => $unit->slug,
                ];
            }

            $this->comment('');
            $this->comment('Cloud Organization Units');

            // If rows exists, render a table or return a no results found message
            if(count($organization_unit_rows) > 0 && count($organization_unit_rows) < 100) {
                $this->table(
                    ['OU Short ID', 'Parent OU', 'Name', 'Slug'],
                    $organization_unit_rows
                );
            } elseif(count($organization_unit_rows) > 100) {
                $this->comment('There are '.count($organization_unit_rows).' cloud organization units attached to this provider.');
            } else {
                $this->error('No cloud organization units have been attached.');
            }

            //
            // Cloud Provider - Child Relationship - Cloud Account
            // --------------------------------------------------------------------
            // Loop through accounts in Eloquent model and add calculated values to
            // array. The dot notation for pivot relationships doesn't work with a
            // get([]) so this is the best way to handle this.
            //

            // Loop through accounts and add values to array
            $account_rows = [];
            foreach($cloud_provider->cloudAccounts as $account) {
                $account_rows[] = [
                    'account_short_id' => $account->short_id,
                    'name' => $account->name,
                    'slug' => $account->slug,
                ];
            }

            $this->comment('');
            $this->comment('Cloud Accounts');

            // If rows exists, render a table or return a no results found message
            if(count($account_rows) > 0 && count($account_rows) < 100) {
                $this->table(
                    ['Account Short ID', 'Name', 'Slug'],
                    $account_rows
                );
            } elseif(count($account_rows) > 100) {
                $this->comment('There are '.count($account_rows).' cloud accounts attached to this provider.');
            } else {
                $this->error('No cloud accounts have been attached.');
            }

        } // if($this->option('without-child-relationships') == false)

        $this->comment('');

    }
}
