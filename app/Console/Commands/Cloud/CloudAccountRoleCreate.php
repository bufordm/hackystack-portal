<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Database\Eloquent\Builder;

class CloudAccountRoleCreate extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-role:create
                            {--T|auth_tenant_slug= : The slug of the tenant that the group exists in}
                            {--C|cloud_account_short_id= : The short ID of the Cloud Account}
                            {--N|api_name= : The name of this role defined in the API docs}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Attach a Role to an Cloud Account';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        $this->comment('');
        $this->comment('Cloud Account Role - Create Record');

        //
        // Authentication Tenant
        // --------------------------------------------------------------------
        // Lookup the authentication tenant or provide list of autocomplete
        // options for the user to choose from.
        //

        // Get auth tenant from console option
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant does the user exist in?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Account
        // --------------------------------------------------------------------
        // Lookup the cloud account or provide list of autocomplete options for
        // the user to choose from.
        //

        // Get cloud account from console option
        $cloud_account_short_id = $this->option('cloud_account_short_id');

        // If cloud account ID option was specified, lookup by ID
        if($cloud_account_short_id) {
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('short_id', $cloud_account_short_id)
                ->first();
        }

        // If cloud account ID was not provided, prompt for input
        else {

            // Get list of groups to show in console
            $cloud_accounts = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->table(
                ['Group Slug'],
                $cloud_accounts
            );

            $cloud_account_prompt = $this->anticipate('Which cloud account should the user be attached to?', Arr::flatten($cloud_accounts));

            // Lookup group based on slug provided in prompt
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_account_prompt)
                ->first();

        }

        // Validate that group exists or return error message
        if(!$cloud_account) {
            $this->error('Error: No cloud account was found.');
            $this->error('');
            die();
        }

        //
        // Additional String Parameters and Options
        // --------------------------------------------------------------------
        //

        // Name
        $api_name = $this->option('api_name');
        if($api_name == null) {
            $api_name = $this->ask('What is the name of this role defined in the API docs?');
        }

        //
        // Verify inputs table
        // --------------------------------------------------------------------
        // Show the user the database values that will be added before saving
        // changes.
        //

        $this->table(
            ['Column', 'Value'],
            [
                [
                    'auth_tenant_id',
                    '['.$auth_tenant->short_id.'] '.$auth_tenant->slug
                ],
                [
                    'cloud_account_id',
                    '['.$cloud_account->short_id.'] '.$cloud_account->slug
                ],
                [
                    'api_name',
                    $api_name
                ],
            ]
        );

        // Ask for confirmation to abort creation.
        if($this->confirm('Do you want to abort the creation of the record?')) {
            $this->error('Error: You aborted. No record was created.');
            die();
        }

        // Initialize service
        $cloudAccountRoleService = new Services\V1\Cloud\CloudAccountRoleService();

        // Use service to create record
        $record = $cloudAccountRoleService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_account_id' => $cloud_account->id,
            'cloud_provider_id' => $cloud_account->cloud_provider_id,
            'cloud_realm_id' => $cloud_account->cloud_realm_id,
            'api_name' => $api_name,
        ]);

        // Show result in console
        $this->comment('Record created successfully.');

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-account-role:get', [
            'short_id' => $record->short_id
        ]);

        $this->comment('This Cloud Account Role does not have any users.');
        $this->error('Use `cloud-account-user-role:create` to grant permissions.');

    }

}
