<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudAccountUserGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user:get
                            {short_id? : The short ID of the cloud account user.}
                            {--P|with-password : The IAM password will be revealed in the output}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Account User by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->comment('You can lookup by short ID using `cloud-account-user:get a1b2c3d4`');
            $this->comment('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_account_user == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Account User Values
        $this->comment('');
        $this->comment('Cloud Account User');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_account_user->id
                ],
                [
                    'short_id',
                    $cloud_account_user->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$cloud_account_user->authTenant->short_id.'] '.$cloud_account_user->authTenant->slug
                ],
                [
                    '[relationship] cloudProvider',
                    '['.$cloud_account_user->cloudProvider->short_id.'] '.$cloud_account_user->cloudProvider->slug
                ],
                [
                    '[relationship] cloudAccount',
                    '['.$cloud_account_user->cloudAccount->short_id.'] '.$cloud_account_user->cloudAccount->slug
                ],
                [
                    '[relationship] authUser',
                    '['.$cloud_account_user->authUser->short_id.'] '.$cloud_account_user->authUser->full_name.' <'.$cloud_account_user->authUser->email.'>'
                ],
                [
                    'username',
                    $cloud_account_user->username
                ],
                [
                    'password',
                    $this->option('with-password') ? decrypt($cloud_account_user->password) : '[MASKED] Use flag `--with-password` to reveal'
                ],
                [
                    'created_at',
                    $cloud_account_user->created_at->toIso8601String()
                ],
                [
                    'created_by',
                    $cloud_account_user->created_by ? '['.$cloud_account_user->createdBy->short_id.'] '.$cloud_account_user->createdBy->full_name : 'System',
                ],
                [
                    'provisioned_at',
                    $cloud_account_user->provisioned_at ? $cloud_account_user->provisioned_at->toIso8601String() : 'never'
                ],
                [
                    'expires_at',
                    $cloud_account_user->expires_at ? $cloud_account_user->expires_at->toIso8601String() : 'never'
                ],
                [
                    'state',
                    $cloud_account_user->state
                ],
            ]
        );

        $api_meta_data_values = [];
        foreach($cloud_account_user->api_meta_data as $meta_data_key => $meta_data_value) {
            $api_meta_data_values[] = [
                $meta_data_key,
                is_array($meta_data_value) ? json_encode($meta_data_value) : $meta_data_value
            ];
        }

        $this->table(
            ['API Meta Data Column', 'API Value'],
            $api_meta_data_values
        );

        $this->comment('');

    }
}
