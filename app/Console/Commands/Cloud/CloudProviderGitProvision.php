<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudProviderGitProvision extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-provider:git-provision
                            {short_id? : The short ID of the provider.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Create or update the Git group meta data for a cloud provider';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of cloud providers using `cloud-provider:list`');
            $this->line('You can lookup by short ID using `cloud-provider:provision a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_provider == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Call the get method to display the tables of values for the record.
        $this->call('cloud-provider:get', [
            'short_id' => $cloud_provider->short_id,
            '--without-child-relationships' => true
        ]);

        $this->comment('');

        // Initialize service
        $cloudProviderService = new Services\V1\Cloud\CloudProviderService();

        // Use service to provision Git group using API
        $cloudProviderService->provisionGitGroup($cloud_provider->id);

        // Get refreshed cloud provider with updated meta data
        $cloud_provider = $cloud_provider->fresh();

        // Check if git_meta_data field has a value
        if($cloud_provider->git_meta_data != null) {

            // Loop through meta data and add values to array
            $git_meta_data_rows = [];
            foreach($cloud_provider->git_meta_data as $meta_data_key => $meta_data_value) {
                $git_meta_data_rows[] = [
                    $meta_data_key,
                    $meta_data_value
                ];
            }

            $this->comment('');
            $this->comment('Git Meta Data');

            $this->table(
                ['Meta Data Key', 'Meta Data Value'],
                $git_meta_data_rows
            );

        } else {
            $this->line('<fg=red>Provider git meta data is empty.</>');
        }

    }
}
