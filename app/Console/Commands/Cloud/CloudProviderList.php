<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudProviderList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-provider:list
                            {--T|auth_tenant_slug= : The slug of the tenant that providers belong to}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Providers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get list of records from database
        $records = Models\Cloud\CloudProvider::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->orderBy('slug')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $provider_record) {
            $provider_rows[] = [
                'short_id' => $provider_record->short_id,
                'auth_tenant' => '['.$provider_record->authTenant->short_id.'] '.$provider_record->authTenant->slug,
                'realms' => $provider_record->cloudRealms()->count(),
                'accounts' => $provider_record->cloudAccounts()->count(),
                'type' => $provider_record->type,
                'name' => $provider_record->name,
                'slug' => $provider_record->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Providers - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Realms', 'Accounts', 'Type', 'Name', 'Slug'];
            $this->table($headers, $provider_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
