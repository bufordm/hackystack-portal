<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudBillingAccountGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-billing-account:get
                            {short_id? : The short ID of the billing account.}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Billing Account by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of billing accounts using `cloud-billing-account:list`');
            $this->line('You can lookup by short ID using `cloud-billing-account:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_billing_account == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Auth Group Values
        $this->comment('');
        $this->comment('Cloud Billing Account');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_billing_account->id
                ],
                [
                    'short_id',
                    $cloud_billing_account->short_id
                ],
                [
                    'auth_tenant_id',
                    $cloud_billing_account->auth_tenant_id
                ],
                [
                    'cloud_provider_id',
                    $cloud_billing_account->cloud_provider_id
                ],
                [
                    'name',
                    $cloud_billing_account->name
                ],
                [
                    'slug',
                    $cloud_billing_account->slug
                ],
                [
                    'created_at',
                    $cloud_billing_account->created_at->toIso8601String()
                ],
            ]
        );

        // Auth Tenant Values
        $this->comment('');
        $this->comment('Auth Tenant');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $cloud_billing_account->authTenant->short_id
                ],
                [
                    'name',
                    $cloud_billing_account->authTenant->name
                ],
                [
                    'slug',
                    $cloud_billing_account->authTenant->slug
                ],
            ]
        );

        // Cloud Provider Values
        $this->comment('');
        $this->comment('Cloud Provider');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'short_id',
                    $cloud_billing_account->cloudProvider->short_id
                ],
                [
                    'name',
                    $cloud_billing_account->cloudProvider->name
                ],
                [
                    'slug',
                    $cloud_billing_account->cloudProvider->slug
                ],
            ]
        );

        //
        // Cloud Billing Account - Child Relationship - Cloud Realms
        // --------------------------------------------------------------------
        // Loop through realms in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        //

        // Loop through roles and add values to array
        $realm_rows = [];
        foreach($cloud_billing_account->cloudRealms as $realm) {
            $realm_rows[] = [
                'realm_short_id' => $realm->short_id,
                'realm_name' => $realm->name,
                'realm_slug' => $realm->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Realms');

        // If rows exists, render a table or return a no results found message
        if(count($realm_rows) > 0) {
            $this->table(
                ['Realm Short ID', 'Realm Name', 'Realm Slug'],
                $realm_rows
            );
        } else {
            $this->error('No realms have been attached.');
        }

        //
        // Cloud Billing Account - Child Relationship - Cloud Account
        // --------------------------------------------------------------------
        // Loop through accounts in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        //

        // Loop through users and add values to array
        $account_rows = [];
        foreach($cloud_billing_account->cloudAccounts as $account) {
            $account_rows[] = [
                'account_short_id' => $account->short_id,
                'cloud_realm' => '['.$account->cloudRealm->short_id.'] '.$account->cloudRealm->slug,
                'name' => $account->name,
                'slug' => $account->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Accounts');

        // If rows exists, render a table or return a no results found message
        if(count($account_rows) > 0 && count($account_rows) < 100) {
            $this->table(
                ['Account Short ID', 'Cloud Realm', 'Name', 'Slug'],
                $account_rows
            );
        } elseif(count($account_rows) > 100) {
            $this->comment('There are '.count($account_rows).' cloud accounts attached to this billing account.');
        } else {
            $this->error('No cloud accounts have been attached.');
        }

        $this->comment('');

    }
}
