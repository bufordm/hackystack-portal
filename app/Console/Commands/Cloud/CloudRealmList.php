<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudRealmList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-realm:list
                            {--T|auth_tenant_slug= : The slug of the tenant that realms belong to}
                            {--B|cloud_billing_account_slug= : The slug of the billing account that realms belong to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that realms belongs to}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Providers';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get billing account slug
        $cloud_billing_account_slug = $this->option('cloud_billing_account_slug');

        // Get cloud provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // Get list of records from database
        $records = Models\Cloud\CloudRealm::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($cloud_billing_account_slug) {
                if($cloud_billing_account_slug) {
                    $query->whereHas('cloudBillingAccount', function (Builder $query) use ($cloud_billing_account_slug) {
                        $query->where('slug', $cloud_billing_account_slug);
                    });
                }
            })->where(function($query) use ($cloud_provider_slug) {
                if($cloud_provider_slug) {
                    $query->whereHas('cloudProvider', function (Builder $query) use ($cloud_provider_slug) {
                        $query->where('slug', $cloud_provider_slug);
                    });
                }
            })->orderBy('slug')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $realm_record) {
            $realm_rows[] = [
                'short_id' => $realm_record->short_id,
                'auth_tenant' => '['.$realm_record->authTenant->short_id.'] '.$realm_record->authTenant->slug,
                'cloud_provider' => '['.$realm_record->cloudProvider->short_id.'] '.$realm_record->cloudProvider->slug,
                'cloud_billing_account' => '['.$realm_record->cloudBillingAccount->short_id.'] '.$realm_record->cloudBillingAccount->slug,
                'accounts' => $realm_record->cloudAccounts()->count(),
                'name' => $realm_record->name,
                'slug' => $realm_record->slug,
                'created_at' => $realm_record->created_at->toIso8601String(),
            ];
        }

        $this->comment('');
        $this->comment('Cloud Realms - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Billing Account', 'Accounts', 'Name', 'Slug'];
            $this->table($headers, $realm_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
