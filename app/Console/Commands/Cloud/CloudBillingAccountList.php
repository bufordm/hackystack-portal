<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudBillingAccountList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-billing-account:list
                            {--T|auth_tenant_slug= : The slug of the tenant that billing accounts belong to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that billing accounts belongs to}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Billing Accounts';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get cloud provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // Get list of records from database
        $records = Models\Cloud\CloudBillingAccount::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($cloud_provider_slug) {
                if($cloud_provider_slug) {
                    $query->whereHas('cloudProvider', function (Builder $query) use ($cloud_provider_slug) {
                        $query->where('slug', $cloud_provider_slug);
                    });
                }
            })->orderBy('slug')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $billing_account_record) {
            $billing_account_rows[] = [
                'short_id' => $billing_account_record->short_id,
                'auth_tenant' => '['.$billing_account_record->authTenant->short_id.'] '.$billing_account_record->authTenant->slug,
                'cloud_provider' => '['.$billing_account_record->cloudProvider->short_id.'] '.$billing_account_record->cloudProvider->slug,
                'realms' => $billing_account_record->cloudRealms()->count(),
                'accounts' => $billing_account_record->cloudAccounts()->count(),
                'name' => $billing_account_record->name,
                'slug' => $billing_account_record->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Billing Accounts - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Realms', 'Accounts', 'Name', 'Slug'];
            $this->table($headers, $billing_account_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
