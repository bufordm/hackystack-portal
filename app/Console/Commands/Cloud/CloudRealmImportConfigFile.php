<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use App\Models\Cloud\CloudOrganizationUnit;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudRealmImportConfigFile extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-realm:import-config-file
                            {--T|auth_tenant_slug= : The slug of the tenant that organization unit belong to}
                            {--B|cloud_billing_account_slug= : The slug of the billing account these realms belongs to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider these organization units belongs to}
                            {--F|config_file_name= : The file name (without .php extension) in the config/ directory that you want to import. Default hackystack-cloud-realms}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Parse the config/hackystack-cloud-realms.php file and create cloud realms, organization units, and cloud account records.';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return mixed
     */
    public function handle()
    {

        //
        // Configuration File
        // --------------------------------------------------------------------
        //

        $config_file_name = $this->option('config_file_name');

        // If configuration file was not specified, use default
        if(!$config_file_name) {
            $config_file_name = 'hackystack-cloud-realms';
        }

        // Verify that file exists in config/ directory
        if(!config($config_file_name)) {
            $this->error('Error: No file named `config/'.$config_file_name.'.php` was found.');
            $this->error('');
            die();
        }

        // If no interaction flag is not set, show preview output
        if($this->option('no-interaction') == false) {

            // Parse Configuration File to Validate Syntax
            // Loop through configuration file array
            foreach(config($config_file_name) as $realm) {

                // Add row to console table output
                $preview_realms_table_output[] = [
                    Arr::exists($realm, 'id') ? substr($realm['id'], 0, 8) : '',
                    $realm['slug'],
                ];

                // Define empty array for table if no other records exist
                $preview_organization_units_table_output = [];

                // Loop through organization units for the realm
                foreach($realm['organization_units'] as $unit) {

                    // Add row to console table output
                    $preview_organization_units_table_output[] = [
                        Arr::exists($unit, 'id') ? substr($unit['id'], 0, 8) : '',
                        $realm['slug'],
                        $unit['slug']
                    ];

                    // Loop through child accounts for the realm (root level organization unit)
                    foreach($unit['accounts'] as $account) {

                        // Add row to console table output
                        $preview_cloud_accounts_table_output[] = [
                            Arr::exists($account, 'id') ? substr($account['id'], 0, 8) : '',
                            $realm['slug'],
                            $unit['slug'],
                            $account['slug'],
                        ];

                    } // foreach($account)

                } // foreach($units)

                // Define empty array for table if no other records exist
                $preview_cloud_accounts_table_output = [];

                // Loop through child accounts for the realm (root level organization unit)
                foreach($realm['accounts'] as $account) {

                    // Add row to console table output
                    $preview_cloud_accounts_table_output[] = [
                        Arr::exists($account, 'id') ? substr($account['id'], 0, 8) : '',
                        $realm['slug'],
                        '(root)',
                        $account['slug'],
                    ];

                } // foreach($account)

            } // foreach(config)

            // Show table in console with changes
            $preview_realms_table_headers = ['Existing ID', 'Realm'];
            $this->table($preview_realms_table_headers, $preview_realms_table_output);

            $preview_organization_units_table_headers = ['Existing ID', 'Realm', 'Organization Unit'];
            $this->table($preview_organization_units_table_headers, $preview_organization_units_table_output);

            $preview_cloud_accounts_table_headers = ['Existing ID', 'Realm', 'Organization Unit', 'Account'];
            $this->table($preview_cloud_accounts_table_headers, $preview_cloud_accounts_table_output);

            // Ask for confirmation to abort creation.
            if($this->confirm('Do you want to abort the import of these realms, organization units and cloud accounts?')) {
                $this->error('Error: You aborted. No records were created.');
                die();
            }

        }

        //
        // Auth Tenant
        // --------------------------------------------------------------------
        //

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // If tenant slug option was specified, lookup by slug
        if($auth_tenant_slug) {
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_slug)
                ->first();
        }

        // If tenant slug was not provided, prompt for input
        else {

            // Get list of tenants to show in console
            $auth_tenants = Models\Auth\AuthTenant::get(['slug'])->toArray();

            $this->line('');
            $this->line('Available tenants: '.implode(',', Arr::flatten($auth_tenants)));

            $auth_tenant_prompt = $this->anticipate('Which tenant should these cloud organization units belong to?', Arr::flatten($auth_tenants));

            // Lookup tenant based on slug provided in prompt
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('slug', $auth_tenant_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$auth_tenant) {
            $this->error('Error: No tenant was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Provider
        // --------------------------------------------------------------------
        //

        // Get provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // If provider slug option was specified, lookup by slug
        if($cloud_provider_slug) {
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_slug)
                ->first();
        }

        // If provider slug was not provided, prompt for input
        else {

            // Get list of providers to show in console
            $cloud_providers = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->get(['slug'])
                ->toArray();

            $this->line('');
            $this->line('Available cloud providers: '.implode(',', Arr::flatten($cloud_providers)));

            $cloud_provider_prompt = $this->anticipate('Which cloud provider should this organization unit belong to?', Arr::flatten($cloud_providers));

            // Lookup cloud provider based on slug provided in prompt
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('slug', $cloud_provider_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_provider) {
            $this->error('Error: No cloud provider was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Cloud Billing Account
        // --------------------------------------------------------------------
        //

        // Get billing account slug
        $cloud_billing_account_slug = $this->option('cloud_billing_account_slug');


        // If billing account slug option was specified, lookup by slug
        if($cloud_billing_account_slug) {
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_billing_account_slug)
                ->firstOrFail();
        }

        // If billing account slug was not provided, prompt for input
        else {

            // Get list of billing accounts to show in console
            $cloud_billing_accounts = Models\Cloud\CloudBillingAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->get(['slug'])
                ->toArray();

            // If no billing accounts exist, then return an error
            if(count($cloud_billing_accounts) == 0) {
                $this->error('Error: No billing accounts were found for the tenant.');
                $this->comment('Please create a billing account before creating a realm.');
                $this->line('cloud-billing-account:create');
                $this->error('');
                die();
            }

            $this->line('');
            $this->line('Available billing accounts: '.implode(',', Arr::flatten($cloud_billing_accounts)));

            $cloud_billing_account_prompt = $this->anticipate('Which billing account should this realm belong to?', Arr::flatten($cloud_billing_accounts));

            // Lookup billing account based on slug provided in prompt
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $cloud_billing_account_prompt)
                ->first();

        }

        // Validate that tenant exists or return error message
        if(!$cloud_billing_account) {
            $this->error('Error: No billing account was found with that slug.');
            $this->error('');
            die();
        }

        //
        // Data File Parsing
        // --------------------------------------------------------------------
        //

        // Initialize service for creating records
        $cloudRealmService = new Services\V1\Cloud\CloudRealmService();
        $cloudOrganizationUnitService = new Services\V1\Cloud\CloudOrganizationUnitService();
        $cloudAccountService = new Services\V1\Cloud\CloudAccountService();

        // Define empty array for table if no other records exist
        $realms_table_output = [];
        $organization_units_table_output = [];
        $cloud_accounts_table_output = [];

        // Loop through configuration file array
        foreach(config($config_file_name) as $realm) {

            // Lookup cloud realm
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('auth_tenant_id', $auth_tenant->id)
                ->where('cloud_provider_id', $cloud_provider->id)
                ->where('slug', $realm['slug'])
                ->first();

            // If realm exists, update the record with any changes
            if($cloud_realm) {
                // Placeholder for future field matching and updates (beyond the `name` field)

                // Add row to console table output
                $realms_table_output[] = [
                    $cloud_provider->slug,
                    $cloud_realm->short_id,
                    $cloud_realm->name,
                    $cloud_realm->slug,
                    '<fg=cyan>exists</>'
                ];
            }

            // Validate that realm exists or create a new realm
            if(!$cloud_realm) {

                // Use service to create record
                $cloud_realm = $cloudRealmService->store([
                    'auth_tenant_id' => $auth_tenant->id,
                    'cloud_billing_account_id' => $cloud_billing_account->id,
                    'cloud_provider_id' => $cloud_provider->id,
                    'name' => $realm['name'],
                    'slug' => $realm['slug'],
                ]);

                sleep(2);

                // Add row to console table output
                $realms_table_output[] = [
                    $cloud_provider->slug,
                    $cloud_realm->short_id,
                    $cloud_realm->name,
                    $cloud_realm->slug,
                    '<fg=green>created</>'
                ];
            }

            // Check if existing ID is specified in array
            if(Arr::exists($realm, 'id')) {

                // Check if ID is different than existing database record
                if($realm['id'] != $cloud_realm->id) {

                    // Validate if UUID is valid format
                    if(!\Ramsey\Uuid\Uuid::isValid($realm['id'])) {
                        $this->error('Error: The existing ID provided for the realm ('.$realm['slug'].') is not formatted as a UUID.');
                        $this->error('');
                        die();
                    }

                    // Update ID on database model
                    $cloud_realm->id = $realm['id'];
                    $cloud_realm->short_id = substr($realm['id'], 0, 8);
                    $cloud_realm->save();

                    // Refresh database model for variable use later in this method
                    $cloud_realm = $cloud_realm->fresh();

                    // Add row to console table output
                    $realms_table_output[] = [
                        $cloud_provider->slug,
                        $cloud_realm->short_id,
                        $cloud_realm->name,
                        $cloud_realm->slug,
                        '<fg=yellow>existing id changed</>'
                    ];

                } // if($realm['id'] !=)
            } // if(Arr::exists($realm))

            // Lookup root organization unit for realm
            $organization_unit_root = Models\Cloud\CloudOrganizationUnit::query()
                ->where('cloud_realm_id', $cloud_realm->id)
                ->where('slug', $cloud_realm->slug)
                ->firstOrFail();

            // Loop through organization units for the realm
            foreach($realm['organization_units'] as $unit) {

                // Lookup cloud organization unit
                $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                    ->where('auth_tenant_id', $auth_tenant->id)
                    ->where('cloud_realm_id', $cloud_realm->id)
                    ->where('slug', $unit['slug'])
                    ->first();

                // If organization unit exists, update the record with any changes
                if($cloud_organization_unit) {
                    // Placeholder for future field matching and updates (beyond the `name` field)

                    // Add row to console table output
                    $organization_units_table_output[] = [
                        $cloud_provider->slug,
                        $cloud_organization_unit->short_id,
                        $cloud_realm->slug,
                        $cloud_organization_unit->slug,
                        '<fg=cyan>exists</>'
                    ];
                }

                // Validate that organization unit exists or create a new organization unit
                if(!$cloud_organization_unit) {

                    // Use service to create record
                    $cloud_organization_unit = $cloudOrganizationUnitService->store([
                        'auth_tenant_id' => $auth_tenant->id,
                        'cloud_organization_unit_id_parent' => $organization_unit_root->id,
                        'cloud_provider_id' => $cloud_provider->id,
                        'cloud_realm_id' => $cloud_realm->id,
                        'name' => $unit['name'],
                        'slug' => $unit['slug'],
                    ]);

                    sleep(2);

                    // Add row to console table output
                    $organization_units_table_output[] = [
                        $cloud_provider->slug,
                        $cloud_organization_unit->short_id,
                        $cloud_realm->slug,
                        $cloud_organization_unit->slug,
                        '<fg=green>created</>'
                    ];

                } // if(!$cloud_organization_unit)

                // Check if existing ID is specified in array
                if(Arr::exists($unit, 'id')) {

                    // Check if ID is different than existing database record
                    if($unit['id'] != $cloud_organization_unit->id) {

                        // Validate if UUID is valid format
                        if(!\Ramsey\Uuid\Uuid::isValid($unit['id'])) {
                            $this->error('Error: The existing ID provided for the organization unit ('.$unit['slug'].') is not formatted as a UUID.');
                            $this->error('');
                            die();
                        }

                        // Update ID on database model
                        $cloud_organization_unit->id = $unit['id'];
                        $cloud_organization_unit->short_id = substr($unit['id'], 0, 8);
                        $cloud_organization_unit->save();

                        // Refresh database model for variable use later in this method
                        $cloud_organization_unit = $cloud_organization_unit->fresh();

                        // Add row to console table output
                        $organization_units_table_output[] = [
                            $cloud_provider->slug,
                            $cloud_organization_unit->short_id,
                            $cloud_realm->slug,
                            $cloud_organization_unit->slug,
                            '<fg=yellow>existing id changed</>'
                        ];

                    } // if($unit['id'] !=)
                } // if(Arr::exists(id))

                // Loop through child accounts for the organization unit
                foreach($unit['accounts'] as $account) {

                    // Lookup cloud account
                    $cloud_account = Models\Cloud\CloudAccount::query()
                        ->where('auth_tenant_id', $auth_tenant->id)
                        ->where('cloud_realm_id', $cloud_realm->id)
                        ->where('cloud_organization_unit_id', $cloud_organization_unit->id)
                        ->where('slug', $account['slug'])
                        ->first();

                    // If cloud account exists, update the record with any changes
                    if($cloud_account) {
                        // Placeholder for future field matching and updates (beyond the `name` field)

                        // Add row to console table output
                        $cloud_accounts_table_output[] = [
                            $cloud_provider->slug,
                            $cloud_account->short_id,
                            $cloud_realm->slug,
                            $cloud_organization_unit->slug,
                            $cloud_account->slug,
                            '<fg=cyan>exists</>'
                        ];
                    }

                    // Validate that account exists or create a new cloud account
                    if(!$cloud_account) {

                        // Use service to create record
                        $cloud_account = $cloudAccountService->store([
                            'auth_tenant_id' => $auth_tenant->id,
                            'cloud_billing_account_id' => $cloud_realm->cloud_billing_account_id,
                            'cloud_organization_unit_id' => $cloud_organization_unit->id,
                            'cloud_provider_id' => $cloud_provider->id,
                            'cloud_realm_id' => $cloud_realm->id,
                            'name' => $account['name'],
                            'slug' => $account['slug'],
                        ]);

                        sleep(2);

                        // Add row to console table output
                        $cloud_accounts_table_output[] = [
                            $cloud_provider->slug,
                            $cloud_account->short_id,
                            $cloud_realm->slug,
                            $cloud_organization_unit->slug,
                            $cloud_account->slug,
                            '<fg=green>created</>'
                        ];

                    } // if(!$cloud_account)

                    // Check if existing ID is specified in array
                    if(Arr::exists($account, 'id')) {

                        // Check if ID is different than existing database record
                        if($account['id'] != $cloud_account->id) {

                            // Validate if UUID is valid format
                            if(!\Ramsey\Uuid\Uuid::isValid($account['id'])) {
                                $this->error('Error: The existing ID provided for the cloud account ('.$account['slug'].') is not formatted as a UUID.');
                                $this->error('');
                                die();
                            }

                            // Update ID on database model
                            $cloud_account->id = $account['id'];
                            $cloud_account->short_id = substr($account['id'], 0, 8);
                            $cloud_account->save();

                            // Refresh database model for variable use later in this method
                            $cloud_account = $cloud_account->fresh();

                            // Add row to console table output
                            $cloud_accounts_table_output[] = [
                                $cloud_provider->slug,
                                $cloud_account->short_id,
                                $cloud_realm->slug,
                                $organization_unit_root->slug,
                                $cloud_account->slug,
                                '<fg=yellow>existing id changed</>'
                            ];

                        } // if($account['id'] !=)
                    } // if(Arr::exists(id))

                } // foreach($account)
            } // foreach($unit)



            // Loop through child accounts for the realm (root level organization unit)
            foreach($realm['accounts'] as $account) {

                // Lookup cloud account
                $cloud_account = Models\Cloud\CloudAccount::query()
                    ->where('auth_tenant_id', $auth_tenant->id)
                    ->where('cloud_realm_id', $cloud_realm->id)
                    ->where('cloud_organization_unit_id', $organization_unit_root->id)
                    ->where('slug', $account['slug'])
                    ->first();

                // If cloud account exists, update the record with any changes
                if($cloud_account) {
                    // Placeholder for future field matching and updates (beyond the `name` field)

                    // Add row to console table output
                    $cloud_accounts_table_output[] = [
                        $cloud_provider->slug,
                        $cloud_account->short_id,
                        $cloud_realm->slug,
                        $organization_unit_root->slug,
                        $cloud_account->slug,
                        '<fg=cyan>exists</>'
                    ];
                }

                // Validate that account exists or create a new cloud account
                if(!$cloud_account) {

                    // Use service to create record
                    $cloud_account = $cloudAccountService->store([
                        'auth_tenant_id' => $auth_tenant->id,
                        'cloud_billing_account_id' => $cloud_realm->cloud_billing_account_id,
                        'cloud_organization_unit_id' => $organization_unit_root->id,
                        'cloud_provider_id' => $cloud_provider->id,
                        'cloud_realm_id' => $cloud_realm->id,
                        'name' => $account['name'],
                        'slug' => $account['slug'],
                    ]);

                    sleep(2);

                    // Add row to console table output
                    $cloud_accounts_table_output[] = [
                        $cloud_provider->slug,
                        $cloud_account->short_id,
                        $cloud_realm->slug,
                        $organization_unit_root->slug,
                        $cloud_account->slug,
                        '<fg=green>created</>'
                    ];

                } // if(!$cloud_account)

                // Check if existing ID is specified in array
                if(Arr::exists($account, 'id')) {

                    // Check if ID is different than existing database record
                    if($account['id'] != $cloud_account->id) {

                        // Validate if UUID is valid format
                        if(!\Ramsey\Uuid\Uuid::isValid($account['id'])) {
                            $this->error('Error: The existing ID provided for the cloud account ('.$account['slug'].') is not formatted as a UUID.');
                            $this->error('');
                            die();
                        }

                        // Update ID on database model
                        $cloud_account->id = $account['id'];
                        $cloud_account->short_id = substr($account['id'], 0, 8);
                        $cloud_account->save();

                        // Refresh database model for variable use later in this method
                        $cloud_account = $cloud_account->fresh();

                        // Add row to console table output
                        $cloud_accounts_table_output[] = [
                            $cloud_provider->slug,
                            $cloud_account->short_id,
                            $cloud_realm->slug,
                            $organization_unit_root->slug,
                            $cloud_account->slug,
                            '<fg=yellow>existing id changed</>'
                        ];

                    } // if($account['id'] !=)
                } // if(Arr::exists(id))
            } // foreach($account)

        } // foreach(config)

        // Show table in console with changes
        $realms_table_headers = ['Provider', 'ID', 'Realm Name', 'Realm Slug', 'State'];
        $this->table($realms_table_headers, $realms_table_output);

        $organization_units_table_headers = ['Provider', 'ID', 'Realm', 'Organization Unit', 'State'];
        $this->table($organization_units_table_headers, $organization_units_table_output);

        $cloud_accounts_table_headers = ['Provider', 'ID', 'Realm', 'Organization Unit', 'Account', 'State'];
        $this->table($cloud_accounts_table_headers, $cloud_accounts_table_output);

    } // handle()

}
