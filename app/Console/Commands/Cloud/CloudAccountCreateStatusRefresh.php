<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use App\Services\V1\Vendor\Aws\OrganizationAccountService;
use Illuminate\Console\Command;
use Illuminate\Support\Arr;

class CloudAccountCreateStatusRefresh extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account:create-status-refresh';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Refresh the status of all cloud account creation in pending state using cloud provider API';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get all cloud account users for AWS accounts that have not been provisioned
        // This is intentionally set to create a loop on a new run of the command to allow
        // for a delay on AWS account creation before IAM services are available.
        $aws_cloud_account_users = Models\Cloud\CloudAccountUser::with([
            'cloudAccountUserRoles'
            ])->whereHas('cloudAccount', function($query) {
                $query->where('state', 'active');
            })->where('state', 'provisioning-pending')
            ->get();

        // Loop through AWS account users
        foreach($aws_cloud_account_users as $cloud_account_user) {

            $cloudAccountUserService = new Services\V1\Cloud\CloudAccountUserService();
            $cloudAccountUserRoleService = new Services\V1\Cloud\CloudAccountUserRoleService();

            // If API meta data has already been populated, then assume the user has already been provisioned.
            if($cloud_account_user->api_meta_data != null) {
                $cloud_account_user->state = 'active';
                $cloud_account_user->save();
                $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$cloud_account_user->id.'] Cloud Account User has meta data. State changed to active.');

                // Loop through cloud account user roles
                foreach($cloud_account_user->cloudAccountUserRoles as $cloud_account_user_role) {
                    $cloud_account_user_role->state = 'active';
                    $cloud_account_user_role->save();
                }
            }

            // If user was created more than 15 minutes ago, assume that provisioning has failed
            elseif($cloud_account_user->created_at < now()->subMinutes(15)) {
                $cloud_account_user->state = 'provisioning-failed';
                $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$cloud_account_user->id.'] Cloud Account User has been provisioning for more than 15 minutes. State changed to provisioning-failed.');

                // Loop through cloud account user roles
                foreach($cloud_account_user->cloudAccountUserRoles as $cloud_account_user_role) {
                    $cloud_account_user_role->state = 'provisioning-failed';
                    $cloud_account_user_role->save();
                }
            }

            // Proceed with provisioning
            else {

                // Provision cloud account user
                $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$cloud_account_user->id.'] Provisioning Cloud Account User');
                $cloudAccountUserService->provision($cloud_account_user->id);

                // Loop through cloud account user roles
                foreach($cloud_account_user->cloudAccountUserRoles as $cloud_account_user_role) {

                    // Provision cloud account user
                    $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$cloud_account_user_role->id.'] Provisioning Cloud Account User Role');
                    $cloudAccountUserRoleService->provision($cloud_account_user_role->id);

                }

            }

        }

        // Get all cloud accounts for AWS cloud providers that have `IN_PROGRESS` state
        $aws_cloud_accounts = Models\Cloud\CloudAccount::with([
            'cloudAccountUsers',
            ])->whereHas('cloudProvider', function($query) {
                $query->where('type', 'aws');
            })->where('api_meta_data', '!=', null)
            ->where('state', 'provisioning')
            ->get();

        // Loop through AWS accounts
        foreach($aws_cloud_accounts as $aws_cloud_account) {

            // Initialize AWS Account service
            $accountService = new Services\V1\Vendor\Aws\OrganizationAccountService($aws_cloud_account->cloud_provider_id);

            // Use service method to get updated status
            $api_meta_data = $accountService->getCreateStatus($aws_cloud_account->api_meta_data['Id']);

            if($api_meta_data['State'] == 'IN_PROGRESS') {
                $aws_cloud_account->state = 'provisioning';
                $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$aws_cloud_account->id.'] Cloud Account is still provisioning');
            } elseif($api_meta_data['State'] == 'SUCCEEDED') {
                $aws_cloud_account->state = 'active';
                $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$aws_cloud_account->id.'] Cloud Account is now active');
            } elseif($api_meta_data['State'] == 'FAILED') {
                $aws_cloud_account->state = 'provisioning-error';
                $this->comment('['.now()->format('Y-m-d H:i:s').'] ['.$aws_cloud_account->id.'] Cloud Account has a provisioning error');
            }

            // Update cloud account database entry
            $aws_cloud_account->api_meta_data = $api_meta_data;
            $aws_cloud_account->save();

        }

        // TODO GCP Accounts

    }

}
