<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudAccountUserList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-account-user:list
                            {--T|auth_tenant_slug= : The slug of the tenant that group user relationships belong to}
                            {--C|cloud_account_short_id= : The short ID of the Cloud Account to filter by}
                            {--U|auth_user_short_id= : The short ID of the Authentication User to filter by}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Account User Relationships';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get cloud account short id
        $cloud_account_short_id = $this->option('cloud_account_short_id');

        // Get auth user short id
        $auth_user_short_id = $this->option('auth_user_short_id');

        // Get list of records from database
        $records = Models\Cloud\CloudAccountUser::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($cloud_account_short_id) {
                if($cloud_account_short_id) {
                    $query->whereHas('cloudAccount', function (Builder $query) use ($cloud_account_short_id) {
                        $query->where('short_id', $cloud_account_short_id);
                    });
                }
            })->where(function($query) use ($auth_user_short_id) {
                if($auth_user_short_id) {
                    $query->whereHas('authUser', function (Builder $query) use ($auth_user_short_id) {
                        $query->where('short_id', $auth_user_short_id);
                    });
                }
            })
            ->orderBy('created_at')
            ->get();

        // Loop through groups in Eloquent model and add values to array
        foreach($records as $record) {
            $table_rows[] = [
                'short_id' => $record->short_id,
                'auth_tenant' => '['.$record->authTenant->short_id.'] '.$record->authTenant->slug,
                'cloud_provider' => '['.$record->cloudProvider->short_id.'] '.$record->cloudProvider->slug,
                'cloud_account' => '['.$record->cloudAccount->short_id.'] '.$record->cloudAccount->slug,
                'auth_user' => '['.$record->authUser->short_id.'] '.$record->authUser->full_name,
                'username' => $record->username,
                'created_at' => $record->created_at->toIso8601String(),
                'state' => $record->state,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Account User Relationships - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $table_headers = ['Short ID', 'Tenant', 'Provider', 'Cloud Account', 'User', 'IAM Username', 'Created at', 'State'];
            $this->table($table_headers, $table_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->line('You can attach cloud accounts to users using `cloud-account-user:create`.');
            $this->comment('');
        }

    }
}
