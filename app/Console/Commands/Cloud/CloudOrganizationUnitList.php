<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;
use Illuminate\Database\Eloquent\Builder;

class CloudOrganizationUnitList extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-organization-unit:list
                            {--T|auth_tenant_slug= : The slug of the tenant that the organization units belong to}
                            {--P|cloud_provider_slug= : The slug of the cloud provider that organization units belongs to}
                            {--R|cloud_realm_slug= : The slug of the cloud realm the organization units belong to}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'List of Cloud Organization Units';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get auth tenant slug
        $auth_tenant_slug = $this->option('auth_tenant_slug');

        // Get cloud provider slug
        $cloud_provider_slug = $this->option('cloud_provider_slug');

        // Get cloud realm slug
        $cloud_realm_slug = $this->option('cloud_realm_slug');

        // Get list of records from database
        $records = Models\Cloud\CloudOrganizationUnit::query()
            ->where(function($query) use ($auth_tenant_slug) {
                if($auth_tenant_slug) {
                    $query->whereHas('authTenant', function (Builder $query) use ($auth_tenant_slug) {
                        $query->where('slug', $auth_tenant_slug);
                    });
                }
            })->where(function($query) use ($cloud_provider_slug) {
                if($cloud_provider_slug) {
                    $query->whereHas('cloudProvider', function (Builder $query) use ($cloud_provider_slug) {
                        $query->where('slug', $cloud_provider_slug);
                    });
                }
            })->where(function($query) use ($cloud_realm_slug) {
                if($cloud_realm_slug) {
                    $query->whereHas('cloudRealm', function (Builder $query) use ($cloud_realm_slug) {
                        $query->where('slug', $cloud_realm_slug);
                    });
                }
            })->orderBy('slug')
            ->get();

        // Loop through roles in Eloquent model and add calculated values to
        // array. The dot notation for pivot relationships doesn't work with a
        // get([]) so this is the best way to handle this.
        foreach($records as $cloud_organization_unit_record) {
            $organization_unit_rows[] = [
                'short_id' => $cloud_organization_unit_record->short_id,
                'auth_tenant' => '['.$cloud_organization_unit_record->authTenant->short_id.'] '.$cloud_organization_unit_record->authTenant->slug,
                'cloud_provider' => '['.$cloud_organization_unit_record->cloudProvider->short_id.'] '.$cloud_organization_unit_record->cloudProvider->slug,
                'parent_cloud_organization_unit' => $cloud_organization_unit_record->cloud_organization_unit_id_parent ? '['.$cloud_organization_unit_record->parentCloudOrganizationUnit->short_id.'] '.$cloud_organization_unit_record->parentCloudOrganizationUnit->slug : '',
                'child_cloud_organization_units' => $cloud_organization_unit_record->childCloudOrganizationUnits()->count(),
                'accounts' => $cloud_organization_unit_record->cloudAccounts()->count(),
                'name' => $cloud_organization_unit_record->name,
                'slug' => $cloud_organization_unit_record->slug,
            ];
        }

        $this->comment('');
        $this->comment('Cloud Organization Units - List of Records');

        if(count($records) > 0) {

            // Show table in console output for verification
            $headers = ['Short ID', 'Tenant', 'Provider', 'Parent OU', 'Child OUs', 'Accounts', 'Name', 'Slug'];
            $this->table($headers, $organization_unit_rows);

            $this->comment('Total: '.count($records));
            $this->comment('');

        } else {
            $this->line('No records exist.');
            $this->comment('');
        }

    }
}
