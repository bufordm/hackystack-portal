<?php

namespace App\Console\Commands\Cloud;

use App\Models;
use App\Services;
use Illuminate\Console\Command;

class CloudRealmGet extends Command
{
    /**
     * The name and signature of the console command.
     *
     * @var string
     */
    protected $signature = 'cloud-realm:get
                            {short_id? : The short ID of the realm.}
                            {--R|without-child-relationships}
                            ';

    /**
     * The console command description.
     *
     * @var string
     */
    protected $description = 'Get a Cloud Realm by ID';

    /**
     * Create a new command instance.
     *
     * @return void
     */
    public function __construct()
    {
        parent::__construct();
    }

    /**
     * Execute the console command.
     *
     * @return int
     */
    public function handle()
    {

        // Get short ID
        $short_id = $this->argument('short_id');

        // If short ID or slug is not set, return an error message
        if($short_id == null) {
            $this->error('You did not specify the short id to lookup the record.');
            $this->line('You can get a list of cloud realms using `cloud-realm:list`');
            $this->line('You can lookup by short ID using `cloud-realm:get a1b2c3d4`');
            $this->line('');
            die();
        }

        // If short ID is specified, lookup by short_id
        elseif($short_id != null) {
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('short_id', $short_id)
                ->first();
        }

        // If record not found, return an error message
        if($cloud_realm == null) {
            $this->error('No record found.');
            $this->error('');
            die();
        }

        // Cloud Realm Values
        $this->comment('');
        $this->comment('Cloud Realm');
        $this->table(
            ['Column', 'Value'],
            [
                [
                    'id',
                    $cloud_realm->id
                ],
                [
                    'short_id',
                    $cloud_realm->short_id
                ],
                [
                    '[relationship] authTenant',
                    '['.$cloud_realm->authTenant->short_id.'] '.$cloud_realm->authTenant->slug
                ],
                [
                    '[relationship] cloudBillingAccount',
                    '['.$cloud_realm->cloudBillingAccount->short_id.'] '.$cloud_realm->cloudBillingAccount->slug
                ],
                [
                    '[relationship] cloudProvider',
                    '['.$cloud_realm->cloudProvider->short_id.'] '.$cloud_realm->cloudProvider->slug
                ],
                [
                    'name',
                    $cloud_realm->name
                ],
                [
                    'slug',
                    $cloud_realm->slug
                ],
                [
                    'created_at',
                    $cloud_realm->created_at->toIso8601String()
                ],
            ]
        );

        // Check if option passed to not show child relationships
        if($this->option('without-child-relationships') == false) {

            // Auth Tenant Values
            $this->comment('');
            $this->comment('Auth Tenant');
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'short_id',
                        $cloud_realm->authTenant->short_id
                    ],
                    [
                        'name',
                        $cloud_realm->authTenant->name
                    ],
                    [
                        'slug',
                        $cloud_realm->authTenant->slug
                    ],
                ]
            );

            // Cloud Provider Values
            $this->comment('');
            $this->comment('Cloud Provider');
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'short_id',
                        $cloud_realm->cloudProvider->short_id
                    ],
                    [
                        'name',
                        $cloud_realm->cloudProvider->name
                    ],
                    [
                        'slug',
                        $cloud_realm->cloudProvider->slug
                    ],
                ]
            );

            // Cloud Billing Account Values
            $this->comment('');
            $this->comment('Cloud Billing Account');
            $this->table(
                ['Column', 'Value'],
                [
                    [
                        'short_id',
                        $cloud_realm->cloudBillingAccount->short_id
                    ],
                    [
                        'name',
                        $cloud_realm->cloudBillingAccount->name
                    ],
                    [
                        'slug',
                        $cloud_realm->cloudBillingAccount->slug
                    ],
                ]
            );

            //
            // Cloud Realm - Child Relationship - Cloud Organization Units
            // --------------------------------------------------------------------
            // Loop through organization units in Eloquent model and add calculated
            // values to array. The dot notation for pivot relationships doesn't
            // work with a get([]) so this is the best way to handle this.
            //

            // Loop through organization units and add values to array
            $organization_unit_rows = [];
            foreach($cloud_realm->cloudOrganizationUnits as $unit) {
                $organization_unit_rows[] = [
                    'account_short_id' => $unit->short_id,
                    'parent_cloud_organization_unit' => $unit->cloud_organization_unit_id_parent ? '['.$unit->parentCloudOrganizationUnit->short_id.'] '.$unit->parentCloudOrganizationUnit->slug : '',
                    'name' => $unit->name,
                    'slug' => $unit->slug,
                ];
            }

            $this->comment('');
            $this->comment('Cloud Organization Units');

            // If rows exists, render a table or return a no results found message
            if(count($organization_unit_rows) > 0 && count($organization_unit_rows) < 100) {
                $this->table(
                    ['OU Short ID', 'Parent OU', 'Name', 'Slug'],
                    $organization_unit_rows
                );
            } elseif(count($organization_unit_rows) > 100) {
                $this->comment('There are '.count($organization_unit_rows).' cloud organization units attached to this realm.');
            } else {
                $this->error('No cloud organization units have been attached.');
            }

            //
            // Cloud Realm - Child Relationship - Cloud Account
            // --------------------------------------------------------------------
            // Loop through accounts in Eloquent model and add calculated values to
            // array. The dot notation for pivot relationships doesn't work with a
            // get([]) so this is the best way to handle this.
            //

            // Loop through accounts and add values to array
            $account_rows = [];
            foreach($cloud_realm->cloudAccounts as $account) {
                $account_rows[] = [
                    'account_short_id' => $account->short_id,
                    'name' => $account->name,
                    'slug' => $account->slug,
                ];
            }

            $this->comment('');
            $this->comment('Cloud Accounts');

            // If rows exists, render a table or return a no results found message
            if(count($account_rows) > 0 && count($account_rows) < 100) {
                $this->table(
                    ['Account Short ID', 'Name', 'Slug'],
                    $account_rows
                );
            } elseif(count($account_rows) > 100) {
                $this->comment('There are '.count($account_rows).' cloud accounts attached to this realm.');
            } else {
                $this->error('No cloud accounts have been attached.');
            }

        } // if($this->option('without-child-relationships') == false)

        $this->comment('');

    }
}
