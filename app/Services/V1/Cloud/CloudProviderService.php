<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudProviderService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudProvider::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id                  required|uuid|exists:auth_tenants,id
     *      api_credentials                 nullable|array
     *      api_meta_data                   nullable|array
     *      type                            required|in:aws,gcp
     *      name                            required|string|max:255
     *      slug                            required|string|max:255
     *      parent_org_unit                 nullable|string|max:55
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Text fields
        $record->type = in_array($request_data['type'], ['aws','gcp']) ? $request_data['type'] : 'invalid';
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');
        $record->parent_org_unit = Arr::get($request_data, 'parent_org_unit');

        // If api credentials is in request, encode as JSON and encrypt
        if(Arr::has($request_data, 'api_credentials')) {
            $record->api_credentials = encrypt(json_encode($request_data['api_credentials']));
        }

        // If api meta data is in request, encode as JSON
        if(Arr::has($request_data, 'api_meta_data') && count($request_data['api_meta_data'] > 0)) {
            $record->api_meta_data = json_encode($request_data['api_meta_data']);
        }

        // If cloud provider type is AWS
        if($request_data['type'] == 'aws') {

            // Define an array of roles that are created for each Cloud Account
            // TODO Refactor with request provided list of roles
            $record->default_roles = [
                'AdministratorAccess'
            ];

        }

        // If cloud provider type is GCP
        elseif($request_data['type'] == 'gcp') {

            // Define an array of roles that are created for each Cloud Account
            // TODO Refactor with request provided list of roles
            $record->default_roles = [
                'Owner'
            ];
        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Create Billing Account
        $cloudBillingAccountService = new Services\V1\Cloud\CloudBillingAccountService();

        $cloud_billing_account = $cloudBillingAccountService->store([
            'auth_tenant_id' => $auth_tenant->id,
            'cloud_provider_id' => $record->id,
            'name' => 'Default Billing Account',
            'slug' => 'default'
        ]);

        $this->provisionGitGroup($record->id);
        $this->syncCloudProviderOrganization($record->id);

        $record = $record->fresh();

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      api_credentials                 nullable|array
     *      api_meta_data                   nullable|array
     *      name                            required|string|max:255
     *      slug                            required|string|max:255
     *      parent_org_unit                 nullable|string|max:55
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);
        $record->parent_org_unit = Arr::get($request_data, 'parent_org_unit', $record->parent_org_unit);

        // If api credentials is in request, encode as JSON and encrypt
        // FUTURE Use collect() methods to perform key matching and only update changed values to avoid data loss
        if(Arr::has($request_data, 'api_credentials')) {
            $record->api_credentials = encrypt(json_encode($request_data['api_credentials']));
        }

        $record->save();

        // Sync cloud provider with API
        $this->syncCloudProviderOrganization($record->id);

        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    /**
     *   Create a subgroup on the Git provider
     *
     *   @param  uuid $id       Cloud Realm ID
     *
     *   @return bool
     */
    public function provisionGitGroup($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->authTenant->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->authTenant->id);

            // If Git meta data is null, perform a create action
            if($record->git_meta_data == null && $record->authTenant->git_meta_data != null) {

                // Perform API call to create group
                $provider_group = $gitlabGroupService->create([
                    'name' => $record->name.' - Provider '.$record->short_id,
                    'path' => $record->slug.'-'.$record->short_id,
                    'parent_id' => $record->authTenant->git_meta_data['id']
                ]);

            }

            // If Git meta data already exists, assume already provisioned and
            // perform an update instead of a create action
            elseif($record->git_meta_data != null) {

                // Perform API call to create group
                $provider_group = $gitlabGroupService->update($record->git_meta_data['id'], [
                    'name' => $record->name.' - Provider '.$record->short_id,
                    'path' => $record->slug.'-'.$record->short_id,
                ]);

            }

            // Update the database with the API result
            $record->git_meta_data = $provider_group;
            $record->save();

            return true;

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

    /**
     *   Destroy the subgroup on the Git provider
     *
     *   @param  uuid $id       Cloud Realm ID
     *
     *   @return bool
     */
    public function destroyGitGroup($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->authTenant->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->authTenant->id);

            // If Git meta data is not null, perform deletion
            if($record->git_meta_data != null) {

                // Perform API call to create group
                $realm_group = $gitlabGroupService->destroy($record->gitlab_meta_data['id']);

                // Update the database with the API result
                $record->git_meta_data = null;
                $record->save();

                return true;

            }

            // If Git meta data is not set, assume group does not exist
            elseif($record->git_meta_data == null) {
                return false;
            }

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

    public function syncCloudProviderOrganization($id)
    {
        // Get record by ID
        $record = $this->model()->query()
            ->where('id', $id)
            ->firstOrFail();

        if($record->api_credentials != null) {

            // GCP Organization Meta Data
            if($record->type == 'gcp') {

                //

            }

            // AWS Organization Meta Data
            if($record->type == 'aws') {

                // Initialize AWS Organizations Organization Unit Service
                $organizationService = new Services\V1\Vendor\Aws\OrganizationService($record->id);

                // Use service method to get meta data from API
                $api_meta_data = $organizationService->get();

                // Update the database with the API result
                $record->api_meta_data = $api_meta_data;
                $record->flag_provisioned = 1;
                $record->save();

            }

        }

    }

}
