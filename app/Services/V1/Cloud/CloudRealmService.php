<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudRealmService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudRealm::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id                  required|uuid|exists:auth_tenants,id
     *      cloud_billing_account_id        required|uuid|exists:cloud_billing_accounts,id
     *      cloud_provider_id               required|uuid|exists:cloud_providers,id
     *      api_meta_data                   nullable|array
     *      name                            required|string|max:255
     *      slug                            required|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get Cloud Billing Account relationship
        if(!empty($request_data['cloud_billing_account_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                ->where('id', $request_data['cloud_billing_account_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_billing_account_id = $cloud_billing_account->id;

        } else {

            // Use default value from cloud realm for billing account
            $record->cloud_billing_account_id = $cloud_realm->cloud_billing_account_id;

        }

        // Get Cloud Provider relationship
        if(!empty($request_data['cloud_provider_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('id', $request_data['cloud_provider_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_provider_id = $cloud_provider->id;
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');

        // If api meta data is in request, encode as JSON
        if(Arr::has($request_data, 'api_meta_data')) {
            $record->api_meta_data = $request_data['api_meta_data'];
        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Root Level Organization Unit
        // --------------------------------------------------------------------
        // When a new realm is created, this is a top-level folder in a cloud
        // provider's organization unit schema. For data consistency, all
        // organization units need a parent organization unit. We create a top
        // level organization unit for each realm when it is created that has
        // the meta data about the top-level folder the realm represents.
        //

        // Instantiate the organization unit service
        $cloudOrganizationUnitService = new Services\V1\Cloud\CloudOrganizationUnitService();

        // Use the service method to create a new organization unit
        $root_organization_unit = $cloudOrganizationUnitService->store([
            'auth_tenant_id' => $record->auth_tenant_id,
            'cloud_provider_id' => $record->cloud_provider_id,
            'cloud_realm_id' => $record->id,
            'name' => $record->name,
            'slug' => $record->slug,
        ]);

        // Update the cloud realm with the root organization unit
        $record->cloud_organization_unit_id_root = $root_organization_unit->id;
        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      cloud_billing_account_id        nullable|uuid|exists:cloud_billing_accounts,id
     *      api_meta_data                   nullable|array
     *      name                            required|string|max:255
     *      slug                            required|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get Cloud Billing Account relationship
        if(!empty($request_data['cloud_billing_account_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_billing_account_id != $request_data['cloud_billing_account_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_billing_account = Models\Cloud\CloudBillingAccount::query()
                    ->where('id', $request_data['cloud_billing_account_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_billing_account_id = $cloud_billing_account->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different billing account. In this
                // iteration, this only affects the database meta data and doesn't
                // perform any infrastructure changes.

            }
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);

        // If api meta data is in request, encode as JSON
        // TODO Use collect() methods to perform key matching and only update changed values to avoid data loss
        if(Arr::has($request_data, 'api_meta_data')) {
            $record->api_meta_data = $request_data['api_meta_data'];
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

}
