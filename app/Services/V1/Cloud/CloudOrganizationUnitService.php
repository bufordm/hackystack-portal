<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudOrganizationUnitService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudOrganizationUnit::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_tenant_id                      required|uuid|exists:auth_tenants,id
     *      cloud_billing_account_id            required|uuid|exists:cloud_billing_accounts,id
     *      cloud_organization_unit_id_parent   nullable|uuid|exists:cloud_organization_units,id
     *      cloud_provider_id                   required|uuid|exists:cloud_providers,id
     *      cloud_realm_id                      required|uuid|exists:cloud_realms,id
     *      api_meta_data                       nullable|array
     *      name                                required|string|max:255
     *      slug                                required|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get Cloud Provider relationship
        if(!empty($request_data['cloud_provider_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_provider = Models\Cloud\CloudProvider::query()
                ->where('id', $request_data['cloud_provider_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_provider_id = $cloud_provider->id;
        }

        // Get Cloud Realm relationship
        if(!empty($request_data['cloud_realm_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_realm = Models\Cloud\CloudRealm::query()
                ->where('id', $request_data['cloud_realm_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_realm_id = $cloud_realm->id;
        }

        // Get Cloud Organization Unit relationship
        if(!empty($request_data['cloud_organization_unit_id_parent'])) {

            // Get relationship by ID to validate that it exists
            $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                ->where('id', $request_data['cloud_organization_unit_id_parent'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_organization_unit_id_parent = $cloud_organization_unit->id;
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name');
        $record->slug = Arr::get($request_data, 'slug');

        $record->state = 'pending';
        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        $this->provisionGitGroup($record->id);
        $this->provisionCloudProviderOrganizationUnit($record->id);

        $record = $record->fresh();

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      cloud_organization_unit_id_parent   nullable|uuid|exists:cloud_organization_units,id
     *      cloud_realm_id                      nullable|uuid|exists:cloud_realms,id
     *      api_meta_data                       nullable|array
     *      name                                required|string|max:255
     *      slug                                required|string|max:255
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Get Cloud Realm relationship
        if(!empty($request_data['cloud_realm_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_realm_id != $request_data['cloud_realm_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_realm = Models\Cloud\CloudRealm::query()
                    ->where('id', $request_data['cloud_realm_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_realm_id = $cloud_realm->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different realm. In this iteration, this
                // only affects the database meta data and doesn't perform any
                // infrastructure changes.

            }
        }

        // Get Cloud Organization Unit relationship
        if(!empty($request_data['cloud_organization_unit_id_parent'])) {

            // If request data value is different than record existing value
            if($record->cloud_organization_unit_id_parent != $request_data['cloud_organization_unit_id_parent']) {

                // Get relationship by ID to validate that it exists
                $cloud_organization_unit = Models\Cloud\CloudOrganizationUnit::query()
                    ->where('id', $request_data['cloud_organization_unit_id_parent'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_organization_unit_id_parent = $cloud_organization_unit->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different organization unit. In this
                // iteration, this only affects the database meta data and doesn't
                // perform any infrastructure changes.

            }
        }

        // Text fields
        $record->name = Arr::get($request_data, 'name', $record->name);
        $record->slug = Arr::get($request_data, 'slug', $record->slug);

        // If api meta data is in request, encode as JSON
        // TODO Use collect() methods to perform key matching and only update changed values to avoid data loss
        if(Arr::has($request_data, 'api_meta_data') && count($request_data['api_meta_data'] > 0)) {
            $record->api_meta_data = json_encode($request_data['api_meta_data']);
        }

        $record->state = 'pending';
        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    /**
     *   Create a subgroup on the Git provider
     *
     *   @param  uuid $id       Cloud Realm ID
     *
     *   @return bool
     */
    public function provisionGitGroup($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->authTenant->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->authTenant->id);

            // If Git meta data is null, perform a create action
            if($record->git_meta_data == null) {

                // Check if organiation unit has a parent cloud organization unit relationship
                if($record->parentCloudOrganizationUnit) {

                    // Check if parent organization unit has Git meta data (parent Git group exists)
                    if($record->parentCloudOrganizationUnit->git_meta_data != null) {
                        // Perform API call to create group
                        $organization_unit_group = $gitlabGroupService->create([
                            'name' => $record->name.' - Org Unit '.$record->short_id,
                            'path' => $record->slug.'-'.$record->short_id,
                            'parent_id' => $record->parentCloudOrganizationUnit->git_meta_data['id']
                        ]);
                    } else {
                        $organization_unit_group = null;
                    }

                } else {

                    // Check if cloud realm has Git meta data (parent Git group exists)
                    if($record->cloudProvider->git_meta_data != null) {
                        // Perform API call to create group
                        $organization_unit_group = $gitlabGroupService->create([
                            'name' => $record->name.' - Realm Org Unit '.$record->short_id,
                            'path' => $record->slug.'-'.$record->short_id,
                            'parent_id' => $record->cloudProvider->git_meta_data['id']
                        ]);
                    } else {
                        $organization_unit_group = null;
                    }

                }

            }

            // If Git meta data already exists, assume already provisioned and
            // perform an update instead of a create action
            elseif($record->git_meta_data != null) {

                // Perform API call to create group
                $organization_unit_group = $gitlabGroupService->update($record->git_meta_data['id'], [
                    'name' => $record->name.' - Org Unit '.$record->short_id,
                    'path' => $record->slug.'-'.$record->short_id,
                ]);

            }

            // Update the database with the API result
            $record->git_meta_data = $organization_unit_group;
            $record->save();

            return true;

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

    /**
     *   Destroy the subgroup on the Git provider
     *
     *   @param  uuid $id       Cloud Realm ID
     *
     *   @return bool
     */
    public function destroyGitGroup($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // If AuthTenant git_provider_type is GitLab
        if($record->authTenant->git_provider_type == 'gitlab') {

            // Initialize GitLab group service
            $gitlabGroupService = new Services\V1\Vendor\Gitlab\GroupService($record->authTenant->id);

            // If Git meta data is not null, perform deletion
            if($record->git_meta_data != null) {

                // Perform API call to create group
                $realm_group = $gitlabGroupService->destroy($record->gitlab_meta_data['id']);

                // Update the database with the API result
                $record->git_meta_data = null;
                $record->save();

                return true;

            }

            // If Git meta data is not set, assume group does not exist
            elseif($record->git_meta_data == null) {
                return false;
            }

        }

        // If AuthTenant git_provider_type is null or invalid value
        else {
            return false;
        }

    }

    public function provisionCloudProviderOrganizationUnit($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Check organization unit has not been provisioned and provider has been
        if($record->flag_provisioned == false && $record->cloudProvider->flag_provisioned == true) {

            // GCP Folder Creation
            if($record->cloudProvider->type == 'gcp') {

                // Initialize GCP Resource Manager Folder Service
                $folderService = new Services\V1\Vendor\Gcp\CloudResourceManagerFolderService($record->cloud_provider_id);

                // Check if parent organization unit is configured
                if($record->parentCloudOrganizationUnit && $record->parentCloudOrganizationUnit->api_meta_data) {
                    $parent_folder_name = $record->parentCloudOrganizationUnit->api_meta_data['name'];
                }

                // Check if the cloud provider has a parent folder or organization
                // unit specified
                elseif($record->cloudProvider->parent_org_unit) {
                    $parent_folder_name = 'folders/'.$record->cloudProvider->parent_org_unit;
                }

                // Check if the cloud provider has been synced with the API and
                // the api_meta_data field is populated
                elseif($record->cloudProvider->api_meta_data) {
                    $parent_folder_name = $record->cloudProvider->api_meta_data['name'];
                }

                else {
                    $parent_folder_name = null;
                }

                if($parent_folder_name != null) {

                    // Use service method to provision with API
                    $api_meta_data = $folderService->provision([
                        'display_name' => $record->slug.'-'.$record->short_id,
                        'parent_folder_name' => $record->cloudProvider->api_meta_data['name']
                    ]);

                    // Update the database record
                    $record->api_meta_data = $api_meta_data;
                    $record->flag_provisioned = 1;
                    $record->state = 'active';
                    $record->save();

                    return true;

                } else {
                    return false;
                }

            }

            // AWS Organization Unit Creation
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Organizations Organization Unit Service
                $organizationalUnitService = new Services\V1\Vendor\Aws\OrganizationalUnitService($record->cloud_provider_id);

                // If organizational unit has a parent and populated meta data
                if($record->parentCloudOrganizationUnit && $record->parentCloudOrganizationUnit->api_meta_data) {
                    $parent_id = $record->parentCloudOrganizationUnit->api_meta_data['Id'];
                }

                // Check if the cloud provider has a parent folder or organization
                // unit specified
                elseif($record->cloudProvider->parent_org_unit) {
                    $parent_id = $record->cloudProvider->parent_org_unit;
                }

                // Check if the cloud provider has been synced with the API and
                // the api_meta_data field is populated
                elseif($record->cloudProvider->api_meta_data) {
                    $parent_id = $record->cloudProvider->api_meta_data['Root']['Id'];
                }

                else {
                    $parent_id = null;
                }

                if($parent_id != null) {

                    // Use service method to provision with API
                    $api_meta_data = $organizationalUnitService->create([
                        'name' => $record->slug.'-'.$record->short_id,
                        'parent_id' => $parent_id,
                        'tags' => [
                            //
                        ]
                    ]);

                    // Update the database record
                    $record->api_meta_data = $api_meta_data;
                    $record->flag_provisioned = 1;
                    $record->state = 'active';
                    $record->save();

                    return true;

                } else {
                    return false;
                }

            }

        } // if($record->flag_provisioned == false)

        else {
            $record->state = 'provisioning-pending';
            $record->save();
        }

        return false;

    }

    public function deprovisionCloudProviderOrganizationUnit($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Check that is provisioned
        if($record->flag_provisioned == 1) {

            // GCP Folder Deletion
            if($record->cloudProvider->type == 'gcp') {

                // Initialize GCP Resource Manager Folder Service
                $folderService = new Services\V1\Vendor\Gcp\CloudResourceManagerFolderService($record->cloud_provider_id);

                // Use service method to provision with API
                $api_meta_data = $folderService->deprovision([
                    'folder_name' => $record->api_meta_data['name']
                ]);

            }

            // AWS Organization Unit Deletion
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Organizations Organization Unit Service
                $organizationalUnitService = new Services\V1\Vendor\Aws\OrganizationalUnitService($record->cloud_provider_id);

                // Use service method to deprovision with API
                $api_meta_data = $organizationalUnitService->delete($record->api_meta_data['Id']);

            }

            // Update the database record
            $record->api_meta_data = null;
            $record->flag_provisioned = 0;
            $record->state = 'deprovisioned';
            $record->save();

        } // if($record->flag_provisioned == 1)
    }

}
