<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudAccountUserRoleService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudAccountUserRole::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      cloud_account_role_id       required_without:cloud_account_role_name|uuid|exists:cloud_accounts_roles,id
     *      cloud_account_role_name     required_without:cloud_account_role_id|string
     *      cloud_account_user_id       required|uuid|exists:cloud_accounts_users,id
     *      expires_at                  nullable|datetime
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Cloud Account User relationship
        if(!empty($request_data['cloud_account_user_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_account_user = Models\Cloud\CloudAccountUser::query()
                ->where('id', $request_data['cloud_account_user_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $cloud_account_user->auth_tenant_id;
            $record->cloud_account_id = $cloud_account_user->cloud_account_id;
            $record->cloud_account_user_id = $cloud_account_user->id;
            $record->cloud_realm_id = $cloud_account_user->cloud_realm_id;
            $record->cloud_provider_id = $cloud_account_user->cloud_provider_id;
        }

        // Get Cloud Account Role relationship
        if(array_key_exists('cloud_account_role_id', $request_data) && !empty($request_data['cloud_account_role_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_account_role = Models\Cloud\CloudAccountRole::query()
                ->where('id', $request_data['cloud_account_role_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_account_role_id = $cloud_account_role->id;

        } elseif(array_key_exists('cloud_account_role_name', $request_data) && !empty($request_data['cloud_account_role_name'])) {

            // Get role by name to check if it exists
            $cloud_account_role = Models\Cloud\CloudAccountRole::query()
                ->where('api_name', $request_data['cloud_account_role_name'])
                ->where('cloud_account_id', $cloud_account_user->cloud_account_id)
                ->first();

            // If role does not exist, create the account role
            if($cloud_account_role == null) {

                // Initialize service for creating a new role
                $cloudAccountRoleService = new CloudAccountRoleService();

                // Use service to create new role with name specified in request
                $cloud_account_role = $cloudAccountRoleService->store([
                    'auth_tenant_id' => $cloud_account_user->auth_tenant_id,
                    'cloud_account_id' => $cloud_account_user->cloud_account_id,
                    'cloud_provider_id' => $cloud_account_user->cloud_provider_id,
                    'cloud_realm_id' => $cloud_account_user->cloud_realm_id,
                    'api_name' => $request_data['cloud_account_role_name']
                ]);

            }

            // Update value of record with ID of relationship
            $record->cloud_account_role_id = $cloud_account_role->id;

        }

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

            if($request_data['expires_at'] == null) {
                $record->expires_at = null;
            } elseif($expires_at > now()) {
                $record->expires_at = $expires_at;
            } elseif($expires_at <= now()) {
                abort(400, 'The `expires_at` value for the user role cannot be in the past.');
            }

        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        $this->provision($record->id);

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      expires_at              nullable|datetime
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Calculate expires at value
        if(!empty($request_data['expires_at'])) {

            // If request value is different than record existing value
            if($record->expires_at != $request_data['expires_at']) {

                $expires_at = \Carbon\Carbon::parse($request_data['expires_at']);

                if($request_data['expires_at'] == null) {
                    $record->expires_at = null;
                } elseif($expires_at > now()) {
                    $record->expires_at = $expires_at;
                } elseif($expires_at <= now()) {
                    abort(400, 'The `expires_at` value for the user role cannot be in the past.');
                }

            }
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    public function provision($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudAccountUser',
                'cloudAccountRole',
                'cloudProvider',
            ])->where('id', $id)
            ->firstOrFail();

        // Check that flag_provisioned is null and account exists
        if($record->flag_provisioned == false && $record->cloudAccountUser->flag_provisioned == true && $record->cloudAccountUser->state == 'active') {

            // GCP Project Creation
            if($record->cloudProvider->type == 'gcp') {

                return false;

            }

            // AWS Account Creation
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Iam User Role Service
                $iamUserRoleService = new Services\V1\Vendor\Aws\IamUserRoleService($record->cloud_provider_id, $record->cloudAccount->api_meta_data['Id']);

                // Use service method to provision with API
                $api_meta_data = $iamUserRoleService->create([
                    'username' => $record->cloudAccountUser->username,
                    'policy' => $record->cloudAccountRole->api_name,
                ]);

                // Update the database record
                $record->flag_provisioned = 1;
                $record->state = 'active';
                $record->save();

                return true;

            }

        } // if($record->flag_provisioned == false)

        else {

            $record->state = 'provisioning-pending';
            $record->save();

        }

        return false;

    }

    public function deprovision($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Check that is provisioned
        if($record->flag_provisioned == 1) {

            // GCP Folder Deletion
            if($record->cloudProvider->type == 'gcp') {

                //

            }

            // AWS Organization Unit Deletion
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Iam User Role Service
                $iamUserRoleService = new Services\V1\Vendor\Aws\IamUserRoleService($record->cloud_provider_id, $record->cloudAccount->api_meta_data['Id']);

                // Use service method to provision with API
                $api_meta_data = $iamUserRoleService->delete([
                    'username' => $record->cloudAccountUser->username,
                    'policy' => $record->cloudAccountRole->api_name,
                ]);

                // Update the database record
                $record->flag_provisioned = 0;
                $record->save();

                return true;

            }

        } // if($record->flag_provisioned == 1)
    }

}
