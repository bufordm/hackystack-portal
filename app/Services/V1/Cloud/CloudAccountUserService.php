<?php

namespace App\Services\V1\Cloud;

use App\Services\BaseService;
use App\Models;
use App\Services;
use Hackzilla\PasswordGenerator\Generator\RequirementPasswordGenerator;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudAccountUserService extends BaseService
{

    public function __construct()
    {
        $this->model = Models\Cloud\CloudAccountUser::class;
    }

    /**
     *   Store a new record
     *
     *   @param  array  $request_data
     *      auth_user_id            required|uuid|exists:auth_users,id
     *      auth_tenant_id          required|uuid|exists:auth_tenants,id
     *      cloud_account_id        required|uuid|exists:cloud_accounts,id
     *      cloud_account_group_id  nullable|uuid|exists:cloud_accounts_groups,id
     *
     *   @return object Eloquent Model
     */
    public function store($request_data = [])
    {
        //
        // Create the new record
        // --------------------------------------------------------------------
        //

        $record = new $this->model();

        // Get Auth Tenant relationship
        if(!empty($request_data['auth_tenant_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_tenant = Models\Auth\AuthTenant::query()
                ->where('id', $request_data['auth_tenant_id'])
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_tenant_id = $auth_tenant->id;
        }

        // Get Auth User relationship
        if(!empty($request_data['auth_user_id'])) {

            // Get relationship by ID to validate that it exists
            $auth_user = Models\Auth\AuthUser::query()
                ->where('id', $request_data['auth_user_id'])
                ->where('auth_tenant_id', $auth_tenant->id)
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->auth_user_id = $auth_user->id;
        }

        // Get Cloud Account relationship
        if(!empty($request_data['cloud_account_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_account = Models\Cloud\CloudAccount::query()
                ->where('id', $request_data['cloud_account_id'])
                ->where('auth_tenant_id', $auth_tenant->id)
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_account_id = $cloud_account->id;
            $record->cloud_provider_id = $cloud_account->cloud_provider_id;
            $record->cloud_realm_id = $cloud_account->cloud_realm_id;
        }

        // Get Cloud Account relationship
        if(!empty($request_data['cloud_account_group_id'])) {

            // Get relationship by ID to validate that it exists
            $cloud_account_group = Models\Cloud\CloudAccountGroup::query()
                ->where('id', $request_data['cloud_account_group_id'])
                ->where('cloud_account_id', $cloud_account->id)
                ->firstOrFail();

            // Update value of record with ID of relationship
            $record->cloud_account_group_id = $cloud_account_group->id;
        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // GCP IAM User
        if($cloud_account->cloudProvider->type == 'gcp') {
            $record->username = $auth_user->email;
        }

        // AWS IAM User
        if($cloud_account->cloudProvider->type == 'aws') {

            // Generate password for IAM user account
            // ----------------------------------------------------------------
            // Each password is randomly generated using complexity requirements
            // that can be customized in config/hackystack.php.
            // https://github.com/hackzilla/password-generator
            $password_generator = new RequirementPasswordGenerator();
            $password_generator
              ->setLength(config('hackystack.cloud.accounts.aws.iam.password_requirements.length'))
              ->setOptionValue(RequirementPasswordGenerator::OPTION_UPPER_CASE, config('hackystack.cloud.accounts.aws.iam.password_requirements.upper_case.enabled'))
              ->setOptionValue(RequirementPasswordGenerator::OPTION_LOWER_CASE, config('hackystack.cloud.accounts.aws.iam.password_requirements.lower_case.enabled'))
              ->setOptionValue(RequirementPasswordGenerator::OPTION_NUMBERS, config('hackystack.cloud.accounts.aws.iam.password_requirements.numbers.enabled'))
              ->setOptionValue(RequirementPasswordGenerator::OPTION_SYMBOLS, config('hackystack.cloud.accounts.aws.iam.password_requirements.symbols.enabled'))
              ->setMinimumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, config('hackystack.cloud.accounts.aws.iam.password_requirements.upper_case.min_count'))
              ->setMinimumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, config('hackystack.cloud.accounts.aws.iam.password_requirements.lower_case.min_count'))
              ->setMinimumCount(RequirementPasswordGenerator::OPTION_NUMBERS, config('hackystack.cloud.accounts.aws.iam.password_requirements.numbers.min_count'))
              ->setMinimumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, config('hackystack.cloud.accounts.aws.iam.password_requirements.symbols.min_count'))
              ->setMaximumCount(RequirementPasswordGenerator::OPTION_UPPER_CASE, config('hackystack.cloud.accounts.aws.iam.password_requirements.upper_case.max_count'))
              ->setMaximumCount(RequirementPasswordGenerator::OPTION_LOWER_CASE, config('hackystack.cloud.accounts.aws.iam.password_requirements.lower_case.max_count'))
              ->setMaximumCount(RequirementPasswordGenerator::OPTION_NUMBERS, config('hackystack.cloud.accounts.aws.iam.password_requirements.numbers.max_count'))
              ->setMaximumCount(RequirementPasswordGenerator::OPTION_SYMBOLS, config('hackystack.cloud.accounts.aws.iam.password_requirements.symbols.max_count'))
            ;
            $generated_password = $password_generator->generatePassword();

            // Update value of record with username and password
            $record->username = $auth_user->user_handle.'-'.$record->short_id;
            $record->password = encrypt($generated_password);

        }

        $record->save();

        // Get a fresh copy of the record after to ensure that any additional
        // setAttribute methods have been model are accessible in the object.
        $record = $record->fresh();

        // Use method in this class to provision cloud account on cloud provider
        $this->provision($record->id);

        // Placeholder for additional business logic

        return $record;
    }

    /**
     *   Update an existing record
     *
     *   If a value is not set in the request, the existing value will be used.
     *
     *   @param  uuid   $id
     *   @param  array  $request_data
     *      auth_user_id                nullable|uuid|exists:auth_users,id
     *      cloud_account_id            nullable|uuid|exists:cloud_accounts,id
     *
     *   @return object Eloquent Model
     */
    public function update($id, $request_data = [])
    {
        //
        // Update the existing record
        // --------------------------------------------------------------------
        //

        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Get Auth User relationship
        if(!empty($request_data['auth_user_id'])) {

            // If request data value is different than record existing value
            if($record->auth_user_id != $request_data['auth_user_id']) {

                // Get relationship by ID to validate that it exists
                $auth_user = Models\Auth\AuthUser::query()
                    ->where('id', $request_data['auth_user_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->auth_user_id = $auth_user->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different realm. In this iteration, this
                // only affects the database meta data and doesn't perform any
                // infrastructure changes.

            }
        }

        // Get Cloud Account relationship
        if(!empty($request_data['cloud_account_id'])) {

            // If request data value is different than record existing value
            if($record->cloud_account_id != $request_data['cloud_account_id']) {

                // Get relationship by ID to validate that it exists
                $cloud_account = Models\Cloud\CloudAccount::query()
                    ->where('id', $request_data['cloud_account_id'])
                    ->firstOrFail();

                // Update value of record with ID of relationship
                $record->cloud_account_id = $cloud_account->id;

                // TODO Refactor this into a separate method and take action to
                // move the account to a different billing account. In this
                // iteration, this only affects the database meta data and doesn't
                // perform any infrastructure changes.

            }
        }

        $record->save();

        //
        // Additional Business Logic
        // --------------------------------------------------------------------
        //

        // Placeholder for additional business logic

        return $record;

    }

    /**
     *   Soft delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function delete($id)
    {
        // Get record by ID
        $record = $this->model()->where('id', $id)->firstOrFail();

        // Soft delete child relationships
        // $record->childRelationship()->delete();

        // Use method in this class to deprovision cloud account on cloud provider
        $this->deprovision($record->id);

        // Soft delete the record
        $record->delete();

        return $record;
    }

    /**
     *   Restore a soft deleted record
     *
     *   @param  uuid   $id
     *
     *   @return object Eloquent Model
     */
    public function restore($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Create variable for deleted at (before restoring when it is cleared)
        // to calculate timestamp that child relationships should be restored.
        // This ensures that child relationships deleted before the record was
        // soft deleted are not accidentally restored as well.
        $deleted_at = $record->deleted_at;

        // Restore the record
        $record->restore();

        // Restore child relationships
        // $record->childRelationship()
        //    ->withTrashed()
        //    ->where('deleted_at', '>=', $deleted_at)
        //    ->restore();

        return $record;
    }

    /**
     *   Permanently delete an existing record
     *
     *   @param  uuid   $id
     *
     *   @return null
     */
    public function destroy($id)
    {
        // Get record by ID
        $record = $this->model()->withTrashed()->where('id', $id)->firstOrFail();

        // Permanently delete child relationships
        // $record->childRelationship()->forceDelete();

        // Permanently delete the record
        $record->forceDelete();

        return null;
    }

    public function provision($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'authUser',
                'cloudAccount',
                'cloudProvider',
            ])->where('id', $id)
            ->firstOrFail();

        // Check that flag_provisioned is null and account exists
        if($record->flag_provisioned == false && $record->cloudAccount->flag_provisioned == true && $record->cloudAccount->state == 'active') {

            // GCP Project Creation
            if($record->cloudProvider->type == 'gcp') {

                return false;

            }

            // AWS Account Creation
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Organizations Account Service
                $iamUserService = new Services\V1\Vendor\Aws\IamUserService($record->cloud_provider_id, $record->cloudAccount->api_meta_data['Id']);

                // Use service method to provision user account with API
                $api_meta_data = $iamUserService->create([
                    'username' => $record->username,
                    // 'tags' => [
                    //    [
                    //        'Key' => 'x',
                    //        'Value' => 'x',
                    //    ],
                    //],
                ]);

                // Use service method to provision a console login profile (password) with API
                $login_profile = $iamUserService->createLoginProfile([
                    'username' => $record->username,
                    'password' => decrypt($record->password),
                    'must_change_password' => config('hackystack.cloud.accounts.aws.iam.password_requirements.must_change_password'),
                    // 'tags' => [
                    //    [
                    //        'Key' => 'x',
                    //        'Value' => 'x',
                    //    ],
                    //],
                ]);

                // Update the database record
                $record->api_meta_data = $api_meta_data;
                $record->flag_provisioned = 1;
                $record->state = 'active';
                $record->save();

                return true;

            }

        } // if($record->flag_provisioned == false)

        else {

            $record->state = 'provisioning-pending';
            $record->save();

        }

        return false;

    }

    public function deprovision($id)
    {
        // Get record by ID
        $record = $this->model()->with([
                'cloudProvider'
            ])->where('id', $id)
            ->firstOrFail();

        // Check that is provisioned
        if($record->flag_provisioned == 1) {

            // GCP IAM User Deletion
            if($record->cloudProvider->type == 'gcp') {

                //

            }

            // AWS IAM User Deletion
            if($record->cloudProvider->type == 'aws') {

                // Initialize AWS Organizations Account Service
                $iamUserService = new Services\V1\Vendor\Aws\IamUserService($record->cloud_provider_id, $record->cloudAccount->api_meta_data['Id']);

                // Use service method to provision with API
                $api_meta_data = $iamUserService->deleteUser([
                    'username' => $record->api_meta_data['Username'],
                ]);

            }

            // Update the database record
            $record->api_meta_data = null;
            $record->flag_provisioned = 0;
            $record->state = 'deleted';
            $record->save();

            return true;

        } // if($record->flag_provisioned == 1)
    }

}
