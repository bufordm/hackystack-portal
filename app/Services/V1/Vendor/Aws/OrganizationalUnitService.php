<?php

namespace App\Services\V1\Vendor\Aws;

use App\Services\V1\Vendor\Aws\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class OrganizationalUnitService extends BaseService
{

    protected $aws_api_client;

    public function __construct($cloud_provider_id)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);

        // Decrypt the provider credentials to be able to use the array keys
        $credentials = json_decode(decrypt($this->cloud_provider->api_credentials), true);

        // Initialize the API client
        $this->aws_api_client = new \Aws\Organizations\OrganizationsClient([
            'version' => 'latest',
            'region'  => 'us-east-1',
            'credentials' => [
                'key'    => $credentials['aws_access_key_id'],
                'secret' => $credentials['aws_access_key_secret'],
            ],
        ]);

    }

    /**
     *   Get list of all organization units
     *
     *   @param  array  $request_data
     *      parent_id|string    Root (r-####) or Org Unit ID (ou-1a2b-34uvwxyz)
     *
     *   @return array
     *      [
     *          'Arn' => '<string>',
     *          'Email' => '<string>',
     *          'Id' => '<string>',
     *          'JoinedMethod' => 'INVITED|CREATED',
     *          'JoinedTimestamp' => <DateTime>,
     *          'Name' => '<string>',
     *          'Status' => 'ACTIVE|SUSPENDED',
     *      ],
     */
    public function all($request_data = [])
    {

        if(array_key_exists('parent_id', $request_data)) {
            $records = $this->aws_api_client->listOrganizationalUnitsForParent([
                'ParentId' => $request_data['parent_id']
            ]);
        } else {
            $records = $this->aws_api_client->listAccounts([]);
        }

        return $records;
    }

    /**
     *   Create a AWS organization unit
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-organizations-2016-11-28.html#createorganizationalunit
     *
     *   @param  array  $request_data
     *      name|string                 Friendly name for AWS organizational unit
     *      parent_id|string            Root (r-####) or Org Unit ID (ou-1a2b-34uvwxyz)
     *      tags|array (opt)
     *          [
     *              'Key' => '<string>',
     *              'Value' => '<string>'
     *          ]
     *
     *   @return array
     *      'Arn' => '<string>',
     *      'Id' => '<string>',
     *      'Name' => '<string>',
     */
    public function create($request_data = [])
    {
        // Create AWS account
        $organizational_unit = $this->aws_api_client->createOrganizationalUnit([
            'Name' => $request_data['name'],
            'ParentId' => $request_data['parent_id'],
            'Tags' => array_key_exists('tags', $request_data) ? $request_data['tags'] : [],
        ]);

        return $organizational_unit['OrganizationalUnit'];
    }

    /**
     *   Get an AWS Organizational Unit
     *
     *   @param  string $id     Organizational Unit ID (ou-1a2b-34uvwxyz)
     *
     *   @return array
     *      'Arn' => '<string>',
     *      'Id' => '<string>',
     *      'Name' => '<string>',
     *      'ChildAccounts' => [
     *          [
     *              'Arn' => '<string>',
     *              'Email' => '<string>',
     *              'Id' => '<string>',
     *              'JoinedMethod' => 'INVITED|CREATED',
     *              'JoinedTimestamp' => <DateTime>,
     *              'Name' => '<string>',
     *              'Status' => 'ACTIVE|SUSPENDED',
     *          ]
     *      ],
     *      'ChildOrganizationalUnits' => [
     *          [
     *              'Arn' => '<string>',
     *              'Id' => '<string>',
     *              'Name' => '<string>',
     *          ]
     *      ],
     *      'Parent' => [
     *          'Id' => '<string>',
     *          'Type' => 'ROOT|ORGANIZATIONAL_UNIT',
     *      ],
     *      'Tags' => [
     *          [
     *              'Key' => '<string>',
     *              'Value' => '<string>'
     *          ]
     *      ]
     */
    public function get($id)
    {
        // Get AWS account
        $record = $this->aws_api_client->describeOrganizationalUnit([
            'OrganizationalUnitId' => $id,
        ]);

        // Get tags for AWS organization unit
        $tags = $this->aws_api_client->listTagsForResource([
            'ResourceId' => $id
        ]);

        // Add tags to returned array
        $record['OrganizationalUnit']['Tags'] = $tags;

        // Get accounts for organizational unit
        $accounts = $this->aws_api_client->listAccountsForParent([
            'ParentId' => $id
        ]);

        // Add accounts to returned array
        $record['OrganizationalUnit']['ChildAccounts'] = $accounts['Accounts'];

        // Get child organizational units
        $organizational_units = $this->aws_api_client->listOrgaizationalUnitsForParent([
            'ParentId' => $id
        ]);

        // Add organizational units to returned array
        $record['OrganizationalUnit']['ChildOrganizationalUnits'] = $orgaizational_units['OrganizationalUnits'];

        // Get parent root or organization unit
        $parents = $this->aws_api_client->listParents([
            'ChildId' => $id
        ]);

        // Add parent to return array
        $record['OrganizationalUnit']['Parent'] = $parents['Parents'][0];

        return $record['OrganizationalUnit'];
    }

    public function update($id, $request_data = [])
    {
        // updateOrganizationalUnit(array $args = [])
        // tagResource(array $args = [])
        // untagResource(array $args = [])
    }

    public function delete($id)
    {
         // deleteOrganizationalUnit(array $args = [])
    }

}
