<?php

namespace App\Services\V1\Vendor\Aws;

use App\Services\V1\Vendor\Aws\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;
use Illuminate\Support\Facades\Log;

class IamUserService extends BaseService
{

    protected $aws_api_client;

    public function __construct($cloud_provider_id, $aws_child_account_id = null)
    {
        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);

        // Decrypt the provider credentials to be able to use the array keys
        $credentials = json_decode(decrypt($this->cloud_provider->api_credentials), true);

        // If AWS child account ID is set, we will use AWS Security Token
        // Service (STS) to get temporary credentials that use the assumeRole
        // functionality to get Administrator access to the child AWS account.
        if($aws_child_account_id != null) {

            // Initialize STS Client using cloud provider credentials
            $aws_sts_client = new \Aws\Sts\StsClient([
                'version' => 'latest',
                'region' => 'us-east-1',
                'credentials' => [
                    'key' => $credentials['aws_access_key_id'],
                    'secret' => $credentials['aws_access_key_secret'],
                ],
            ]);

            // Use STS to assume role and get credentials for child account
            $child_account_credentials = $aws_sts_client->assumeRole([
                'DurationSeconds' => '900',
                'ExternalId' => $aws_child_account_id,
                'RoleArn' => 'arn:aws:iam::'.$aws_child_account_id.':role/OrganizationAccountAccessRole',
                'RoleSessionName' => 'HackyStackIamUserService'.now()->format('YmdHiSu')
            ]);

            // Initialize the API client
            $this->aws_api_client = new \Aws\Iam\IamClient([
                'version' => 'latest',
                'region'  => 'us-east-1',
                'credentials' => [
                    'key'    => $child_account_credentials['Credentials']['AccessKeyId'],
                    'secret' => $child_account_credentials['Credentials']['SecretAccessKey'],
                    'token' => $child_account_credentials['Credentials']['SessionToken']
                ],
            ]);

        }

        // If AWS child account ID is not set, the actions will be performed
        // in the organization root account.
        else {

            // Initialize the API client
            $this->aws_api_client = new \Aws\Iam\IamClient([
                'version' => 'latest',
                'region'  => 'us-east-1',
                'credentials' => [
                    'key'    => $credentials['aws_access_key_id'],
                    'secret' => $credentials['aws_access_key_secret'],
                ],
            ]);

        }

    }

    public function all($request_data = [])
    {

        //
    }

    /**
     *   Get an IAM User
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#getuser
     *
     *   @param  array  $request_data
     *      username|string     IAM username
     *
     *   @return array
     *      'Arn' => '<string>',
     *      'CreateDate' => <DateTime>,
     *      'PasswordLastUsed' => <DateTime>,
     *      'Path' => '<string>',
     *      'PermissionsBoundary' => [
     *          'PermissionsBoundaryArn' => '<string>',
     *          'PermissionsBoundaryType' => 'PermissionsBoundaryPolicy',
     *      ],
     *      'Tags' => [
     *          [
     *              'Key' => '<string>',
     *              'Value' => '<string>',
     *          ],
     *          // ...
     *      ],
     *      'UserId' => '<string>',
     *      'UserName' => '<string>',
     */
    public function get($request_data = [])
    {
        // Get AWS IAM user
        $iam_user = $this->aws_api_client->getUser([
            'UserName' => $request_data['username']
        ]);

        return $iam_user['User'];
    }

    /**
     *   Create a AWS IAM user
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#createuser
     *
     *   @param  array  $request_data
     *      username|string             Alphadash username for the AWS IAM User
     *      tags|array (opt)
     *          [
     *              'Key' => '<string>',
     *              'Value' => '<string>'
     *          ]
     *
     *   @return array
     *      'UserId' => '<string>',
     *      'Arn' => '<string>',
     *      'Username' => '<string>',
     *      'Tags' => '<array>',
     */
    public function create($request_data = [])
    {
        // Create AWS IAM user
        $iam_user = $this->aws_api_client->createUser([
            'UserName' => $request_data['username'],
            'Tags' => array_key_exists('tags', $request_data) ? $request_data['tags'] : []
        ]);

        return [
            'UserId' => $iam_user['User']['UserId'],
            'Arn' => $iam_user['User']['Arn'],
            'Username' => $iam_user['User']['UserName'],
            'Tags' => array_key_exists('tags', $request_data) ? $request_data['tags'] : []
        ];

    }

    /**
     *   Create a Login Profile for AWS Console Access with Password
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#createloginprofile
     *
     *   @param  array  $request_data
     *      username|string             Alphadash username for the AWS IAM User
     *      password|string             Strong password to set
     *      must_change_password|bool   (opt) `true` if must change password, default `false`
     *
     *   @return array
     *      'CreateDate' => <DateTime>
     *      'PasswordResetRequired' => true || false
     *      'UserName' => '<string>'
     *      'Password' => '<string>'
     */
    public function createLoginProfile($request_data = [])
    {
        // Create login profile which sets the user's password for the AWS
        // management console
        $login_profile = $this->aws_api_client->createLoginProfile([
            'Password' => $request_data['password'],
            'UserName' => $request_data['username'],
            'PasswordResetRequired' => $request_data['must_change_password']
        ]);

        return [
            'CreateDate' => $login_profile['LoginProfile']['CreateDate'],
            'PasswordResetRequired' => $login_profile['LoginProfile']['PasswordResetRequired'],
            'UserName' => $login_profile['LoginProfile']['UserName'],
            'Password' => $request_data['password']
        ];
    }

    /**
     *   Create an access key id and secret for an IAM user
     *   https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#createaccesskey
     *
     *   @param  array  $request_data
     *      username|string             Alphadash username for the AWS IAM User
     *
     *   @return array
     *      'AccessKeyId' => '<string>'
     *      'CreateDate' => <DateTime>
     *      'SecretAccessKey' => '<string>'
     *      'Status' => '<string>'
     *      'UserName' => '<string>'
     */
    public function createAccessKey($request_data = [])
    {
        // Create access key which provides the user an API key and secret token
        $access_key = $this->aws_api_client->createAccessKey([
            'UserName' => $request_data['username']
        ]);

        return [
            'AccessKeyId' => $access_key['AccessKeyId'],
            'CreateDate' => $access_key['CreateDate'],
            'SecretAccessKey' => $access_key['SecretAccessKey'],
            'Status' => $access_key['Status'],
            'UserName' => $request_data['username'],
        ];
    }

    public function update($id, $request_data = [])
    {
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#updateuser
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#taguser
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#untaguser
    }

    /**
     *   Delete an IAM User
     *
     *   Unlike the AWS Management Console, when you delete a user
     *   programmatically, you must delete the items attached to the
     *   user manually, or the deletion fails.
     *
     *   @param  array  $request_data
     *      username|string             Alphadash username for the AWS IAM User
     *
     *   @return array
     *
     */
    public function delete($request_data = [])
    {

        //
        // Delete Password (DeleteLoginProfile)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deleteloginprofile
        //

        $this->aws_api_client->deleteLoginProfile([
            'UserName' => $request_data['username']
        ]);

        //
        // Delete Access keys (DeleteAccessKey)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listaccesskeys
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deleteaccesskey
        //

        // Get list of records
        $access_keys = $this->aws_api_client->listAccessKeys([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($access_keys['AccessKeyMetadata'] as $access_key) {
            $this->aws_api_client->deleteAccessKey([
                'AccessKeyId' => $access_key['AccessKeyId'],
                'UserName' => $access_key['UserName']
            ]);
        }

        //
        // Delete Signing certificates
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listsigningcertificates
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deletesigningcertificate
        //

        // Get list of records
        $signing_certificates = $this->aws_api_client->listSigningCertificates([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($signing_certificates['Certificates'] as $certificate) {
            $this->aws_api_client->deleteSigningCertificate([
                'CertificateId' => $certificate['CertificateId'],
                'UserName' => $certificate['UserName'],
            ]);
        }

        //
        // Delete SSH public keys (DeleteSSHPublicKey)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listsshpublickeys
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deletesshpublickey
        //

        // Get list of records
        $signing_certificates = $this->aws_api_client->listSSHPublicKeys([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($ssh_public_keys['SSHPublicKeys'] as $key) {
            $this->aws_api_client->deleteSSHPublicKey([
                'SSHPublicKeyId' => $key['SSHPublicKeyId'],
                'UserName' => $request_data['username'],
            ]);
        }

        //
        // Delete service specific credentials (DeleteServiceSpecificCredential)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listservicespecificcredentials
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deleteservicespecificcredential
        //

        // Get list of records
        $service_specific_credentials = $this->aws_api_client->listServiceSpecificCredentials([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($service_specific_credentials['ServiceSpecificCredentials'] as $credential) {
            $this->aws_api_client->deleteServiceSpecificCredential([
                'ServiceSpecificCredentialId' => '<string>',
                'UserName' => '<string>',
            ]);
        }

        //
        // Delete multi-factor authentication devices (DeactivateMFADevice, DeleteVirtualMFADevice)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listmfadevices
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deactivatemfadevice
        //

        // Get list of records
        $mfa_devices = $this->aws_api_client->listMFADevices([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($mfa_devices['MFADevices'] as $device) {
            $this->aws_api_client->deactivateMFADevice([
                'SerialNumber' => $device['SerialNumber'],
                'UserName' => $device['UserName'],
            ]);
        }

        //
        // Delete virtual multi-factor authentication devices
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listvirtualmfadevices
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deletevirtualmfadevice
        //

        // Get list of records
        $mfa_devices = $this->aws_api_client->listVirtualMFADevices([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($mfa_devices['VirtualMFADevices'] as $device) {
            if($device['User']['UserName'] == $request_data['username']) {
                $this->aws_api_client->deleteVirtualMFADevice([
                    'SerialNumber' => $device['SerialNumber'],
                ]);
            }
        }

        //
        // Delete inline user policies (DeleteUserPolicy)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deleteuserpolicy
        //

        // Get list of records
        $user_policies = $this->aws_api_client->listUserPolicies([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($user_policies['PolicyNames'] as $policy) {
            $this->aws_api_client->deleteUserPolicy([
                'PolicyName' => $policy['PolicyName'],
                'UserName' => $request_data['username']
            ]);
        }

        //
        // Detach managed policies (DetachUserPolicy)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#detachuserpolicy
        //

        // Get list of records
        $user_attached_policies = $this->aws_api_client->listAttachedUserPolicies([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($user_attached_policies['AttachedPolicies'] as $policy) {
            $this->aws_api_client->detachUserPolicy([
                'PolicyArn' => $policy['PolicyArn'],
                'UserName' => $request_data['username']
            ]);
        }

        //
        // Remove users from groups (RemoveUserFromGroup)
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#listgroupsforuser
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#removeuserfromgroup
        //

        // Get list of records
        $user_attached_groups = $this->aws_api_client->listGroupsForUser([
            'UserName' => $request_data['username'],
        ]);

        // Loop through records and delete the record
        foreach($user_attached_groups['AttachedPolicies'] as $group) {
            $this->aws_api_client->removeUserFromGroup([
                'GroupName' => $group['GroupName'],
                'UserName' => $request_data['username']
            ]);
        }

        //
        // Delete AWS IAM User
        // https://docs.aws.amazon.com/aws-sdk-php/v3/api/api-iam-2010-05-08.html#deleteuser
        //

        // Delete AWS IAM user
        $iam_user = $this->aws_api_client->deleteUser([
            'UserName' => $request_data['username'],
        ]);

        return [
            'Username' => $iam_user['User']['UserName'],
            'Actions' => $actions
        ];
    }

}
