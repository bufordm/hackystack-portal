<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Services\V1\Vendor\Gcp\BaseService;
use App\Models;
use App\Services;
use Illuminate\Support\Arr;
use Illuminate\Support\Str;

class CloudResourceManagerFolderService extends BaseService
{

    public function __construct($cloud_provider_id)
    {

        // Call BaseService methods to establish API connection
        $this->setCloudProvider($cloud_provider_id);
        $this->setGoogleApiClient();

        // Add API Scope(s)
        // https://developers.google.com/identity/protocols/oauth2/scopes
        // https://github.com/googleapis/google-api-php-client-services/blob/master/src/Google/Service/CloudResourceManager.php
        $this->google_api_client->addScope(\Google_Service_CloudResourceManager::CLOUD_PLATFORM);
    }

    /**
     *   Provision a Folder
     *
     *   @param  array  $request_data
     *      display_name|string          Ex. my-folder-name-a1b2c3d4
     *      parent_folder_name|string    Ex. folders/123456789012 or organizations/123456789012
     *
     *   @return array                  API Meta Data (Response)
     *      "@type" => "type.googleapis.com/google.cloud.resourcemanager.v2.Folder"
     *      "name" => "folders/987654321098"
     *      "parent" => "folders/123456789012"
     *      "displayName" => "my-folder-name-a1b2c3d4"
     *      "lifecycleState" => "ACTIVE"
     *      "createTime" => "2020-10-14T19:17:37.678Z"
     */
    public function provision($request_data = [])
    {

        // Initialize the Resource Manager API Service
        // https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/CloudResourceManager
        $google_cloud_resource_manager_service = new \Google_Service_CloudResourceManager($this->google_api_client);

        // Define API request body
        // https://github.com/googleapis/google-api-php-client#making-requests
        $request_body = new \Google_Service_CloudResourceManager_Folder([
            'displayName' => $request_data['display_name'],
            'parent' => $request_data['parent_folder_name']
        ]);

        try {
/*
            // Create the new folder
            //
            // https://cloud.google.com/resource-manager/reference/rest/v2/folders/create
            // https://cloud.google.com/resource-manager/reference/rest/v2/folders#Folder
            $folder_api_response = $google_cloud_resource_manager_service->folders->create($request_body, [
                'parent' => $request_data['parent_folder_name']
            ]);

            // Returns a Cloud Resource Manager Operation instance
*/

            // returns a Guzzle HTTP Client
            $httpClient = $this->google_api_client->authorize();

            // make an HTTP request
            $folder_api_response = $httpClient->request('POST', 'https://cloudresourcemanager.googleapis.com/v2/folders', [
                'query' => [
                    'parent' => $request_data['parent_folder_name']
                ],
                'form_params' => [
                    'displayName' => $request_data['display_name'],
                    'parent' => $request_data['parent_folder_name']
                ],
            ]);

            dd($folder_api_response);

        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);
        }

        // Loop through 1 second intervals (for 30 seconds) until operation status is done
        for($wait_secs = 0; $wait_secs<=30; $wait_secs++) {

            // Perform API call to Operations Service to get status
            $operation_api_response = $google_cloud_resource_manager_service->operations->get($folder_api_response->name);

            // If operation has completed, return the response array
            if($operation_api_response->done == true) {

                // Google_Service_CloudResourceManager_Operation^ {
                //   +done: true
                //   #errorType: "Google_Service_CloudResourceManager_Status"
                //   #errorDataType: ""
                //   +metadata: null
                //   +name: "operations/cf.1234567890123456789"
                //   +response: array:6 [
                //     "@type" => "type.googleapis.com/google.cloud.resourcemanager.v2.Folder"
                //     "name" => "folders/987654321098"
                //     "parent" => "folders/123456789012"
                //     "displayName" => "my-folder-name-a1b2c3d4"
                //     "lifecycleState" => "ACTIVE"
                //     "createTime" => "2020-10-14T19:17:37.678Z"
                //  ]
                //   #internal_gapi_mappings: []
                //   #modelData: []
                //   #processed: []
                // }

                return $operation_api_response->response;

            }

            // If operation has not completed, sleep and try again
            else {

                // Google_Service_CloudResourceManager_Operation^ {
                //   +done: null
                //   #errorType: "Google_Service_CloudResourceManager_Status"
                //   #errorDataType: ""
                //   +metadata: null
                //   +name: "operations/cf.1234567890123456789"
                //   +response: null
                //   #internal_gapi_mappings: []
                //   #modelData: []
                //   #processed: []
                // }

                if($wait_secs == 30) {
                    abort(504, 'API operation timeout when creating a GCP folder', [
                        'display_name' => $request_data['display_name'],
                        'parent_folder_name' => $request_data['parent_folder_name'],
                        'operation_id' => $operation_api_response->name
                    ]);
                    break;
                } else {
                    sleep(1);
                    continue;
                }

            }
        } // for($wait_secs)

    }

    /**
     *   Deprovision a Folder
     *
     *   @param  array  $request_data
     *      folder_name|string          Ex. folders/987654321098
     *
     *   @return bool
     */
    public function deprovision($request_data = [])
    {

        // Initialize the Resource Manager API Service
        // https://github.com/googleapis/google-api-php-client-services/tree/master/src/Google/Service/CloudResourceManager
        $google_cloud_resource_manager_service = new \Google_Service_CloudResourceManager($this->google_api_client);

        try {

            // Delete the folder
            // https://cloud.google.com/resource-manager/reference/rest/v2/folders/delete
            $folder_api_response = $google_cloud_resource_manager_service->folders->delete($request_data['folder_name']);

            // Google_Service_CloudResourceManager_Operation^ {
            //   +done: null
            //   #errorType: "Google_Service_CloudResourceManager_Status"
            //   #errorDataType: ""
            //   +metadata: null
            //   +name: "operations/cf.6502245121924581892"
            //   +response: null
            //   #internal_gapi_mappings: []
            //   #modelData: []
            //   #processed: []
            // }

            return true;

        } catch(\Google_Service_Exception $e) {
            $this->handleException($e);

            return false;

        }
    }

}
