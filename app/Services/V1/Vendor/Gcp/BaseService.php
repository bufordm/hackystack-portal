<?php

namespace App\Services\V1\Vendor\Gcp;

use App\Models;
use Illuminate\Support\Str;

class BaseService
{

    protected $cloud_provider;
    protected $google_api_client;

    public function setCloudProvider($cloud_provider_id)
    {
        // Get cloud provider from database
        $this->cloud_provider = Models\Cloud\CloudProvider::query()
            ->where('id', $cloud_provider_id)
            ->firstOrFail();

        // Check if cloud provider type is not correct
        if($this->cloud_provider->type != 'gcp') {
            abort(500, 'The GCP service is trying to use a cloud provider that is not the correct type.', [
                'cloud_provider_id' => $this->cloud_provider->id,
                'cloud_provider_type' => $this->cloud_provider->type,
            ]);
        }
    }

    public function setGoogleApiClient()
    {
        // Connect to Google API using Cloud Provider credentials
        // https://github.com/googleapis/google-api-php-client#authentication-with-service-accounts
        // https://developers.google.com/my-business/samples#detailed_error_responses
        $this->google_api_client = new \Google_Client();
        $this->google_api_client->setAuthConfig(json_decode(decrypt($this->cloud_provider->api_credentials), true));
        $this->google_api_client->setApiFormatV2(2);
    }

    public function handleException($e)
    {
        $error_message = json_decode($e->getMessage());
        dd($error_message->error->code.' '.$error_message->error->message);
    }

}
